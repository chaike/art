<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Summary_model extends CI_Model {

    public $Cache;

    public function __construct() {
        parent::__construct();

        if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
            $client = new PDO('mysql:dbname=art_cache_database;host=127.0.0.1', 'java', 'java');
            $this->Cache = new \MatthiasMullie\Scrapbook\Adapters\MySQL($client);
        } else {
            $client = new PDO('mysql:dbname=art_cache_database;host=127.0.0.1', 'root', 'MyNewPass');
            $this->Cache = new \MatthiasMullie\Scrapbook\Adapters\MySQL($client);
        }
    }

    public function get_patient_scaleup($filters) {
        $columns = array();
        $scaleup_data = array(
            array('type' => 'column', 'name' => 'Paediatric', 'data' => array()),
            array('type' => 'column', 'name' => 'Adult', 'data' => array())
        );

        $this->db->select("CONCAT_WS('/', data_month, data_year) period, SUM(IF(age_category = 'paed', total, NULL)) paed_total, SUM(IF(age_category = 'adult', total, NULL)) adult_total", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'data_date') {
                    $this->db->where("data_date >= ", date('Y-01-01', strtotime($filter . "- 1 year")));
                    $this->db->where("data_date <=", $filter);
                } else {
                    $this->db->where_in($category, $filter);
                }
            }
        }
        $this->db->group_by('period');
        $this->db->order_by("data_year ASC, FIELD( data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )");
        $query = $this->db->get('dsh_patient');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['period'];
                foreach ($scaleup_data as $index => $scaleup) {
                    if ($scaleup['name'] == 'Adult') {
                        array_push($scaleup_data[$index]['data'], $result['adult_total']);
                    } else if ($scaleup['name'] == 'Paediatric') {
                        array_push($scaleup_data[$index]['data'], $result['paed_total']);
                    }
                }
            }
        }
        return array('main' => $scaleup_data, 'columns' => $columns);
    }

    public function get_county_drug_mos($filters) {
        unset($filters['regimen_service']);
        //if (empty($this->Cache->get('county_drug'))) {
            $get_drugs = $this->db->query("SELECT * FROM `vw_drug_list` where regimen_category like '%adult%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('73','76');")->result();
            $columns = array();
            $scaleup_data = [];
            $totals = [];
            $i = 0;
            $query_ = '';
            $sum = '';
            foreach ($get_drugs as $cols) {
                array_push($scaleup_data, ['type' => 'column', 'name' => $cols->name, 'data' => [], 'id' => $cols->id]);
                $query_ .= " ROUND(SUM(IF(drug = '$cols->name', total, NULL))/SUM(IF(drug = '$cols->name', amc_total, NULL)),2) mos$i,";
                $sum .= "IF(mos$i IS NULL,0.00,mos$i)+";
                $i++;
            }
            $final_query = rtrim($query_, ',');
            $sum_query = ',SUM(' . rtrim($sum, '+') . ') mos_total';


            $this->db->select("UPPER(county)county, $final_query", FALSE);
            if (!empty($filters)) {
                foreach ($filters as $category => $filter) {
                    $this->db->where_in($category, $filter);
                }
            }
            $this->db->group_by('county');
            //$this->db->order_by("county DESC");
            $query = $this->db->get_compiled_select('dsh_stock');


            // $this->db->query("DROP VIEW IF EXISTS vw_paeds_mos");
            // $this->db->query("DROP VIEW IF EXISTS vw_paeds_mos_final");
            $this->db->query("CREATE OR REPLACE VIEW vw_adult_mos AS $query");
            $this->db->query("CREATE OR REPLACE VIEW vw_adult_mos_final AS SELECT * $sum_query FROM vw_adult_mos GROUP BY county ORDER BY mos_total DESC");

            //echo $query;
            $results = $this->db->get('vw_adult_mos_final')->result_array();

            if ($results) {
                foreach ($results as $key => $result) {
                    $columns[] = $result['county'];
                    // $total[] = 
                    foreach ($scaleup_data as $index => $scaleup) {
                        if ($scaleup['name'] == $scaleup_data[$index]['name']) {
                            array_push($scaleup_data[$index]['data'], $result['mos' . $index]);
                        }
                    }
                    //array_push($scaleup_data['total'], $result['mos_total']);
                }
            }


            //$data = json_encode(array('main' => $scaleup_data, 'columns' => $columns));
            //$this->Cache->add('county_drug', $data);
            return array('main' => $scaleup_data, 'columns' => $columns);
        //} else {
          //  return json_decode($this->Cache->get('county_drug'), true);
        //}
    }

    public function get_paed_drug_mos($filters) {
        unset($filters['regimen_service']);
        //if (empty($this->Cache->get('paed_drug'))) {
            $get_drugs = $this->db->query("SELECT * FROM `vw_drug_list` where regimen_category like '%paed%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('71');")->result();
            $columns = array();
            $scaleup_data = [];
            $i = 0;
            $query_ = '';
            $sum = '';

            foreach ($get_drugs as $cols) {
                array_push($scaleup_data, ['type' => 'column', 'name' => $cols->name, 'data' => [], 'id' => $cols->id]);
                $query_ .= " ROUND(SUM(IF(drug = '$cols->name', total, 0))/SUM(IF(drug = '$cols->name', amc_total, 0)),2) mos$i,";
                $sum .= "IF(mos$i IS NULL,0.00,mos$i)+";
                $i++;
            }
            $final_query = rtrim($query_, ',');
            $sum_query = ',SUM(' . rtrim($sum, '+') . ') mos_total';


            $this->db->select("UPPER(county)county, $final_query", FALSE);
            if (!empty($filters)) {
                foreach ($filters as $category => $filter) {
                    $this->db->where_in($category, $filter);
                }
            }
            $this->db->group_by('county');
            //$this->db->order_by("mos_total DESC");
            $query = $this->db->get_compiled_select('dsh_stock');
            // $this->db->query("DROP VIEW IF EXISTS vw_paeds_mos");
            // $this->db->query("DROP VIEW IF EXISTS vw_paeds_mos_final");
            $this->db->query("CREATE OR REPLACE VIEW vw_paeds_mos AS $query");
            $this->db->query("CREATE OR REPLACE VIEW vw_paeds_mos_final AS SELECT * $sum_query FROM vw_paeds_mos GROUP BY county ORDER BY mos_total DESC");

            //echo $query;
            $results = $this->db->get('vw_paeds_mos_final')->result_array();



            if ($results) {
                foreach ($results as $result) {
                    $columns[] = $result['county'];
                    foreach ($scaleup_data as $index => $scaleup) {
                        if ($scaleup['name'] == $scaleup_data[$index]['name']) {
                            array_push($scaleup_data[$index]['data'], $result['mos' . $index]);
                        }
                    }
                }
            }

           // $data = json_encode(array('main' => $scaleup_data, 'columns' => $columns));
            //$this->Cache->add('paed_drug', $data);
            return array('main' => $scaleup_data, 'columns' => $columns);
        //} else {
          //  return json_decode($this->Cache->get('paed_drug'), true);
        //}
    }

    public function get_paed_drug_mos_single($filters) {

        // $this->db->select("drug, (SUM(total)/SUM(amc_total)) mos", FALSE);
        $columns = array();
        $data = array();
        //$patient_regm_data = array();
        //if (empty($this->Cache->get('mos_single'))) {
            $this->db->select("drug, ROUND(SUM(total)/SUM(amc_total),2) mos", FALSE);
            if (!empty($filters)) {
                foreach ($filters as $category => $filter) {
                    $this->db->where_in($category, $filter);
                }
            }
            $this->db->from('dsh_stock ds');
            $this->db->join('vw_drug_list d', 'ds.drug = d.name', 'inner');
            $this->db->like('d.regimen_category', 'Paed');
            $this->db->group_by('drug');
            $this->db->order_by("mos DESC");
            $query = $this->db->get();
            $results = $query->result_array();

            foreach ($results as $result) {
                array_push($columns, $result['drug']);
                array_push($data, $result['mos']);
            }


            //$data1 = json_encode(array('main' => $data, 'columns' => array_values(array_unique($columns))));
            //$this->Cache->add('mos_single', $data1);
            return array('main' => $data, 'columns' => array_values(array_unique($columns)));
        //} else {
           // return json_decode($this->Cache->get('mos_single'), true);
        //}
    }

    public function get_adult_drug_mos_single($filters) {
        //  print_r($filters);
        // $this->db->select("drug, (SUM(total)/SUM(amc_total)) mos", FALSE);
        $columns = array();
        $data = array();
        //$patient_regm_data = array();
       // if (empty($this->Cache->get('drug_mos_single'))) {
            $this->db->select("drug, ROUND(SUM(total)/SUM(amc_total),2) mos", FALSE);
            if (!empty($filters)) {
                foreach ($filters as $category => $filter) {
                    $this->db->where_in($category, $filter);
                }
            }
            $this->db->from('dsh_stock ds');
            $this->db->join('vw_drug_list d', 'ds.drug = d.name', 'inner');
            $this->db->like('d.regimen_category', 'Adult');
            $this->db->group_by('drug');
            $this->db->order_by("mos DESC");
            $query = $this->db->get();
            $results = $query->result_array();

            foreach ($results as $result) {
                array_push($columns, $result['drug']);
                array_push($data, $result['mos']);
            }

           // $data1 = json_encode(array('main' => $data, 'columns' => array_values(array_unique($columns))));
           // $this->Cache->add('drug_mos_single', $data1);
            return array('main' => $data, 'columns' => array_values(array_unique($columns)));
        //} else {
           // return json_decode($this->Cache->get('drug_mos_single'), true);
        //}
    }

    public function get_patient_services($filters) {
        $columns = array();
        $patient_services_data = array(
            array('type' => 'column', 'name' => 'ART', 'data' => array()),
            array('type' => 'column', 'name' => 'HepB', 'data' => array()),
            array('type' => 'column', 'name' => 'PEP', 'data' => array()),
            array('type' => 'column', 'name' => 'PMTCT Mother', 'data' => array()),
            array('type' => 'column', 'name' => 'PMTCT Child', 'data' => array()),
            array('type' => 'column', 'name' => 'PrEP', 'data' => array())
        );

        $this->db->select("UPPER(county) county, SUM(IF(regimen_service= 'ART', total, 0)) art, SUM(IF(regimen_service= 'PMTCT' AND age_category = 'adult', total, 0)) pmtct_mother, SUM(IF(regimen_service= 'PMTCT' AND age_category = 'paed', total, 0)) pmtct_child, SUM(IF(regimen_service= 'HepB', total, 0)) hepb, SUM(IF(regimen_service= 'PrEP', total, 0)) prep, SUM(IF(regimen_service= 'PEP', total, 0)) pep", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->where_not_in('regimen_service', 'OI Only');
        $this->db->group_by('county');
        $this->db->order_by("county, regimen_service asc");
        $query = $this->db->get('dsh_patient');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['county'];
                foreach ($patient_services_data as $index => $scaleup) {
                    if ($scaleup['name'] == 'ART') {
                        array_push($patient_services_data[$index]['data'], $result['art']);
                    } else if ($scaleup['name'] == 'HepB') {
                        array_push($patient_services_data[$index]['data'], $result['hepb']);
                    } else if ($scaleup['name'] == 'PEP') {
                        array_push($patient_services_data[$index]['data'], $result['pep']);
                    } else if ($scaleup['name'] == 'PMTCT Mother') {
                        array_push($patient_services_data[$index]['data'], $result['pmtct_mother']);
                    } else if ($scaleup['name'] == 'PMTCT Child') {
                        array_push($patient_services_data[$index]['data'], $result['pmtct_child']);
                    } else if ($scaleup['name'] == 'PrEP') {
                        array_push($patient_services_data[$index]['data'], $result['prep']);
                    }
                }
            }
        }
        return array('main' => $patient_services_data, 'columns' => $columns);
    }

    public function get_national_mos($filters) {
        $columns = array();
        $scaleup_data = array(
            array('name' => 'Pending Orders', 'data' => array()),
            array('name' => 'KEMSA', 'data' => array()),
            array('name' => 'Facilities', 'data' => array())
        );

        $this->db->select('drug, facility_mos, cms_mos, supplier_mos');
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drug');
        $this->db->order_by('drug', 'DESC');
        $query = $this->db->get('dsh_mos');
        $results = $query->result_array();

        foreach ($results as $result) {
            $columns[] = $result['drug'];
            foreach ($scaleup_data as $index => $scaleup) {
                if ($scaleup['name'] == 'Facilities') {
                    array_push($scaleup_data[$index]['data'], $result['facility_mos']);
                } else if ($scaleup['name'] == 'KEMSA') {
                    array_push($scaleup_data[$index]['data'], $result['cms_mos']);
                } else if ($scaleup['name'] == 'Pending Orders') {
                    array_push($scaleup_data[$index]['data'], $result['supplier_mos']);
                }
            }
        }
        return array('main' => $scaleup_data, 'columns' => $columns);
    }

    public function get_all_commodity_view_table($filters) {
        //if (empty($this->Cache->get('commodity_view_table'))) {
            $query2 = '';
            $columns = array();
            $datayear = $filters['data_year'];
            $datamonth = $filters['data_month'];
            $county = $filters['county'];

            if (empty($county)) {
                $query2 .= "AND county='nairobi'";
            } else {
                $query2 .= "AND county='$county[0]'";
            }

            $query = $this->db->query("SELECT county, sub_county, facility,drug, SUM(closing_bal_qty) stock_on_hand, SUM(consumed_qty) qty_consumed,SUM(received_qty) qty_received FROM dsh_order_item
                                    WHERE data_year='$datayear' 
                                    AND data_month='$datamonth'
                                   $query2
                                    GROUP BY drug ,facility")->result_array();

            //$data = json_encode(array('main' => $query, 'columns' => $columns));
           // $this->Cache->add('commodity_view_table', $data);
            return array('main' => $query, 'columns' => $columns);
        //} else {
           // return json_decode($this->Cache->get('commodity_view_table'), true);
        //}
    }

}
