
<form method="get">
    <div class="container">
        </center>
        <div class="navbar-collapse collapse" id="navbar-filter">
            <div class="navbar-form" role="search">

                <div class="form-group" style="font-size: 12px;">
                    <div class="filter form-control" id="year-filter">
                        Year: <select  name="filter_year">
                            <option  value="<?php echo $year; ?>"><?php echo $year; ?></option>
                            <?php
                            $start = (int) date('Y') - 4;
                            $current = (int) date('Y');

                            for ($i = $start; $i <= $current; $i++) {
                                ?>                                              
                                <option  value="<?php echo $i; ?>"><?php echo $i; ?></option>                               
                            <?php } ?>
                        </select>
                    </div>
                    <div class="filter form-control" id="month-filter">
                        Month: <select name="filter_month">
                            <option  value="<?php echo $month; ?>"><?php echo $month; ?></option>
                            <option value="Jan"> Jan </option>
                            <option value="Feb"> Feb </option>
                            <option value="Mar"> Mar </option>
                            <option value="Apr"> Apr </option>
                            <option value="May"> May </option>
                            <option value="Jun"> Jun </option>
                            <option value="Jul"> Jul </option>
                            <option value="Aug"> Aug </option>
                            <option value="Sep"> Sep </option> 
                            <option value="Oct"> Oct </option>
                            <option value="Nov"> Nov </option>
                            <option value="Dec"> Dec</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-filter">Filter</span></button>
            </div>
        </div>
        </center>
    </div>
</form>
