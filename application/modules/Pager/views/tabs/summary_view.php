<style>
    .nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #ffffff;background: #3D4A57; }
    .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none;  color: #5a4080 !important; background: #fff; }
    .nav-tabs > li > a::after { content: ""; background: #5a4080; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
    .tab-nav > li > a::after { background: #5a4080 none repeat scroll 0% 0%; color: #fff; }
    .tab-pane { padding: 15px 0; }
    .tab-content{padding:20px}
    .nav-tabs > li  {width:20%; text-align:center;}
    .card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
    body{ background: #EDECEC; padding:50px}

    @media all and (max-width:724px){
        .nav-tabs > li > a > span {display:none;}	
        .nav-tabs > li > a {padding: 5px 5px;}
    }

</style>
<div role="tabpanel" id="summary" style="overflow: auto;">
    <div class="row">
        <div class="col-md-12" style="overflow-x:hidden;"> 
            <div id="STICKY">
                <div class="row">
                    <center><strong>Commodity Summary as at end of <?php echo $month . ' ,' . $year; ?></strong></center>
                </div>
                <!-- Nav tabs -->
                <div class="">
                    <div class="header-holder">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-home"></i>  <span>Ordering Points</span></a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user"></i>  <span>Patients By Regimen</span></a></li>
        <!--                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i>  <span>Patient ScaleUp Trends</span></a></li>-->
                            <li role="presentation"><a href="#extra" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-plus-square-o"></i>  <span>Pipeline Commodity Consumption</span></a></li>
                            <li role="presentation"><a href="#stocks" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-plus-square-o"></i>  <span>Stocks</span></a></li>
                            <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>  <span>SOH Summary</span></a></li>

                        </ul>
                    </div>

                    <div class="row">
                        <div class="col-md-4"> 
                        </div>
                        <div class="col-md-4"> 
                            <?php $this->load->view('template/filter_view'); ?>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
                <!-- Tab panes -->
                <div style="overflow-x: auto;">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home" >

                            <div class="col-md-8 card " style="padding: 20px;">
                                <table class="table table-responsive table-bordered table-striped" id="example1">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>MFLCode</th>
                                            <th>Category</th>
                                            <th>County</th>
                                            <th>Subcounty</th>   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr></tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-4 card" >
                                <table class="table table-responsive table-bordered table-striped" id="example1">
                                    <thead>
                                        <tr><th colspan="3">Summary of Facility Numbers on ART vs KHIS</th></tr>
                                        <tr><th style="background: #92cddc;">SITE</th>
                                            <th  style="background: #92cddc;"><strong>ART</strong><th  style="background: #92cddc;"><strong>KHIS</strong></tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>CENTRAL</th>
                                            <td style="text-align: right;">350</td>
                                            <td style="text-align: right;">358</td>
                                        </tr>
                                        <tr>
                                            <th>STANDALONE</tdh>
                                            <td style="text-align: right;">571</td>
                                            <td style="text-align: right;">188</td>
                                        </tr>
                                        <tr>
                                            <th>SATELITTE</th>
                                            <td style="text-align: right;">2,655</td>
                                            <td style="text-align: right;">3,065</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr style="background: #92cddc;">
                                            <td ><strong>Total Sites</strong></td>
                                            <th style="text-align: right;"><strong><?php echo number_format(3576, 0); ?> </strong></th>
                                            <th style="text-align: right;"><strong><?php echo number_format(3611, 0); ?> </strong></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class=""><center><strong>PATIENTS BY REGIMEN</strong></center></div>
                            <div class="col-md-6 card " style="padding: 20px;">
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr style="background: #bfbfbf;">
                                                <th>CODE | REGIMEN</th>
                                                <th>Total Patients on Regimen</th>                                              
                                            </tr>
                                            <tr><td colspan="2" style="background: #ff00ff;"><strong>Adult ART Patients</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $total = 0;
                                            foreach ($adult_art as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right; background: #ffff99;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $total = $total + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #ff00ff;">
                                                <td ><center><strong>Total - Adult ART Patients</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($total, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #00b050;"><strong>Adult PEP</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ptotal = 0;
                                            foreach ($adult_pep as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $ptotal = $ptotal + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #00b050;">
                                                <td ><center><strong>Total - Adult PEP</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($ptotal, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #92cddc;"><strong>PMTCT - Women</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $pmttotal = 0;
                                            foreach ($pmtct as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $pmttotal = $pmttotal + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #92cddc;">
                                                <td ><center><strong>Total - PMTCT - Women</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($pmttotal, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #ff00ff;"><strong>Paediatric ART Patients</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ttotal = 0;
                                            foreach ($paed_art as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $ttotal = $ttotal + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #ff00ff;">
                                                <td ><center><strong>Total - Paediatric ART</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($ttotal, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #00b050;"><strong>Paediatric PEP</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ttotalp = 0;
                                            foreach ($paed_pep as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $ttotalp = $ttotalp + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #00b050;">
                                                <td ><center><strong>Total - Paediatric ART</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($ttotalp, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #92cddc;"><strong>Paediatric PMTCT</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tptotal = 0;
                                            foreach ($paed_pmtct as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $tptotal = $tptotal + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #92cddc;">
                                                <td ><center><strong>Total - Paediatric PMTCT</strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($tptotal, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #ffc000;"><strong>Patients on PrEP</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tptotalp = 0;
                                            foreach ($prep as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $tptotalp = $tptotalp + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #ffc000;">
                                                <td ><center><strong>Total - Patients on PrEP </strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($tptotalp, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="row">
                                    <table class="table table-responsive table-bordered table-striped" id="example1">
                                        <thead>
                                            <tr><td colspan="2" style="background: #fcd5b4;"><strong>Patients on Medicines for OIs</strong></tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tptotalc = 0;
                                            foreach ($oi as $p) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p->regimen; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($p->patients, 0); ?></td>
                                                </tr>
                                                <?php
                                                $tptotalc = $tptotalc + $p->patients;
                                            }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr style="background: #fcd5b4;">
                                                <td ><center><strong>Total - Patients on Medicines for OIs </strong></center></td>
                                        <td style="text-align: right;"><strong><?php echo number_format($tptotalc, 2); ?> </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div> 
                            <div class="col-md-1">

                            </div>

                            <div class="col-md-5 card">
                                <div class="row">
                                    <center>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><strong>Summary Tables: Patient Regimens</strong></td>                                                
                                                </tr>
                                                <tr>
                                                    <td><strong>Category</strong></td>
                                                    <td><strong>Total</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>

                                                    <td>Adult ART Patients</td>
                                                    <td style="text-align: right;"><?php echo number_format($total, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>Paediatric ART Patients</td>
                                                    <td style="text-align: right;"><?php echo number_format($ttotal, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>PEP - Children</td>
                                                    <td style="text-align: right;"><?php echo number_format($ttotalp, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>PMTCT - Infants</td>
                                                    <td style="text-align: right;"><?php echo number_format($tptotal, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>PMTCT - Pregnant & Lactating Women</td>
                                                    <td style="text-align: right;"><?php echo number_format($pmttotal, 0); ?></td>
                                                </tr>
                                                <?php $art_data = [$total, $ttotal, $tptotalp, $tptotal, $pmttotal]; ?>
                                            </tbody>
                                            <tbody>
                                                <tr style="background: #d9edf2">
                                                    <th style="text-align: right;">TOTAL</th>
                                                    <th style="text-align: right;"><?php echo number_format($pmttotal + $total + $ttotal + $ttotalp + $tptotal, 0); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </center>
                                </div>

                                <div class="row">
                                    <center>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <td colspan="2"><strong></strong></td>                                                
                                                </tr>
                                                <tr>
                                                    <td><strong>Category</strong></td>
                                                    <td><strong>Total</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>

                                                    <td>OIs - Universal prophylaxis</td>
                                                    <td style="text-align: right;"><?php echo number_format($uprof, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>OIs - IPT</td>
                                                    <td style="text-align: right;"><?php echo number_format($ipt, 0); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>OIs - Fluconazole (Diflucan program)</td>
                                                    <td style="text-align: right;"><?php echo number_format($flu, 0); ?></td>
                                                </tr>

                                            </tbody>
                                            <tbody>
                                                <tr style="background: #d9edf2">
                                                    <th style="text-align: right;">TOTAL</th>
                                                    <th style="text-align: right;"><?php echo number_format($uprof + $ipt + $flu, 0); ?></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </center>
                                </div>

                                <div class="row mt-4" >
                                    <center>
                                        <strong></strong>
                                    </center>

                                    <div id="artNumbers" style="width:590px !important;"></div>

                                </div>
                                <div class="row mt-4" >
                                    <center>
                                        <strong></strong>
                                    </center>

                                    <div id="artNumbersProf" style="width:590px !important;"></div>

                                </div>




                            </div>
                        </div>
                        <!--                    <div role="tabpanel" class="tab-pane" id="messages">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>-->
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="col-md-12 ">
                                <div class="col-md-12 card" >
                                    <p><strong>Adult Summary Stock Status</strong></p>
                                    
                                            <div id="mos_fadult_chart"></div>
                                     

                                </div>

                            </div>
                            <div class="col-md-12  " style="padding: 10px;">
                                <div class="col-md-12 card" >
                                    <p><strong>Paed Summary Stock Status</strong></p>
                                    <div class="panel panel-info">

                                        <div class="panel-heading">
                                            <i class="fa fa-clock-o fa-fw"></i> National MoS Paeds (Facilities + CENTRAL STORE)  <span class="">: REPORT AS AT END <?php echo $month . ' ,' . $year; ?>/span>
                                        </div>
                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div id="mos_fpaeds_chart"></div>
                                        </div>
                                        <!-- /.panel-body -->
                                        <div class="panel-footer">
                <!--                            <span class="stock_status_trend_chart_heading heading"></span>-->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="extra">
                            <div class="col-md-12  " style="padding: 20px;">
                                <?php echo $cons; ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="stocks">
                            <div class="col-md-12  " style="padding: 20px;">
                                <div class="col-md-12 card" >
                                    <table class="table table-bordered table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <th>Drug</th>
                                                <th>Pack Size</th>
                                                <th>AMC</th>
                                                <th>Facility Stock on Hand</th>
                                                <th> Stocks on hand - Central stores</th>
                                                <th>Pending with suppliers</th>
                                                <th>Issued</th>
                                                <th>Received</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($amc as $m):
                                                if ($m->drug == 'Lopinavir/Ritonavir (LPV/r) 80/20mg/ml Liquid') {
                                                    $Amc = $m->amc / 5;
                                                    $facility_stocks = $m->facility_stocks / 5;
                                                    $close_kemsa = $m->close_kemsa / 5;
                                                    $pending_stock = $m->pending_stock / 5;
                                                    $receipts_kemsa = $m->receipts_kemsa / 5;
                                                    $issues = $m->issues / 5;
                                                } else {

                                                    $Amc = $m->amc;
                                                    $facility_stocks = $m->facility_stocks;
                                                    $close_kemsa = $m->close_kemsa;
                                                    $pending_stock = $m->pending_stock;
                                                    $receipts_kemsa = $m->receipts_kemsa;
                                                    $issues = $m->issues;
                                                }
                                                ?>
                                                <tr>
                                                    <td><?php echo $m->drug; ?></td>
                                                    <td><?php echo $m->pack_size; ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($Amc, 0); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($facility_stocks, 0); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($close_kemsa, 0); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($pending_stock, 0); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($issues, 0); ?></td>
                                                    <td style="text-align: right;"><?php echo number_format($receipts_kemsa, 0); ?></td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      

</div>
<script type="text/javascript" src="<?php echo base_url() . 'public/manager/js/2pager_2.js'; ?>"></script>

<script>
    $(document).ready(function () {


        var chartDIV = 'mos_fadult_chart';
        Highcharts.wrap(Highcharts.Axis.prototype, 'getPlotLinePath', function (proceed) {
            var path = proceed.apply(this, Array.prototype.slice.call(arguments, 1));
            if (path) {
                path.flat = false;
            }
            return path;
        });

        Highcharts.chart(chartDIV, {
            legend: {
                enabled: true
            },
            chart: {
                type: 'column',
                height: 600,

            },

            colors: ['green', 'red', 'blue'],

            title: {
                text: ''
            },

            xAxis: {
                categories: <?php echo $chart_categories_adult; ?>,
                crosshair: true,
                labels: {
                    rotation: 0,
                    style: {
                        // color: 'red',
                        fontSize: '9px'
                    }
                }

            },
            yAxis: [{
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    title: {
                        text: 'Months of Stock'
                    },
                    reversedStacks: false,
                }, {
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    opposite: true, //optional, you can have it on the same side.
                    title: {
                        text: 'Months of Stock'
                    },
                    plotLines: [{
                            value: 9,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            x: -10,
                            label: {
                                text: 'Under Stocked'
                            }
                        }, {
                            value: 18,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            label: {
                                text: 'Over Stocked'
                            }
                        }],
                }],

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: 'yellow',
                color: 'white',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
                footerFormat: 'Total: <b>{point.total:,.1f}</b>',
                shared: true,
                useHTML: true,
                zIndex: 1000
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 15,
                    dataLabels: {
                        enabled: true,
                        verticalAlign: 'bottom',
                        style: {
                            color: 'black'
                        }
                    },
                    enableMouseTracking: true,
                    borderWidth: 0
                },
                marker: {

                    radius: 300
                }
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data_adult; ?>
        });


        Highcharts.chart('mos_fpaeds_chart', {
            legend: {
                enabled: true
            },
            chart: {
                type: 'column',
                height: 600

            },

            colors: ['green', 'red', 'blue'],

            title: {
                text: ''
            },

            xAxis: {
                categories: <?php echo $chart_categories_paed; ?>,
                crosshair: true,
                labels: {
                    rotation: 0,
                    style: {
                        // color: 'red',
                        fontSize: '9px'
                    }
                }

            },
            yAxis: [{
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    title: {
                        text: 'Months of Stock'
                    },
                    reversedStacks: false,
                }, {
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    opposite: true, //optional, you can have it on the same side.
                    title: {
                        text: 'Months of Stock'
                    },
                    plotLines: [{
                            value: 9,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            x: -10,
                            label: {
                                text: 'Under Stocked'
                            }
                        }, {
                            value: 18,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            label: {
                                text: 'Over Stocked'
                            }
                        }],
                }],

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: 'yellow',
                color: 'white',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
                footerFormat: 'Total: <b>{point.total:,.1f}</b>',
                shared: true,
                useHTML: true,
                zIndex: 1000
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 15,
                    dataLabels: {
                        enabled: true,
                        verticalAlign: 'bottom',
                        style: {
                            color: 'black'
                        }
                    },
                    enableMouseTracking: true,
                    borderWidth: 0
                },
                marker: {

                    radius: 300
                }
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data_paed; ?>
        });



        dtable = $('#example1').DataTable({
            responsive: true,
            ordering: false,
            pagingType: "full_numbers",
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: "<?php echo base_url() . 'Pager/getData'; ?>",

            "columns":
                    [
                        {"data": "id"},
                        {"data": "name"},
                        {"data": "mflcode"},
                        {"data": "category"},
                        {"data": "county"},
                        {"data": "subcounty"}
                    ], initComplete: function () {
                this.api().columns([3, 4, 5]).every(function () {
                    var column = this;
                    var select = $('<br/><select><option value="">Show all</option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var val = $('<div/>').html(d).text();
                        select.append('<option value="' + val + '">' + val + '</option>');
                    });
                });
                //Show reporting rate

            }
        });

        $('#consumedItems').DataTable();


        Highcharts.chart('artNumbers', {
            chart: {
                type: 'column'},
            colors: ['#dd0806'],

            title: {
                text: 'ART Patients by Age and Regimen category'
            },
            subtitle: {
                text: 'Source: commodities.nascop.com'
            },
            xAxis: {
                categories: [
                    'Adult ART Patients',
                    'Paediatric ART Patients',
                    'PEP - Children',
                    'PMTCT - Infants',
                    'PMTCT - Pregnant & Lactating Women'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Patients / Clients'
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }},
            series: [{
                    data: [<?php echo $total ?>, <?php echo $ttotal ?>, <?php echo $ttotalp ?>, <?php echo $tptotal ?>, <?php echo $pmttotal ?>]
                }]
        });

        Highcharts.chart('artNumbersProf', {
            chart: {
                type: 'column'},
            colors: ['#4f81bd'],

            title: {
                text: 'Patients on OI medicines by Age and Regimen category'
            },
            subtitle: {
                text: 'Source: commodities.nascop.com'
            },
            xAxis: {
                categories: [
                    'OIs - Universal prophylaxis',
                    'OIs - IPT',
                    'OIs - Fluconazole (Diflucan program)'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Patients / Clients'
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }},
            series: [{
                    data: [<?php echo @$uprof ?>, <?php echo @$ipt ?>, <?php echo @$flu ?>]
                }]
        });


    })

</script>