<div role="tabpanel" class="tab-pane" id="adt_analytics">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<!--adt_art_ageing_table-->
				<div class="chart-wrapper">
					<div class="chart-title">
						<strong>ADT SITE(S) INSTALLATION NUMBERS</strong>
					</div>
					<div class="chart-stage">
						<div id="adt_art_ageing_table"></div>
					</div>
					<div class="chart-notes">
						<span class="adt_art_ageing_table_heading"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>