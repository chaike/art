<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>';

        Highcharts.setOptions({
            colors: ['#5cb85c', '#f0ad4e', '#5bc0de', '#808080', '#ac5353', '#0080ff', '#ff4000']
        });

        Highcharts.chart(chartDIV, {
            legend: {
                marginTop: 50,
                verticalAlign: 'top',
                floating: false,
                x: 0,
                y: 0
            },
            chart: {
                type: 'column',
                marginBottom: 110,
                marginLeft: 80,
                height: 700,
                events: {
                    load: function (event) {                       
                        this.series.forEach(function (d, i) {
                            if (d.options.id == '1' || 
                                    d.options.id=='8' || 
                                    d.options.id=='15' || 
                                    d.options.id=='25' || 
                                    d.options.id=='25' || 
                                    d.options.id=='26' || 
                                    d.options.id=='38' || 
                                    d.options.id=='41' || 
                                    d.options.id=='44' || 
                                    d.options.id=='49' || 
                                    d.options.id=='55' || 
                                    d.options.id=='56' || 
                                    d.options.id=='60' || 
                                    d.options.id=='61' || 
                                    d.options.id=='65' || 
                                    d.options.id=='73' || 
                                    d.options.id=='76'){
                                d.hide()
                            }
                        })
                    }
                }

            },

            title: {
                text: '<?php echo $chart_title; ?>'
            },
            subtitle: {
                text: '<?php echo $chart_source; ?>'
            },

            xAxis: {
                categories: <?php echo $chart_categories; ?>,
                crosshair: true
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: '<?php echo $chart_yaxis_title; ?>',
                }
            },

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                footerFormat: '<b>County Total MOS: {point.total:,.2f}</b>',
                shared: true
            },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        y: -20,
                        verticalAlign: 'top',
                        pointWidth: 50
                    }
                }
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data; ?>,
        });
    })
</script>
