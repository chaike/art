<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>';

        Highcharts.setOptions({
            colors: ['#5cb85c', '#f0ad4e', '#5bc0de', '#808080', '#ac5353', '#0080ff', '#ff4000']
        });

        Highcharts.chart(chartDIV, {
            legend: {
                marginTop: 50,
                verticalAlign: 'top',
                floating: false,
                x: 0,
                y: 0
            },
            chart: {
                type: 'column',
                marginBottom: 110,
                marginLeft: 80,
                height: 700,
                events: {
                    load: function (event) {
                        this.series.forEach(function (d, i) {
                            if (d.options.id == '17' ||
                                    d.options.id == '18' ||
                                    d.options.id == '19' ||
                                    d.options.id == '20' ||
                                    d.options.id == '21' ||
                                    d.options.id == '27' ||
                                    d.options.id == '31' ||
                                    d.options.id == '32' ||
                                    d.options.id == '39' ||
                                    d.options.id == '43' ||
                                    d.options.id == '45' ||
                                    d.options.id == '50' ||
                                    d.options.id == '51' ||
                                    d.options.id == '52' ||
                                    d.options.id == '54' ||
                                    d.options.id == '66' ||
                                    d.options.id == '71') {
                                d.hide()
                            }
                        })
                    }
                }

            },

            title: {
                text: '<?php echo $chart_title; ?>'
            },
            subtitle: {
                text: '<?php echo $chart_source; ?>'
            },

            xAxis: {
                categories: <?php echo $chart_categories; ?>,
                crosshair: true
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: '<?php echo $chart_yaxis_title; ?>',
                }
            },

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                footerFormat: '<b>County Total MOS: {point.total:,.2f}</b>',
                shared: true
            },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        y: -20,
                        verticalAlign: 'top',
                        pointWidth: 50
                    }
                }
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data; ?>,
        });
    })
</script>
