<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pager extends MX_Controller {

    public function index() {
        $yearf = $this->input->get('filter_year');
        if (empty($yearf)) {
            $data['year'] = $year = date('Y');
            $data['month'] = $month = date('M', strtotime('-1 month'));
        } else {
            $data['year'] = $year = $this->input->get('filter_year');
            $data['month'] = $month = $this->input->get('filter_month');
        }
        //redirect('manager/2pager');  
        // $data['sites'] = $this->db->get('vw_art_facilities')->result();
        $data['adult_art'] = $this->loadRegimen('ART', 'adult', $month, $year);
        $data['adult_pep'] = $this->loadRegimen('PEP', 'adult', $month, $year);
        $data['paed_pep'] = $this->loadRegimen('PEP', 'paed', $month, $year);
        $data['pmtct'] = $this->loadRegimen('PMTCT', 'adult', $month, $year);
        $data['prep'] = $this->loadRegimen('PrEP', 'prep', $month, $year);
        $data['oi'] = $this->loadRegimen('OI Only', 'oi', $month, $year);
        $data['paed_pmtct'] = $this->loadRegimen('PMTCT', 'paed', $month, $year);
        $data['paed_art'] = $this->loadRegimen('ART', 'paed', $month, $year);
        $data['uprof'] = $this->getTotals($month, $year, ['OI1A', 'OI2A', 'OI1C', 'OI2C']);
        $data['ipt'] = $this->getTotals($month, $year, ['OI4A', 'OI4C']);
        $data['flu'] = $this->getTotals($month, $year, ['OI3A', 'OI3C']);
        $data['cons'] = $this->buildPipelineConsumptionModulde();
        $main_data = $this->loadAdult2Pager($month, $year);
        $main_datac = $this->loadPaeds2Pager($month, $year);
        $data['chart_categories_adult'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        $data['chart_series_data_adult'] = json_encode(@$main_data['main'], JSON_NUMERIC_CHECK);
        $data['chart_categories_paed'] = json_encode(@$main_datac['columns'], JSON_NUMERIC_CHECK);
        $data['chart_series_data_paed'] = json_encode(@$main_datac['main'], JSON_NUMERIC_CHECK);
        $data['amc'] = $this->loadAMC($month, $year);
        $data['page_title'] = 'ART | 2-Pager';
        $this->load->view('template/dashboard_view', $data);
    }

    function buildPipelineConsumptionModulde() {
        $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $year = ['2018', '2019', '2020'];
        $columns = [];
        $query = "SELECT  P.`drug`,";
        $table = '<table class="table table-stripped table-condensed table-bordered table-hover card" id="consumedItems"><thead><tr><th>Drug</th>';
        $i = 0;
        array_push($columns, 'drug');
        foreach ($year as $y):
            foreach ($month as $m):
                $table .= '<th>' . $m . '-' . $y . '</th>';
                $query .= "SUM(
                            CASE 
                                WHEN P.`data_month`='$m' AND P.`data_year` = $y 
                                THEN P.total
                                ELSE 0 
                            END
                        ) AS '$m-$y',";
                array_push($columns, $m . '-' . $y);
            endforeach;
        endforeach;
        $table .= '</tr></thead><tbody>';



        $query .= "FROM dsh_consumption P
                    GROUP BY P.`drug`;";

        $final = str_replace(',FROM', ' FROM ', $query);

        // echo '<pre>';
        $result = $this->db->query($final)->result();



        foreach ($result as $res):
            $table .= '<tr>';

            for ($i = 0; $i < count($columns); $i++) {

                if ($res->drug == 'Lopinavir/Ritonavir (LPV/r) 80/20mg/ml Liquid') {
                    if ($i == 0) {
                        $table .= '<td style="text-align:left;">' . $res->drug . '</td>';
                    } else {
                        $table .= '<td style="text-align:center; vertical-align: middle;">' . number_format($res->$columns[$i] / 5, 0) . '</td>';
                    }
                } else {
                    if ($i == 0) {
                        $table .= '<td style="text-align:left;">' . $res->drug . '</td>';
                    } else {
                        $table .= '<td style="text-align:center; vertical-align: middle;">' . number_format($res->$columns[$i], 0) . '</td>';
                    }
                }
            }
            $table .= '</tr>';
        endforeach;

        return $table .= '</tbody></table>';
    }

    function loadAMC($month, $year) {
        $number_month = date('m', strtotime($month));
        $new_date = $year . '-' . $number_month . '-01';

        return $this->db->query("SELECT  d.name drug,d.pack_size,fn_get_dyn_consumption_average(d.id,'$new_date',d.amc_months) amc, p.issues, dst.total facility_stocks,p.close_kemsa,p.receipts_kemsa,fn_get_pending_stocks(d.id) pending_stock
                        FROM vw_drug_list d 
                        LEFT JOIN dsh_stock_totals dst ON dst.drug_id = d.id
                        LEFT JOIN vw_procurement_list p ON p.drug_id = d.id
                        WHERE dst.data_year = p.data_year
                        AND dst.data_month = p.data_month
                        AND dst.data_year='$year'
                        AND dst.data_month='$month'
                        group by d.id
                        order by d.name asc")->result();
    }

    function loadAdult2Pager($month, $year) {
        $number_month = date('m', strtotime($month));
        $new_date = $year . '-' . $number_month . '-01';

        $columns = array();

        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Facilities',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Central Store',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending',
                'yAxis' => 0,
                'data' => array()),
        );

        $results = $this->db->query("SELECT d.name drug,d.pack_size,fn_get_consumption_average(d.id,'$new_date') amc,p.close_kemsa central_stocks,dst.total facility_stock,fn_get_pending_stocks(d.id) pending_stock,
                                IF(dst.total <=0,0,ROUND((dst.total/fn_get_consumption_average(d.id,'$new_date')),1))  facility_mos,
                                IF(p.close_kemsa<=0,0,ROUND((p.close_kemsa/fn_get_consumption_average(d.id,'$new_date')),1))  central_mos,
                                IF(fn_get_pending_stocks(d.id)<=0,0,ROUND((fn_get_pending_stocks(d.id)/fn_get_consumption_average(d.id,'$new_date')),1))  pending_mos
                                FROM vw_drug_list d 
                                LEFT JOIN dsh_stock_totals dst ON dst.drug_id = d.id 
                                LEFT JOIN tbl_procurement p ON p.drug_id = d.id 
                                WHERE dst.data_year = p.transaction_year 
                                AND dst.data_month = p.transaction_month 
                                AND dst.data_year='$year' 
                                AND dst.data_month='$month' 
                                AND d.name IN (SELECT name FROM `vw_drug_list` where regimen_category like '%adult%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id in('3','8','65','44','63','38','67','69','25','31','41','60','62','98','64','61'))
                                group by d.id order by d.name asc;")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Central Store') {
                        array_push($soh_data[$index]['data'], empty($result['central_mos']) ? 0 : $result['central_mos']);
                    } else if ($soh['name'] == 'Pending') {
                        array_push($soh_data[$index]['data'], empty($result['pending_mos']) ? 0 : $result['pending_mos']);
                    } else if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], empty($result['facility_mos']) ? 0 : $result['facility_mos']);
                    }
                }
            }
        }

        return array('main' => $soh_data, 'columns' => $columns);
    }

    function loadPaeds2Pager($month, $year) {
        $number_month = date('m', strtotime($month));
        $new_date = $year . '-' . $number_month . '-01';

        $columns = array();

        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Facilities',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Central Store',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending',
                'yAxis' => 0,
                'data' => array()),
        );

        $results = $this->db->query("SELECT d.name drug,d.pack_size,fn_get_consumption_average(d.id,'$new_date') amc,p.close_kemsa central_stocks,dst.total facility_stock,fn_get_pending_stocks(d.id) pending_stock,
                                IF(dst.total <=0,0,ROUND((dst.total/fn_get_consumption_average(d.id,'$new_date')),1))  facility_mos,
                                IF(p.close_kemsa<=0,0,ROUND((p.close_kemsa/fn_get_consumption_average(d.id,'$new_date')),1))  central_mos,
                                IF(fn_get_pending_stocks(d.id)<=0,0,ROUND((fn_get_pending_stocks(d.id)/fn_get_consumption_average(d.id,'$new_date')),1))  pending_mos
                                FROM vw_drug_list d 
                                LEFT JOIN dsh_stock_totals dst ON dst.drug_id = d.id 
                                LEFT JOIN tbl_procurement p ON p.drug_id = d.id 
                                WHERE dst.data_year = p.transaction_year 
                                AND dst.data_month = p.transaction_month 
                                AND dst.data_year='$year' 
                                AND dst.data_month='$month' 
                                AND d.name IN (SELECT name FROM `vw_drug_list` where regimen_category like '%paed%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('71','2','52','30','31','32','68','15','20','19','26','69','16','17','18','21','49','43','85'))
                                group by d.id order by d.name asc;")->result_array();
        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Central Store') {
                        array_push($soh_data[$index]['data'], empty($result['central_mos']) ? 0 : $result['central_mos']);
                    } else if ($soh['name'] == 'Pending') {
                        array_push($soh_data[$index]['data'], empty($result['pending_mos']) ? 0 : $result['pending_mos']);
                    } else if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], empty($result['facility_mos']) ? 0 : $result['facility_mos']);
                    }
                }
            }
        }

        return array('main' => $soh_data, 'columns' => $columns);
    }

    function loadRegimen($regimen_service, $age_category, $data_month, $data_year) {

        if ($age_category == 'prep' || $age_category == 'oi') {
            $age = "";
        } else {
            $age = " AND age_category='$age_category'";
        }

        return $this->db->query("SELECT regimen,sum(total) patients
                                FROM dsh_patient
                                WHERE regimen_service='$regimen_service' 
                                $age
                                AND data_month='$data_month'
                                AND data_year='$data_year'
                                GROUP BY regimen
                                ORDER BY patients DESC")->result();
    }

    function getTotals($data_month, $data_year, $data = []) {
        error_reporting(0);
        $total = 0;
        foreach ($data as $d):
            $res = $this->db->query("SELECT sum(total) patients
                                FROM dsh_patient
                                WHERE data_month='$data_month'
                                AND data_year='$data_year'
                                AND regimen like '$d%' 
                                GROUP BY regimen")->result()[0]->patients;
            $total += $res;

        endforeach;
        return $total;
    }

    function getData() {
        echo json_encode(array('data' => $this->db->get('vw_art_facilities')->result_array()));
    }

    public function get_default_period() {
        $default_period = array(
            'year' => $this->config->item('data_year'),
            'month' => $this->config->item('data_month')
        );
        echo json_encode($default_period);
    }

    public function get_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_data($chartname, $selectedfilters);

        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);
        //Load chart

        $this->load->view($this->config->item($chartname . '_chartview'), $data);
        // file_put_contents('charts/'.$chartname, $html);
        //print_r($d);
    }

    public function get_filter($chartname, $selectedfilters) {
        $filters = $this->config->item($chartname . '_filters_default');
        $filtersColumns = $this->config->item($chartname . '_filters');

        if (!empty($selectedfilters)) {
            foreach (array_keys($selectedfilters) as $filter) {
                if (in_array($filter, $filtersColumns)) {
                    $filters[$filter] = $selectedfilters[$filter];
                }
            }
        }
        return $filters;
    }

    public function get_data($chartname, $filters) {

        // echo $chartname;

        if ($chartname == 'patient_scaleup_chart') {
            $main_data = $this->summary_model->get_patient_scaleup($filters);
        } else if ($chartname == 'county_drugs_chart') {
            $main_data = $this->summary_model->get_county_drug_mos($filters);
        } else if ($chartname == 'paed_drugs_chart') {
            $main_data = $this->summary_model->get_paed_drug_mos($filters);
        } else if ($chartname == 'paed_drugs_per_county_chart') {
            $main_data = $this->summary_model->get_paed_drug_mos_single($filters);
        } else if ($chartname == 'adult_drugs_per_county_chart') {
            $main_data = $this->summary_model->get_adult_drug_mos_single($filters);
        } else if ($chartname == 'patient_services_chart') {
            $main_data = $this->summary_model->get_patient_services($filters);
            //table 
        } else if ($chartname == 'all_commodity_view_table') {
            $main_data = $this->summary_model->get_all_commodity_view_table($filters);
        } else if ($chartname == 'national_mos_chart') {
            $main_data = $this->summary_model->get_national_mos($filters);
        } else if ($chartname == 'commodity_consumption_chart') {
            $main_data = $this->trend_model->get_commodity_consumption($filters);
        } else if ($chartname == 'patients_regimen_chart') {
            $main_data = $this->trend_model->get_patients_regimen($filters);
        } else if ($chartname == 'commodity_month_stock_chart') {
            $main_data = $this->trend_model->get_commodity_month_stock($filters);
        } else if ($chartname == 'county_patient_distribution_chart') {
            $main_data = $this->county_model->get_county_patient_distribution($filters);
        } else if ($chartname == 'county_commodity_soh_chart') {
            $main_data = $this->county_model->get_county_commodity_soh($filters);
        } else if ($chartname == 'county_commodity_stock_movement_table') {
            $main_data = $this->county_model->get_county_commodity_stock_movement_numbers($filters);
        } else if ($chartname == 'county_patient_distribution_table') {
            $main_data = $this->county_model->get_county_patient_distribution_numbers($filters);
        } else if ($chartname == 'subcounty_patient_distribution_chart') {
            $main_data = $this->subcounty_model->get_subcounty_patient_distribution($filters);
        } else if ($chartname == 'subcounty_commodity_soh_chart') {
            $main_data = $this->subcounty_model->get_subcounty_commodity_soh($filters);
        } else if ($chartname == 'subcounty_commodity_stock_movement_table') {
            $main_data = $this->subcounty_model->get_subcounty_commodity_stock_movement_numbers($filters);
        } else if ($chartname == 'subcounty_patient_distribution_table') {
            $main_data = $this->subcounty_model->get_subcounty_patient_distribution_numbers($filters);
        } else if ($chartname == 'facility_patient_distribution_chart') {
            $main_data = $this->facility_model->get_facility_patient_distribution($filters);
        } else if ($chartname == 'facility_commodity_soh_chart') {
            $main_data = $this->facility_model->get_facility_commodity_soh($filters);
        } else if ($chartname == 'facility_commodity_stock_movement_table') {
            $main_data = $this->facility_model->get_facility_commodity_stock_movement_numbers($filters);
        } else if ($chartname == 'facility_patient_distribution_table') {
            $main_data = $this->facility_model->get_facility_patient_distribution_numbers($filters);
        } else if ($chartname == 'partner_patient_distribution_chart') {
            $main_data = $this->partner_model->get_partner_patient_distribution($filters);
        } else if ($chartname == 'partner_patient_distribution_table') {
            $main_data = $this->partner_model->get_partner_patient_distribution_numbers($filters);
        } else if ($chartname == 'regimen_patient_chart') {
            $main_data = $this->regimen_model->get_patient_regimen_category($filters);
        } else if ($chartname == 'regimen_nrti_drugs_chart') {
            $main_data = $this->regimen_model->get_nrti_drugs_in_regimen($filters);
        } else if ($chartname == 'regimen_nnrti_drugs_chart') {
            $main_data = $this->regimen_model->get_nnrti_drugs_in_regimen($filters);
        } else if ($chartname == 'adt_sites_version_chart') {
            $main_data = $this->adt_sites_model->get_adt_sites_versions($filters);
        } else if ($chartname == 'adt_sites_internet_chart') {
            $main_data = $this->adt_sites_model->get_adt_sites_internet($filters);
        } else if ($chartname == 'adt_sites_backup_chart') {
            $main_data = $this->adt_sites_model->get_adt_sites_backup($filters);
        } else if ($chartname == 'adt_sites_distribution_chart') {
            $main_data = $this->adt_sites_model->get_adt_sites_distribution($filters);
        } else if ($chartname == 'adt_sites_distribution_table') {
            $main_data = $this->adt_sites_model->get_adt_sites_distribution_numbers($filters);
        } else if ($chartname == 'adt_reports_patients_started_art_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_patients_started_art($filters);
        } else if ($chartname == 'adt_reports_active_patients_regimen_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_patients_active_regimen($filters);
        } else if ($chartname == 'adt_reports_commodity_consumption_regimen_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_commodity_consumption_regimen($filters);
        } else if ($chartname == 'adt_reports_commodity_consumption_drug_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_commodity_consumption_drug($filters);
        } else if ($chartname == 'adt_reports_commodity_consumption_dose_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_commodity_consumption_dose($filters);
        } else if ($chartname == 'adt_reports_paediatric_weight_age_chart') {
            $main_data = $this->adt_reports_model->get_paediatric_patients_by_weight_age($filters);
        } else if ($chartname == 'adt_reports_commodity_consumption_chart') {
            $main_data = $this->adt_reports_model->get_adt_reports_commodity_consumption($filters);
        }
        return $main_data;
    }

    public function get_adt_data($table, $start_date, $end_date) {
        //Setup the elements
        $elements = array(
            'dsh_patient_adt' => array(
                'rows' => array('current_regimen'),
                'columns' => array('gender'),
                'query' => "SELECT gender, CASE WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 0 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 14 THEN '0-14' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 15 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 24 THEN '15-24' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 25 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 40 THEN '25-40' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 41 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 49 THEN '41-49' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 50 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 60 THEN '50-60' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) > 60  THEN 'Over 60' ELSE NULL END AS age, start_weight, current_weight, enrollment_date, start_regimen_date, status_change_date, start_regimen, current_regimen, service, status, CASE WHEN last_viral_test_result LIKE '%ldl%' THEN 'LDL' WHEN last_viral_test_result >= 1 AND last_viral_test_result < 1000 THEN '1-1000' WHEN last_viral_test_result >= 1000 AND last_viral_test_result < 5000 THEN '1000-5000' WHEN last_viral_test_result >= 5000 THEN 'Above 5000' ELSE NULL END AS viral_load FROM dsh_patient_adt WHERE enrollment_date >= ? AND enrollment_date <= ? "
            ),
            'dsh_visit_adt' => array(
                'rows' => array('drug'),
                'columns' => array('current_weight'),
                'query' => "SELECT gender, CASE WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 0 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 14 THEN '0-14' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 15 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 24 THEN '15-24' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 25 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 40 THEN '25-40' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 41 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 49 THEN '41-49' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) >= 50 AND ROUND(DATEDIFF(CURDATE(), birth_date)/365) <= 60 THEN '50-60' WHEN ROUND(DATEDIFF(CURDATE(), birth_date)/365) > 60  THEN 'Over 60' ELSE NULL END AS age, pv.current_weight, purpose, last_regimen, pv.current_regimen, regimen_change_reason, dispensing_date, appointment_date, appointment_adherence, non_adherence_reason, drug, dose, duration, pill_count_adherence, self_reporting_adherence  FROM dsh_visit_adt pv  INNER JOIN dsh_patient_adt p ON p.id = pv.patient_adt_id WHERE dispensing_date >= ? AND dispensing_date <= ? "
            )
        );
        //Get elements based on selected options
        $response['data'] = $this->db->query($elements[$table]['query'], array($start_date, $end_date))->result_array();
        $response['defs'] = array('rows' => $elements[$table]['rows'], 'cols' => $elements[$table]['columns']);
        echo json_encode($response);
    }

}
