<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>'

        Highcharts.setOptions({
            global: {
                useUTC: false,

            },
            lang: {
                decimalPoint: '.',
                thousandsSep: ','
            }
        });

        Highcharts.chart(chartDIV, {
            chart: {
                type: 'bar',
                //marginBottom: 2,
                //marginLeft: 80,
                height: 500
            },
            title: {
                text: '<?php echo $chart_title; ?>'
            },
            subtitle: {
                text: '<?php echo $chart_source; ?>'
            },
            credits: false,
            xAxis: {
                categories: <?php echo $chart_categories; ?>,
                crosshair: true
            },
            yAxis: [{
                    min: 0,
                    max: 9,
                    tickInterval: 1,
                    title: {
                        text: 'Months of Stock'
                    }
                }, {
                    min: 0,
                    max: 9,
                    tickInterval: 1,
                    opposite: true, //optional, you can have it on the same side.
                    title: {
                        text: 'Months of Stock'
                    },
                    plotLines: [{
                            value: 3,
                            color: 'red',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            x: -10,
                            label: {
                                text: 'Under Stocked'
                            }
                        }, {
                            value: 6,
                            color: 'red',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            label: {
                                text: 'Over Stocked'
                            }
                        }],
                }],
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '<span style="color:{series.color}"><b>MOS</span>: {point.y}</b><br/>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    colorByPoint: true,
                    pointWidth: 50,
                    dataLabels: {
                        enabled: true,
                        pointWidth: 80
                    }
                },
            },
            series: [{
                    showInLegend: false,
                    colorByPoint: true,
                    data: <?php echo $chart_series_data; ?>
                }],
        });

    });
</script>        