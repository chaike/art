<?php
$dyn_table = "<table class='table table-bordered table-condensed table-hover table-striped " . $chart_name . "_distribution_table'>";
$thead = "<thead><tr>";
$table_data = json_decode($chart_series_data, TRUE);
$count = 0;
$tbody = "<tbody>";
foreach ($table_data as $row_data) {
    $tbody .= "<tr>";
    foreach ($row_data as $key => $value) {
        //Header
        if ($count == 0) {
            $thead .= "<th>" . strtoupper(str_ireplace('_', ' ', $key)) . "</th>";
        }
        //Body
        if (gettype($value) == 'string') {
            $tbody .= "<td>" . ucwords($value) . "</td>";
        } else {
            $tbody .= "<td>" . number_format($value) . "</td>";
        }
    }
    $tbody .= "</tr>";
    $count++;
}
$thead .= "</tr></thead>";
$tbody .= "</tbody>";
$dyn_table .= $thead;
$dyn_table .= $tbody;
$dyn_table .= "</table>";
echo $dyn_table;
?>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>
<script src="<?php echo base_url() . 'public/manager/js/dataTables.buttons.min.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/buttons.flash.min.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/jszip.min.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/pdfmake.min.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/vfs_fonts.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/buttons.html5.min.js'; ?>"></script> 
<script src="<?php echo base_url() . 'public/manager/js/buttons.print.min.js'; ?>"></script>
<script type="text/javascript">
    $(function () {
        var table_class = "<?php echo $chart_name . '_distribution_table'; ?>";
        //DataTable
        var table = $('.' + table_class).DataTable({
            "bDestroy": true,
            "pagingType": "full_numbers",
            "ordering": false,
            "responsive": true,
            "buttons": [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            initComplete: function () {
                this.api().columns([1, 3]).every(function () {
                    var column = this;
                    var select = $('<br/><select><option class="form-control" width="200px;" value="">Show all</option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var val = $('<div/>').html(d).text();
                        select.append('<option value="' + val + '">' + val + '</option>');
                    });
                });
            }
        });

    });
</script>