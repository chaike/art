<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid"> 
        <div class="navbar-header"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <span class="glyphicon glyphicon-dashboard"></span>
            </a>
            <a class="navbar-brand" href="#">ART COMMODITY DASHBOARD</a>
        </div> 
        <nav class="collapse navbar-collapse" id="filter-navbar">
            <input type="hidden" name="filter_tab" id="filter_tab" value="" />
            <ul class="nav navbar-nav navbar-right" id="main_tabs">
                <li><a href="<?php echo base_url();?>manager/2pager">Stock Summary</i></a></li>
<!--                <li><a href="<?php echo base_url();?>main">Analytics</a></li>-->
                
                <?php if ($this->session->userdata('id')) { ?>
                    <li><a href="#summary" aria-controls="summary" role="tab" class="other" data-toggle="tab">Summary</a></li>                
                    <li><a href="#trend" aria-controls="trend" role="tab" data-toggle="tab" class="other">Trend</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            County/Subcounty
                            <span class="caret" class="other"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#county" aria-controls="county" role="tab" data-toggle="tab" class="other">County</a></li>
                            <li><a href="#subcounty" aria-controls="subcounty" role="tab" data-toggle="tab" class="other">Subcounty</a></li>
                        </ul>
                    </li>
                    <li><a href="#facility" aria-controls="facility" role="tab" data-toggle="tab" class="other">Facility</a></li>
                    <li><a href="#partner" aria-controls="partner" role="tab" data-toggle="tab" class="other">Partner</a></li>
                    <li><a href="#regimen" aria-controls="regimen" role="tab" data-toggle="tab" class="other">Regimen</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            ADT
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#adt_sites" aria-controls="adt_sites" role="tab" data-toggle="tab" class="other">Sites</a></li>
                            <li><a href="#adt_analytics" aria-controls="adt_analytics" role="tab" data-toggle="tab" class="other">Analytics</a></li>
                            <li><a href="#adt_reports" aria-controls="adt_reports" role="tab" data-toggle="tab" class="other">Reports</a></li>
                            <li><a href="#adt_data_warehouse" aria-controls="adt_data_warehouse" role="tab" data-toggle="tab" class="other">Data Warehouse</a></li>
                        </ul>
                    </li>


                <?php } ?>
                <li> 
                    <a style="border:3px solid white; font-weight: bold;" class="btn btn-warning btn-sm" href="<?= base_url(); ?>manager/dashboard"><i class="fa fa-dashboard"> Allocation Tool Login</i></a>
                </li>
            </ul> 
        </nav> 
    </div>
</div>
<script>
    $(function () {
        $('#analytics').click(function () {
            $('.FILTER_VIEW').hide();
        })
        $('.other').click(function () {
            $('.FILTER_VIEW').show();
        })
    })
</script>