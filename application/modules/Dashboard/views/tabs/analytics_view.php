<style>
    .iframe-container {
        overflow: hidden;
        padding-top: 56.25%;
        position: relative;
    }

    .iframe-container iframe {
        border: 0;
        height: 100%;
        left: 0;
        position: relative;
        top: 0;
        width: 100%;
    }

    /* 4x3 Aspect Ratio */
    .iframe-container-4x3 {
        padding-top: 75%;
    }
    iframe{
        display: block;
        border: none;         /* Reset default border */
        height: 100vh;        /* Viewport-relative units */
        width: calc(100% + 17px);
        overflow-x:hidden;
    }


</style>

<div role="tabpanel" class="tab-pane " id="analytics_view">
    <div class="">
        <div class="container-fluid" id="MAINFRAME">
            <strong>Loading Page.....</strong>
        </div>
    </div>
</div>





