<div role="tabpanel" class="tab-pane  fade in active " id="stock_summary">
    <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="chart-wrapper">
                            <div class="chart-title">
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>ADULT DRUG MOS PER COUNTY </strong><span class="MONTHLY_TITLE"></span>
                                    </div>
                                    <div class="col-md-9">
        
                                    </div>
                                </div>
                            </div>
                            <div class="chart-stage">
                                <div id="county_drugs_chart"></div>
                            </div>
        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        
                        <div class="chart-wrapper">
                            <div class="chart-title">
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>PAED DRUG MOS PER COUNTY</strong> <span class="MONTHLY_TITLE"></span>
                                    </div>
                                    <div class="col-md-9">
        
                                    </div>
                                </div>
                            </div>
                            <div class="chart-stage">
                                <div id="paed_drugs_chart"></div>
                            </div>
        
                        </div>
                    </div>            
        
                </div>
        
                <div class="row">
        
                    <div class="col-sm-12">
                        <div class="chart-wrapper">
                            <div class="chart-title">
                                <strong>PAED DRUG MOS PER COUNTY</strong> <span class="MONTHLY_TITLE"></span> 
                                <div class="nav navbar-right">
                                    <button data-toggle="modal" data-target="#paed_drugs_per_county_chart_filter_modal" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-filter"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="chart-stage">
                                <div id="paed_drugs_per_county_chart"></div>
                            </div>
                            <div class="chart-notes">
                                <span class="paed_drugs_per_county_chart_heading heading"></span>
                            </div>
                        </div>
                    </div>
                </div>
        
        
              
                <div class="modal fade" id="paed_drugs_per_county_chart_filter_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title"><strong>PAED COUNTY MONTHS OF STOCK (MOS) FILTER</strong></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <select id="paed_drugs_per_county_chart_filter" multiple="multiple" data-filter_type="drug"></select>
                                    </div>
                                    <div class="col-sm-3">
                                        <button id="paed_drugs_per_county_chart_clear_btn" class="btn btn-danger btn-sm clear_btn"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
                                        <button id="paed_drugs_per_county_chart_filter_btn" class="btn btn-warning btn-sm filter_btn"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-sm-12">
                        <div class="chart-wrapper">
                            <div class="chart-title">
                                <strong>ADULT DRUG MOS PER COUNTY</strong> <span class="MONTHLY_TITLE"></span>
                                <div class="nav navbar-right">
                                    <button data-toggle="modal" data-target="#adult_drugs_per_county_chart_filter_modal" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-filter"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="chart-stage">
                                <div id="adult_drugs_per_county_chart"></div>
                            </div>
                            <div class="chart-notes">
                                <span class="adult_drugs_per_county_chart_heading heading"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="adult_drugs_per_county_chart_filter_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title"><strong>ADULT COUNTY MONTHS OF STOCK (MOS) FILTER</strong></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <select id="adult_drugs_per_county_chart_filter" multiple="multiple" data-filter_type="drug"></select>
                                    </div>
                                    <div class="col-sm-3">
                                        <button id="adult_drugs_per_county_chart_clear_btn" class="btn btn-danger btn-sm clear_btn"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
                                        <button id="adult_drugs_per_county_chart_filter_btn" class="btn btn-warning btn-sm filter_btn"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




        <div class="modal fade" id="all_commodity_view_table_filter_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title"><strong>STOCK STATUS</strong></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-9">
                                <select id="all_commodity_view_table_filter" multiple="multiple" data-filter_type="drug"></select>
                            </div>
                            <div class="col-sm-3">
                                <button id="all_commodity_view_table_filter_clear_btn" class="btn btn-danger btn-sm clear_btn"><span class="glyphicon glyphicon-refresh"></span> Reset</button>
                                <button id="all_commodity_view_table_filter_btn" class="btn btn-warning btn-sm filter_btn"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                <div class="chart-wrapper">
                    <div class="chart-title">
                        <div class="row">
                            <div class="col-md-12">
                                <strong>STOCK STATUS</strong> <span class="MONTHLY_TITLE"></span>
                                <div class="nav navbar-right">
                                    <button data-toggle="modal" data-target="#all_commodity_view_table_filter_modal" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-filter"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-9">

                            </div>
                        </div>
                    </div>
                    <div class="chart-stage">
                        <div id="all_commodity_view_table"></div>
                    </div>

                </div>
            </div>            

        </div>


    </div>
</div>
<script>
    $(function () {
        $('#summary').hide()
        $('.FILTER_VIEW').hide();
    })
</script>