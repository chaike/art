<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/dompdf/autoload.inc.php';

use Dompdf\Dompdf;

class Manager extends MX_Controller {

    public $Cache;

    public function __construct() {
        parent::__construct();
        $this->load->model('Orders_model');
        $this->load->model('Manager_model');
        $client = new PDO('mysql:dbname=art_cache_database;host=127.0.0.1', 'java', 'java');
        $this->Cache = new \MatthiasMullie\Scrapbook\Adapters\MySQL($client);
    }

    function cacheManager() {
        $data = $this->input->post('twopager');
        if (empty($this->Cache->get('2pager'))) {
            $this->Cache->add('2pager', $data, 60);
        }
    }

    public function index() {
        $data['title'] = 'Login';
        $this->load_page('user', 'login', $data);
    }

    public function load_page($module = 'user', $page = 'login', $title = 'Login') {
        if ($page == 'register') {
            $this->db->where_in('name', array('subcounty', 'county'));
            $data['roles'] = $this->db->get('tbl_role')->result_array();
        }

        if ($page == 'minute') {
            $data['minutes'] = $this->db->where('meeting_id', $this->uri->segment(4))->get('tbl_minutes')->result();
        }
        $data['page_title'] = 'ART | ' . $title;
        $this->load->view('pages/' . $module . '/' . $page . '_view', $data);
    }

    public function load_template_dashboar($module = 'manager', $page = '2pager', $title = '2 Pager Dashboard', $is_table = FALSE) {
        $data['page_name'] = $page;
        $data['content_view'] = 'pages/' . $module . '/' . $page . '_view';
        if ($is_table) {

            $data['columns'] = $this->db->list_fields('tbl_' . $page);
            $data['content_view'] = 'template/table_view';
        } else {

            $data['page_title'] = 'ART | ' . ucwords($title);
            if (!empty($this->Cache->get('2pager'))) {
                echo $this->Cache->get('2pager');
            } else {
                $this->load->view('template/template_view_1', $data);
            }
        }
    }

    function getReceived($id) {
        echo json_encode($this->db->query("SELECT * FROM tbl_deliveries WHERE contract_id='$id'")->result());
    }

    public function load_template($module = 'dashboard', $page = 'dashboard', $title = 'Dashboard', $is_table = TRUE) {


        if ($page == 'hepb') {
            $this->session->set_userdata('hepb', 'hepb');
        }

        $min_id = $this->uri->segment(5);
        $this->session->set_userdata('minute', '');



        if ($this->session->userdata('id')) {
            $data['page_name'] = $page;
            $data['content_view'] = 'pages/' . $module . '/' . $page . '_view';
            if ($is_table) {

                if ($page == 'mos') {
                    $columns = [
                        0 => 'ID',
                        1 => 'DRUG NAME',
                        2 => 'PACK SIZE',
                        3 => 'STOCK ON HAND (SOH)',
                        4 => 'MONTHS OF STOCK (MOS)',
                    ];
                    $data['columns'] = $columns;
                } else {
                    $data['columns'] = $this->db->list_fields('tbl_' . $page);
                }
                $data['content_view'] = 'template/table_view';
            }

            if ($module == 'orders') {
                $columns = array(
                    'reports' => array(
                        'subcounty' => array('Facility Name', 'Period Beginning', 'Description', 'Status', 'Actions'),
                        'county' => array('Facility Name', 'Period Beginning', 'Description', 'Subcounty', 'Status', 'Actions'),
                        'nascop' => array('Facility Name', 'Period Beginning', 'Description', 'County', 'Subcounty', 'Status', 'Actions'),
                        'partner' => array('Facility Name', 'Period Beginning', 'Description', 'County', 'Subcounty', 'Status', 'Actions')
                    ),
                    'reporting_rates' => array(
                        'subcounty' => array('MFL Code', 'Facility Name', 'Status', 'Description', 'Period', 'Actions'),
                        'county' => array('Subcounty', 'Submitted', 'Progress'),
                        'nascop' => array('County', 'Submitted', 'Progress'),
                        'partner' => array('County', 'Submitted', 'Progress')
                    ),
                    'cdrr_maps' => array(
                        'subcounty' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'county' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'nascop' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'partner' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        )
                    ),
                    'allocation' => array(
                        'subcounty' => array('MFL Code', 'Facility Name', 'Period', 'Description', 'Status', 'Actions'),
                        'county' => array('Period', 'Approved', 'Status', 'Actions'),
                        'partner' => array('Period', 'Approved', 'Status', 'Actions'),
                        'nascop' => array('Period', 'Submitted to KEMSA', 'Status', 'Actions')
                    ),
                    'allocate' => array(
                        'subcounty' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'county' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'partner' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        ),
                        'nascop' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_cdrr_data($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'pcdrrs' => $this->Orders_model->get_cdrr_data_previous($this->uri->segment('4'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'maps' => $this->Orders_model->get_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role')),
                            'previousmaps' => $this->Orders_model->get_previous_maps_data($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'))
                        )
                    ),
                    'edit_allocation' => array(
                        'subcounty' => array(),
                        'county' => array('Subcounty', 'Report Count', 'Status', 'Approval', 'Actions'),
                        'partner' => array('Subcounty', 'Report Count', 'Status', 'Approval', 'Actions'),
                        'nascop' => array('County', 'Report Count', 'Status', 'Approval', 'Actions')
                    ),
                    'subcounty_reports' => array(
                        'subcounty' => array(),
                        'county' => array('MFL Code', 'Facility Name', 'Description', 'Status', 'Period', 'Actions'),
                        'partner' => array('MFL Code', 'Facility Name', 'Description', 'Status', 'Period', 'Actions'),
                        'nascop' => array('MFL Code', 'Facility Name', 'Description', 'Status', 'Period', 'Actions')
                    ),
                    'county_reports' => array(
                        'subcounty' => array(),
                        'county' => array(),
                        'partner' => array(),
                        'nascop' => array('SubCounty', 'MFL Code', 'Facility Name', 'Description', 'Status', 'Period', 'Actions')
                    ),
                    'satellites' => array(
                        'subcounty' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_satellite_cdrr($this->uri->segment('4')),
                            'maps' => $this->Orders_model->get_satellite_maps($this->uri->segment('5'))
                        ),
                        'county' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_satellite_cdrr($this->uri->segment('4')),
                            'maps' => $this->Orders_model->get_satellite_maps($this->uri->segment('5'))
                        ),
                        'nascop' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_satellite_cdrr($this->uri->segment('4')),
                            'maps' => $this->Orders_model->get_satellite_maps($this->uri->segment('5'))
                        ),
                        'partner' => array(
                            'drugs' => $this->Orders_model->get_drugs(),
                            'regimens' => $this->Orders_model->get_regimens(),
                            'cdrrs' => $this->Orders_model->get_satellite_cdrr($this->uri->segment('4')),
                            'maps' => $this->Orders_model->get_satellite_maps($this->uri->segment('5'))
                        )
                    )
                );

                $data['columns'] = $columns[$page][$this->session->userdata('role')];
                $data['county'] = $this->getCountySubcounty();
                //$data['data_maps'] = $this->Orders_model->get_maps_data_patients_against_regimen($this->uri->segment('5'), $this->session->userdata('scope'), $this->session->userdata('role'));
                $data['role'] = $this->session->userdata('role');
                $data['scope'] = $this->session->userdata('scope');
                $data['cdrr_id'] = $this->uri->segment('4');
                $data['map_id'] = $this->uri->segment('5');
                $data['seg_4'] = $this->uri->segment('4');
                $data['seg_5'] = $this->uri->segment('5');
                $data['seg_6'] = $this->uri->segment('6');
            }

            if ($page == 'minute') {
                $data['topaob'] = $this->db->select('start,aob')->where('meeting_id', $min_id)->get("tbl_minutes")->result();
            }
            $data['to_procure'] = $this->db->query("SELECT * FROM vw_stocks_purchases")->result();

            $data['page_title'] = 'ART | ' . ucwords($title);



           redirect("manager/login");
        } else if ($page == '2pager' && !$this->session->userdata('id')) {
            $data['page_name'] = $page;
            if (empty($this->Cache->get('procu_out'))) {
                $tablespan = $this->db->query("SELECT * FROM vw_stocks_purchases")->result();
                for ($i = 0; $i < count($tablespan); $i++):
                    $table .= '<tr>';
                    $table .= '<td class="tg-0lax" style="font-weight:bold !important;">' . $tablespan[$i]->item . '</td>';
                    $table .= '<td class="tg-0lax" style="text-align:right !important;">' . number_format($tablespan[$i]->qty_to_procure) . '</td>
                                <td class="tg-0lax  style="text-align:right !important;">' . $tablespan[$i]->GF . '</td>
                                <td class="tg-0lax" style="text-align:right !important;">' . $tablespan[$i]->USAID . '</td>
                                <td class="tg-0lax" style="text-align:right !important;">' . $tablespan[$i]->CPF . '</td>';
                    $table .= '</tr>';
                endfor;
                $this->Cache->add('procu_out', $table);
                $data['tbody'] = $table;
            } else {
                $data['tbody'] = $this->Cache->get('procu_out');
            }
            $data['content_view'] = 'pages/' . $module . '/' . $page . '_view';
            if ($is_table) {
                $data['columns'] = $this->db->list_fields('tbl_' . $page);
                $data['content_view'] = 'template/table_view';
            }
            redirect("manager/login");
        } else {
            redirect("manager/login");
        }
    }

    function getRegimenDetails() {
        echo $this->response($this->Orders_model->get_maps_data_patients_against_drugs());
    }

    public function get_default_period() {
        $default_period = array(
            'year' => '2020', // $this->config->item('data_year'),
            'month' => 'Feb', //$this->config->item('data_month'),
            'drug' => $this->config->item('drug')
        );
        echo json_encode($default_period);
    }

    public function get_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Set filters based on role and scope
        $role = str_ireplace('subcounty', 'sub_county', $this->session->userdata('role'));
        if (!in_array($role, array('admin', 'nascop'))) {
            $selectedfilters[$role] = $this->session->userdata('scope_name');
        }
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_data($chartname, $selectedfilters);
        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);
        //Load chart
        $this->load->view($this->config->item($chartname . '_chartview'), $data);
    }

    public function get_hepb_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Set filters based on role and scope
        $role = str_ireplace('subcounty', 'sub_county', $this->session->userdata('role'));
        if (!in_array($role, array('admin', 'nascop'))) {
            $selectedfilters[$role] = $this->session->userdata('scope_name');
        }
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_hb_data($chartname, $selectedfilters);
        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);
        //Load chart
        $this->load->view($this->config->item($chartname . '_chartview'), $data);
    }

    public function get_2pager_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Set filters based on role and scope
        $role = str_ireplace('subcounty', 'sub_county', $this->session->userdata('role'));
        if (!in_array($role, array('admin', 'nascop'))) {
            $selectedfilters[$role] = $this->session->userdata('scope_name');
        }
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_2pager_data($chartname, $selectedfilters);
        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);

        //Load chart
        $this->load->view($this->config->item($chartname . '_chartview'), $data);
    }

    public function get_two_pager_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Set filters based on role and scope
        $role = str_ireplace('subcounty', 'sub_county', $this->session->userdata('role'));
        if (!in_array($role, array('admin', 'nascop'))) {
            $selectedfilters[$role] = $this->session->userdata('scope_name');
        }
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_two_pager_data($chartname, $selectedfilters);
        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);
        //Load chart
        $this->load->view($this->config->item($chartname . '_chartview'), $data);
    }

    public function get_filter($chartname, $selectedfilters) {
        $filters = $this->config->item($chartname . '_filters_default');
        $filtersColumns = $this->config->item($chartname . '_filters');

        if (!empty($selectedfilters)) {
            foreach (array_keys($selectedfilters) as $filter) {
                if (in_array($filter, $filtersColumns)) {
                    $filters[$filter] = $selectedfilters[$filter];
                }
            }
        }
        return $filters;
    }

    function generateMinute() {
        $data['minutes'] = $this->db->where('meeting_id', $this->uri->segment(4))->get('tbl_minutes')->result();
        $page_builder = $this->load->view('pages/public/pdf_view', $data, true);
        $dompdf = new Dompdf;
        // Load HTML content
        $dompdf->loadHtml($page_builder);
        $dompdf->set_option('isHtml5ParserEnabled', true);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
        $output = $dompdf->output();
        unlink('public/minutes_pdf/minutes.pdf');
        file_put_contents('public/minutes_pdf/minutes.pdf', $output);
    }

    public function newAccountRequestForOpening() {
        $config['mailtype'] = 'html';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = stripslashes('webartmanager2018@gmail.com');
        $config['smtp_pass'] = stripslashes('WebArt_052013');

        $requester = $this->input->post('name');
        $reason = $this->input->post('message');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $county = $this->input->post('county');
        $subcounty = $this->input->post('subcounty');

        if (empty($reason) || empty($requester) || empty($phone) || empty($email)) {
            echo json_encode(['fail' => 'fail']);
        } else {

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('noreply@nascop.org', 'Commodity Manager');
            $this->email->to('webartmanager2018@gmail.com,eskechi@gmail.com,carolasin88@gmail.com');
            $this->email->cc('kmugambi@clintonhealthaccess.org,pkimani@clintonhealthaccess.org,alpho07@gmail.com');
            $this->email->subject('Commodity Manager | New Account Creation Request');
            $this->email->message('Dear NASCOP Commodity Manager <br> ' . $requester . ' is seeking to have access to the commodity platform for the reason stated below.<br>'
                    . ' <strong>' . $reason . '</strong><br>'
                    . ' County:' . $county . '<br>'
                    . ' Subcounty:' . $subcounty . '<br>'
                    . ' Phone: ' . $phone . '<br> '
                    . ' Email: ' . $email);

            if ($this->email->send()) {
                echo json_encode(['status' => 'success']);
            } else {
                echo json_encode(['status' => 'fail']);
            }
        }
    }

    function getFaq() {
        redirect(base_url() . 'public/manager/Allocation_FAQs.pdf');
    }

    function general() {
        $this->manager_model->createExcel();
    }

    function regcat() {
        $this->manager_model->createExcelLevel2();
    }

    function linegroup() {
        $this->manager_model->createExcelLevel3();
    }

    public function get_data($chartname, $filters) {
        if ($chartname == 'reporting_rates_chart') {
            $main_data = $this->manager_model->get_reporting_rates($filters);
        } else if ($chartname == 'patients_by_regimen_chart') {
            $main_data = $this->manager_model->get_patient_regimen($filters);
        } else if ($chartname == 'drug_consumption_allocation_trend_chart') {
            $main_data = $this->manager_model->get_drug_consumption_allocation_trend($filters);
        } else if ($chartname == 'stock_status_trend_chart') {
            $main_data = $this->manager_model->get_drug_soh_trend($filters);
        } else if ($chartname == 'low_mos_commodity_table') {
            $main_data = $this->manager_model->get_low_mos_commodities($filters);
        } else if ($chartname == 'high_mos_commodity_table') {
            $main_data = $this->manager_model->get_high_mos_commodities($filters);
        }
        return $main_data;
    }

    public function get_hb_data($chartname, $filters) {

        if ($chartname == 'reporting_rates_chart') {
            $main_data = $this->manager_model->get_reporting_rates($filters);
        } else if ($chartname == 'patients_by_regimen_chart') {
            $main_data = $this->manager_model->get_patient_regimen_hb($filters);
        } else if ($chartname == 'stock_status_trend_hb_chart') {
            $main_data = $this->manager_model->get_drug_soh_trend_hb($filters);
        } else if ($chartname == 'low_mos_commodity_table') {
            $main_data = $this->manager_model->get_low_mos_commodities_hb($filters);
        } else if ($chartname == 'high_mos_commodity_table') {
            $main_data = $this->manager_model->get_high_mos_commodities_hb($filters);
        }
        return $main_data;
    }

    public function get_2pager_data($chartname, $filters) {

        if ($chartname == 'mos_adult_chart') {
            $main_data = $this->manager_model->get_adult_mos_2pager($filters);
        } else if ($chartname == 'mos_paeds_chart') {
            $main_data = $this->manager_model->get_paeds_mos_2pager($filters);
        } else if ($chartname == 'forecast_table') {
            $main_data = $this->manager_model->get_2pager_forecast($filters);
        } else if ($chartname == 'supplier_chart') {
            $main_data = $this->manager_model->get_2pager_supplier($filters);
        } else if ($chartname == 'mos_fpaeds_chart') {
            $main_data = $this->manager_model->get_facility_paeds_mos_2pager($filters);
        } else if ($chartname == 'mos_fadult_chart') {
            $main_data = $this->manager_model->get_facility_adult_mos_2pager($filters);
        } else if ($chartname == 'mos_county_chart') {
            $main_data = $this->manager_model->get_county_mos_2pager($filters);
        } else if ($chartname == 'facility_mos_paeds_chart') {
            $main_data = $this->manager_model->get_facility_main_paeds_mos_2pager($filters);
        } else if ($chartname == 'facility_mos_adult_chart') {
            $main_data = $this->manager_model->get_facility_main_adult_mos_2pager($filters);
        } else if ($chartname == 'two_pager_adult_chart') {
            $main_data = $this->manager_model->get_two_pager_adult($filters);
        } else if ($chartname == 'two_pager_paeds_chart') {
            $main_data = $this->manager_model->get_two_pager_paeds($filters);
        } else if ($chartname == 'two_pager_oi_chart') {
            $main_data = $this->manager_model->get_two_pager_oi($filters);
        }
        return $main_data;
    }

    public function excel_export_regimen() {
        if ($_GET['year'] != '' or $_GET['month'] != '') {
            $year = $_GET['year'];
            $month = $_GET['month'];
        } else {
            $year = $this->config->item('data_year');
            $month = $this->config->item('data_month');
        }
        $data['year'] = $year;
        $data['month'] = $month;
        $data['units'] = $this->Manager_model->get_units($year, $month);
        $data['categories'] = $this->Manager_model->get_categories($year, $month);
        $data['sub_categories'] = $this->Manager_model->get_sub_categories($year, $month);
        $this->load->view('pages/dashboard/excel/regimen_chart_table', $data);
    }

    function getSuppliers() {
        $suppliers = '';
        $supp = $this->db->get('tbl_supplier')->result();
        foreach ($supp as $s):
            $suppliers .= '<option value="' . strtoupper($s->name) . '">' . strtoupper($s->name) . '</option>';
        endforeach;
        return $suppliers;
    }

    function getFunders() {
        $funds = '';
        $fund = $this->db->get('tbl_funding_agent')->result();
        foreach ($fund as $f):
            $funds .= '<option value="' . strtoupper($f->name) . '">' . strtoupper($f->name) . '</option>';
        endforeach;
        return $funds;
    }

    function saveProcurementData($drug_id, $year, $month) {
        $old_trans_id = $this->input->post('old_trans_id');
        $this->db->where('trans_id', $old_trans_id)->delete('tbl_funding_agent_amount');
        $this->db->where('trans_id', $old_trans_id)->delete('tbl_contracted_qty');
        $this->db->where('trans_id', $old_trans_id)->delete('tbl_funding_suppier_qty');
        $this->db->where('trans_id', $old_trans_id)->delete('tbl_calldown_received_qty');

        $key_ = sha1(date('Y-m-d H:i:s') . date('s:i:H Y-d-m'));

        $proposedYear = $this->input->post('proposed_year');
        $proposedMonth = $this->input->post('proposed_month');
        $proposedQty = $this->input->post('proposed_qty');

        $fundingAgent = $this->input->post('funding');
        $fundingQty = $this->input->post('quantity');


        $contractedYear = $this->input->post('contacting_year');
        $contractedMonth = $this->input->post('contracting_month');
        $contractedQty = $this->input->post('contracting_qty');

        $funder = $this->input->post('fund');
        $contractor = $this->input->post('contractor');
        $Qty = $this->input->post('contracted_qty');

        $calldMonth = $this->input->post('calldown_month');
        $callQty = $this->input->post('calldown_qty');

        $recMonth = $this->input->post('received_month');
        $recQty = $this->input->post('received_qty');

        $unique = $this->input->post('hash_id');
        $unique_child = $this->input->post('hash_id_child');

        $comments = $this->input->post('commentp');

        $this->db->where('year', $year)->where('month', $month)->where('drug_id', $drug_id)->update('tbl_procurement_item', ['proposed' => $proposedQty, 'trans_id' => $key_, 'comments' => $comments]);

        $this->db->where('year', $year)->where('month', $month)->where('drug_id', $drug_id)->delete('tbl_funding_agent_amount');
        for ($a = 0; $a < count($fundingAgent); $a++) {
            $this->db->insert('tbl_funding_agent_amount', [
                'trans_id' => $key_,
                'drug_id' => $drug_id,
                'year' => $proposedYear,
                'month' => $proposedMonth,
                'agent' => $fundingAgent[$a],
                'amount' => $fundingQty[$a]
            ]);
        }

        $this->db->where('year', $contractedYear)->where('month', $contractedMonth)->where('drug_id', $drug_id)->update('tbl_procurement_item', ['contracted' => $contractedQty]);

        $this->db->where('year', $contractedYear)->where('month', $contractedMonth)->where('drug_id', $drug_id)->delete('tbl_contracted_qty');

        $this->db->insert('tbl_contracted_qty', [
            'trans_id' => $key_,
            'drug_id' => $drug_id,
            'year' => $contractedYear,
            'month' => $contractedMonth,
            'amount' => $contractedQty
        ]);


        $this->db->where('year', $contractedYear)->where('month', $contractedMonth)->where('drug_id', $drug_id)->delete('tbl_funding_suppier_qty');
        for ($b = 0; $b < count($funder); $b++) {
            $this->db->insert('tbl_funding_suppier_qty', [
                'trans_id' => $key_,
                'drug_id' => $drug_id,
                'year' => $contractedYear,
                'month' => $contractedMonth,
                'funder' => $funder[$b],
                'supplier' => $contractor[$b],
                'amount' => $Qty[$b],
                'unique_id' => $unique[$b],
            ]);
        }


        for ($c = 0; $c < count($calldMonth); $c++) {
            $this->db->insert('tbl_calldown_received_qty', [
                'trans_id' => $key_,
                'drug_id' => $drug_id,
                'cyear' => $contractedYear,
                'ryear' => $contractedYear,
                'cmonth' => $calldMonth[$c],
                'rmonth' => $recMonth[$c],
                'cqty' => $callQty[$c],
                'rqty' => $recQty[$c],
                'unique_child' => $unique_child[$c]
            ]);
        }

        $this->db->where('year', $contractedYear)->where('month', $contractedMonth)->where('drug_id', $drug_id)->update('tbl_procurement_item', ['contracted' => $contractedQty]);


        echo json_encode(['status' => 'success']);
    }

    function loadMainHash($year, $month, $drug) {
        $ref = $this->db->where('drug_id', $drug)->where('year', $year)->where('month', $month)->get('tbl_procurement_item')->result();
        $hash = $ref[0]->trans_id;
        $funding_agents = $this->db->where('trans_id', $hash)->get('tbl_funding_agent_amount')->result();
        $contracted = $this->db->where('trans_id', $hash)->get('tbl_contracted_qty')->result();
        //$suppliers = $this->db->where('trans_id', $hash)->get('tbl_funding_suppier_qty')->result();

        echo json_encode([
            'proposed' => $ref,
            'funding_agents' => $funding_agents,
            'contracted' => $contracted,
            'calldown_body' => $this->tableBuilder($hash)
        ]);
    }

    function tableBuilder($hash) {
        $suppliers = $this->db->where('trans_id', $hash)->get('tbl_funding_suppier_qty')->result();
        $table = '';
        $counter = 1;
        foreach ($suppliers as $sup):

            $table .= '
    <table class="table table-striped" id="levelOne">
    <thead>
        <tr>
            <th>Fund Source</th>
            <th>Contractor</th>
            <th>Quantity</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <select class="form-control  form-group FAgent" name="fund[]">
                    <option value="' . $sup->funder . '">' . $sup->funder . '</option>' . $this->getFunders() . '                    
                </select>
            </td>
            <td>
                <select class="form-control form-group FSupplier" name="contractor[]">
                    <option value="' . $sup->supplier . '">' . $sup->supplier . '</option>' . $this->getSuppliers() . ' 
                </select>
            </td>
            <td>
                <input type="number" class="form-control form-group" name="contracted_qty[]" value="' . $sup->amount . '" placeholder="Quantity">

            </td>
            <td><input type="hidden" class="form-control form-group HASH_" name="hash_id[]" value="' . $sup->unique_id . '">';
            if ($counter == 1) {
                $table .= '<a href="#" class="btn btn-sm btn-primary pull-right" id="levelOneAdd"><i class="fa fa-plus-circle" title="Add"></i></a>';
            } else {
                $table .= '<a href="#rem" title="Remove Section" class="btn btn-sm btn-danger levelOneRem"><i class="fa fa-minus"></i></a>';
            }
            $table .= '</td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>CallDown</th>
                            <th></th>
                            <th>Receipt</th>
                            <th></th>
                            <th><a href="#" class="btn btn-sm btn-success pull-right levelTwoAdd"><i class="fa fa-plus-circle" title="Add"></i></a></th>
                        </tr>
                    </thead>
                    <tbody class="levelTwo">';
            $call_down = $this->db->where('unique_child', $sup->unique_id)->get('tbl_calldown_received_qty')->result();
            foreach ($call_down as $cd):
                $table .= '<tr>
                            <td>
                                <select class="form-control form-group" name="calldown_month[]" title="Call-Down Month" style="width:80px;">
                                    <option value="' . $cd->cmonth . '">' . $cd->cmonth . '</option>
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" class="form-control form-group" name="calldown_qty[]" value="' . $cd->cqty . '" placeholder="Qty" style="width:100px;">
                            </td>
                            <td>
                                <select class="form-control form-group" name="received_month[]" style="width:80px;">
                                    <option value="' . $cd->rmonth . '">' . $cd->rmonth . '</option>
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </td>
                            <td>
                                <input type="number" class="form-control form-group" name="received_qty[]" value="' . $cd->rqty . '" placeholder="Qty" style="width:100px;">
                            </td>
                            <td>
                                <input type="hidden" class="form-control form-group HASH_" name="hash_id_child[]" value="' . $cd->unique_child . '"><a href="#" class="btn btn-sm btn-warning removeLevelOneSub"><i class="fa fa-minus-circle" title="Remove"></i></a> 
                            </td>
                        </tr>';
            endforeach;
            $table .= '</tbody>
                </table>
            </td>
        </tr>

    </tbody>
</table>';
            $counter++;
        endforeach;

        return $table;
    }

    function buildAdultMos($data_month, $data_year, $category) {
        $adult = "SELECT name FROM `vw_drug_list` where regimen_category like '%adult%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id in('3','8','65','67','69','25','31','41','60','62','98','64','61')";
        $paed = "SELECT name FROM `vw_drug_list` where regimen_category like '%paed%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('71','2','52','30','31','32','68','15','20','19','26','69','16','17','18','21','49','66','43','85')";
        if ($category == 'adult') {
            $in = $adult;
        } else {
            $in = $paed;
        }

        $query = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,pi.contracted ordered,k.data_month,k.data_year,
            ROUND((k.close_kemsa)/fn_get_mos_amc(d.name, DATE_FORMAT(NOW(),'%Y-%m-01')),1) mos,
            IF(pi.contracted <=0,0,ROUND((k.close_kemsa + pi.contracted)/fn_get_mos_amc(d.name, DATE_FORMAT(NOW(),'%Y-%m-01')),1)) pmos 
            FROM vw_drug_list d 
            INNER JOIN vw_procurement_list k ON k.drug_id = d.id 
            LEFT JOIN tbl_procurement_item pi ON pi.drug_id = d.id 
            WHERE d.drug_category='ARV' 
            AND k.data_month = pi.month 
            AND k.data_year = pi.year 
            AND k.data_date= DATE_FORMAT(NOW(),'%Y-%m-01') AND d.name IN($in) ORDER BY mos ASC;")->result();
        $this->db->query("DELETE FROM dsh_national_mos WHERE data_month='$data_month' AND data_year='$data_year' AND category='$category'");

        foreach ($query as $d) {
            $data = [
                'id' => $d->id,
                'drug' => $d->drug,
                'orderd' => $d->orderd,
                'mos' => $d->mos,
                'pmos' => $d->pmos,
                'data_month' => $d->data_month,
                'data_year' => $d->data_year,
                'category' => $category,
            ];
            $this->db->insert('dsh_national_mos', $data);
        }
        echo 'Build Finished';
    }

}
