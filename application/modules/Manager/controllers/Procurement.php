<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/dompdf/autoload.inc.php';
require APPPATH . 'libraries/PHPExcel.php';

use Dompdf\Dompdf;

class Procurement extends MX_Controller {

    public $email;

    public function __construct() {
        parent::__construct();
        $this->load->model('Procurement_model');
        $this->email = new Email_sender;
    }

    function look() {
        echo (int) 01;
    }

    function saveProcurementData() {
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $drug_id = $this->input->post('drug_id');
        $action = $this->input->post('action_');
        $value = $this->input->post('value');
        $date = $this->input->post('date');
        $comment = $this->input->post('comment');

        for ($i = 0; $i < count($action); $i++) {
            $data = [
                'year' => $year,
                'month' => $month,
                'status' => $action[$i],
                'drug_id' => $drug_id,
                'quantity' => $value[$i],
                'date' => $date[$i],
                'comment' => $comment[$i],
                'log_date' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('tbl_commodity_status', $data);
        }
    }

    function loadStatuses() {
        echo json_encode($this->db->get('tbl_funding_agent')->result());
    }

    function loadSuppliers_() {
        echo json_encode($this->db->get('tbl_supplier')->result());
    }

    function loadProducts() {
        echo json_encode($this->db->query("SELECT id,name,drug_category FROM `vw_drug_list` 
                    where kemsa_code is not null 
                    and kemsa_code !='0' 
                    and drug_category in ('ARV','OPPORTUNISTIC INFECTIONS (OIs)')  
                    and id not in('73','76','71')
                    order by drug_category,name ASC")->result());
    }

    function createSOHMOS() {
        $d_values = '';
        $drugs = $this->input->post('drugs');
        $month = $this->input->post('data_month');
        $year = $this->input->post('data_year');

        $date = "01 $month $year";
        $current = date('Y-m-d', strtotime($date));
        $prev = date('Y-m-d', strtotime($current . '-1 month'));

        foreach ($drugs as $d) {
            $d_values .= "'" . $d . "'" . ',';
        }
        $valid = rtrim($d_values, ',');
        $drugs_ = '(' . $valid . ')';
        if (file_exists('public/minutes_pdf/stock.xlsx')) {
            unlink("public/minutes_pdf/stock.xlsx");
        }
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("public/minutes_pdf/stock_status.xlsx");
        $objPHPExcel->getActiveSheet(0);

        $worksheet = $objPHPExcel->getActiveSheet();

        $row2 = 3;

        $lastUnits = $this->db->query("SELECT p.drug_id ,                    
                           p.avg_issues                  
                            
                        FROM vw_procurement_list p
                        INNER JOIN vw_drug_list d
                         ON p.drug_id = d.id
                        WHERE p.drug_id IN $drugs_
                        AND p.data_date = '$prev'
                        ORDER BY d.name ASC")->result();

        $units = $this->db->query("SELECT d.name , 
                        p.drug_id,
                        d.pack_size,
                           p.data_month,                           
                            p.open_kemsa,
                            p.receipts_kemsa,
                            p.issues issues_kemsa,
                            p.close_kemsa,
                            CASE WHEN p.drug_id = 42 THEN p.consumption/5 ELSE p.consumption END AS monthly_consumption ,
                            p.avg_issues,
                            CASE WHEN p.data_month = DATE_FORMAT(NOW(),'%b') THEN 0 ELSE p.avg_consumption END AS avg_consumption,
                            p.adj_losses,
                        ROUND(p.close_kemsa/p.avg_issues) mos,
                        ROUND(p.close_kemsa/p.avg_consumption)cmos
                        FROM vw_procurement_list p
                        INNER JOIN vw_drug_list d
                         ON p.drug_id = d.id
                        WHERE p.drug_id IN $drugs_
                        AND p.data_date = '$current'
                        ORDER BY d.name ASC")->result();
        $i = 1;
        $close_quantity = 0;
        foreach ($units as $signatures):
            foreach ($lastUnits as $sig2):
                if ($signatures->drug_id === $sig2->drug_id):
                    if ($signatures->close_kemsa <= 0) {
                        $close_quantity = 0;
                    } else {
                        $close_quantity = round($signatures->close_kemsa / $sig2->avg_issues, 1);
                    }
                    $col = 0;
                    $worksheet
                            ->setCellValueByColumnAndRow($col++, $row2, $i)
                            ->setCellValueByColumnAndRow($col++, $row2, $signatures->name)
                            ->setCellValueByColumnAndRow($col++, $row2, $signatures->pack_size)
                            ->setCellValueByColumnAndRow($col++, $row2, $signatures->close_kemsa)
                            ->setCellValueByColumnAndRow($col++, $row2, $sig2->avg_issues, 0)
                            ->setCellValueByColumnAndRow($col++, $row2, $close_quantity);
                    $row2++;
                    $i++;
                endif;
            endforeach;
        endforeach;



        //$objPHPExcel->getActiveSheet()->setCellValue('A2', $month . '-' . $year);
        $objPHPExcel->getActiveSheet()->setTitle('Stock Status File');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("public/minutes_pdf/stock.xlsx");
        redirect("public/minutes_pdf/stock.xlsx");
        echo 'Data exported';
    }

    function saveFundings() {
        $id = $this->input->post('fundID');
        $funder = $this->input->post('funding');
        $quantity = $this->input->post('quantity');

        $this->db->where('proc_id', $id)->delete('tbl_funding_agent_amount');

        for ($i = 0; $i < count($funder); $i++) {
            $data = [
                'proc_id' => $id,
                'agent' => $funder[$i],
                'amount' => $quantity[$i],
            ];
            $this->db->insert('tbl_funding_agent_amount', $data);
        }
    }

    function remUser($email) {
        $this->db->where('email', $email)->delete('tbl_mailing_list');
        echo 'success';
    }

    function saveSupplier() {
        $id = $this->input->post('supplierID');
        $supplier = $this->input->post('supplier');

        $this->db->where('proc_id', $id)->delete('tbl_supplier_contracted');

        for ($i = 0; $i < count($supplier); $i++) {
            $data = [
                'proc_id' => $id,
                'supplier' => $supplier[$i]
            ];
            $this->db->insert('tbl_supplier_contracted', $data);
        }
    }

    function crontest() {
        $this->db->insert('mytable', [
            'mflcode' => rand(10000000, 999999999999),
            'name' => date('Y-m-d H:i:s')
        ]);
    }

    function liveIssues() {
//Server url
        //$period_begin = date('Y-m-d', strtotime('first day of previous month'));
        $period_begin = date('Y-m-01');
        $period_end = date('Y-m-t');
        //$period_end = date('Y-m-t', strtotime('last day of previous month'));
        $workingMonth = date('M');
        $workingYear = date('Y');
        //$period_begin = '2019-07-01';
        //$period_end = '2019-07-31';
        //$workingMonth = 'Aug';
        // $workingYear = date('Y');
        // $this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");
        //get drugs
        $drugs = $this->db->get('vw_drug_list')->result();
        $this->db->where('data_year', $workingYear)->where('data_month', $workingMonth)->delete('tbl_issues');
        $this->db->query("ALTER TABLE tbl_issues AUTO_INCREMENT = 1");

        foreach ($drugs as $d):
            $drug = $d->kemsa_code;
            $url = "https://api.kemsa.co.ke/vlmis_facilityissues?filter[where][lmis_tool_id]=1000000&filter[where][issuedate][between][0]=$period_begin&filter[where][issuedate][between][1]=$period_end&filter[where][productcode]=$drug";
            $apiKey = '$2y$10$S0JuZi5EAxAsuMaV2r4Nh.1HyC.nIfSW9Pnf1UPkPsapni6Vv/xLC'; // should match with Server key
            $headers = array(
                'apitoken:' . $apiKey
            );
// Send request to Server
            $ch = curl_init($url);
// To save response in a variable from server, set headers;
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// Get response
            $response = curl_exec($ch);
// Decode
            $result = json_decode($response);

            //$this->prettyPrint($result);
            //die;
            // $this->db->query("ALTER TABLE tablename AUTO_INCREMENT = 1");
            for ($i = 0; $i < count($result); $i++) {

                $update_data = [
                    'data_year' => $workingYear,
                    'data_month' => $workingMonth,
                    'kemsa_code' => $result[$i]->productcode,
                    'mflcode' => $result[$i]->mflcode,
                    'quantity' => $result[$i]->quantity,
                    'date_issued' => $result[$i]->issuedate,
                    'batch_no' => $result[$i]->batchno,
                    'date_updated' => date('Y-m-d')
                ];
                $this->db->insert('tbl_issues', $update_data);
            }
        endforeach;

        $issues = $this->db->where('data_year', $workingYear)->where('data_month', $workingMonth)->get('vw_national_issues')->result();
        foreach ($issues as $is):
            $this->db
                    ->where('kemsa_code', $is->kemsa_code)
                    ->where('transaction_year', $workingYear)
                    ->where('transaction_month', $workingMonth)
                    ->update('tbl_procurement', ['issues_kemsa' => $is->total_issued]);
        endforeach;

        echo 'Updated issues Movement ';
    }

    function liveReceipts() {

        //$period_begin = date('Y-m-d', strtotime('first day of previous month'));
        //$period_end = date('Y-m-t', strtotime('last day of previous month'));
        $period_begin = date('Y-m-01');
        $period_end = date('Y-m-t');
        $workingMonth = date('M');
        $workingYear = date('Y');
        //$workingMonth = 'Aug';
        //$workingYear = '2019';
        $this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");
        $this->db->where('data_year', $workingYear)->where('data_month', $workingMonth)->delete('tbl_receipts');
        $this->db->query("ALTER TABLE tbl_receipts AUTO_INCREMENT = 1");

        $url = "https://api.kemsa.co.ke/vlmis_programreceipts?filter[where][lmis_tool_id]=1000000&filter[where][kem_partnerportfolio_id]=1000003&filter[where][receiptdate][between][0]=$period_begin&filter[where][receiptdate][between][1]=$period_end";
        $apiKey = '$2y$10$S0JuZi5EAxAsuMaV2r4Nh.1HyC.nIfSW9Pnf1UPkPsapni6Vv/xLC'; // should match with Server key
        $headers = array(
            'apitoken:' . $apiKey
        );
        // Send request to Server
        $ch = curl_init($url);
        // To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // Get response
        $response = curl_exec($ch);
        // Decode
        $result = json_decode($response);

        // $this->prettyPrint($result);

        for ($i = 0; $i < count($result); $i++) {
            $update_data = [
                'kemsa_code' => $result[$i]->productcode,
                'supplier' => $result[$i]->supplier,
                'suppliercode' => $result[$i]->suppliercode,
                'qty' => $result[$i]->qty,
                'receiptdate' => $result[$i]->receiptdate,
                'receipttype' => $result[$i]->receipttype,
                'sourceoffunds' => $result[$i]->sourceoffunds,
                'atl_sourceoffunds_id' => $result[$i]->atl_sourceoffunds_id,
                'atl_batch_id' => $result[$i]->atl_batch_id,
                'batchno' => $result[$i]->batchno,
                'manufacturedate' => $result[$i]->manufacturedate,
                'expirydate' => $result[$i]->expirydate,
                'data_year' => $workingYear,
                'data_month' => $workingMonth,
                'date_updated' => date('Y-d-m H:i:s'),
            ];

            $this->db->insert('tbl_receipts', $update_data);
        }


        $receipts = $this->db->where('data_year', $workingYear)->where('data_month', $workingMonth)->get('vw_national_receipts')->result();
        foreach ($receipts as $is):
            $this->db
                    ->where('kemsa_code', $is->kemsa_code)
                    ->where('transaction_year', $workingYear)
                    ->where('transaction_month', $workingMonth)
                    ->update('tbl_procurement', ['receipts_kemsa' => $is->qty_received]);

        endforeach;

        echo 'Updated Product Receipts';
    }

    function usm() {
//Server url
        $period = '2020-01-01';
        $workingMonth = 'Jan';
        $workingYear = date('Y');

        //$this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");

        $url = "https://api.kemsa.co.ke/p_productmovements?filter[where][lmis_tool_id]=1000000&filter[where][startdate]='$period'";

        //$url = "https://api.kemsa.co.ke/p_productmovements?filter[where][lmis_tool_id]=1000000&filter[where][startdate]=$period";
        $apiKey = '$2y$10$S0JuZi5EAxAsuMaV2r4Nh.1HyC.nIfSW9Pnf1UPkPsapni6Vv/xLC'; // should match with Server key
        $headers = array(
            'apitoken:' . $apiKey
        );
// Send request to Server
        $ch = curl_init($url);
// To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// Get response
        $response = curl_exec($ch);
// Decode
        $result = json_decode($response);
        //$this->prettyPrint($result);
        //


        for ($i = 0; $i < count($result); $i++) {
            $update_data = [
                'open_kemsa' => $result[$i]->openingbal,
                'receipts_kemsa' => $result[$i]->receipts,
                'issues_kemsa' => $result[$i]->issues,
                'close_kemsa' => $result[$i]->calculatedclosing,
                'monthly_consumption' => $result[$i]->consumption,
                'adj_losses' => $result[$i]->adjustments,
            ];
            $update_data2 = [
                'received' => $result[$i]->receipts
            ];
            $this->db
                    ->where('kemsa_code', $result[$i]->value)
                    ->where('transaction_year', $workingYear)
                    ->where('transaction_month', $workingMonth)
                    ->update('tbl_procurement', $update_data);

            $code = $this->db->where('kemsa_code', trim($result[$i]->value))->get('vw_drug_list')->result()[0]->id;
            if (\is_null($code)) {
                echo $result[$i]->value . '<br>';
            }

            $this->db
                    ->where('drug_id', $code)
                    ->where('year', $workingYear)
                    ->where('month', $workingMonth)
                    ->update('tbl_procurement_item', $update_data2);
        }

        echo 'Updated Stock Movement ';
    }

    function liveStocks() {
//Server url
        //$period = date('Ymd', strtotime('first day of previous month'));
        $workingMonth = date('M');

        $workingYear = date('Y');
        //$this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");

        $url = "https://api.kemsa.co.ke/vlmis_tooldrugstocks?filter[where][lmis_tool_id]=1000000";
        //$url = "https://api.kemsa.co.ke/p_productmovements?filter[where][lmis_tool_id]=1000000&filter[where][startdate]=$period";
        $apiKey = '$2y$10$S0JuZi5EAxAsuMaV2r4Nh.1HyC.nIfSW9Pnf1UPkPsapni6Vv/xLC'; // should match with Server key
        $headers = array(
            'apitoken:' . $apiKey
        );
// Send request to Server
        $ch = curl_init($url);
// To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// Get response
        $response = curl_exec($ch);
// Decode
        $result = json_decode($response);
        //  echo '<pre>';
        // print_r($result);
        // die;
        for ($i = 0; $i < count($result); $i++) {
            $update_data = [
                'close_kemsa' => $result[$i]->qtyavailable,
            ];

            $update_stock = [
                'drug_id' => $result[$i]->value,
                'date_year' => $workingYear,
                'data_month' => $workingMonth,
                'value' => $result[$i]->qtyavailable,
                'time' => date('Y-m-d H:i:s'),
            ];

            $this->db
                    ->where('kemsa_code', $result[$i]->value)
                    ->where('transaction_year', $workingYear)
                    ->where('transaction_month', $workingMonth)
                    ->update('tbl_procurement', $update_data);

            $this->db->insert('tbl_commoditytrends', $update_stock);
        }

        echo 'Updated Stock Movement ';
    }

    function stockMovement() {
//Server url
        $period = date('Ymd', strtotime('first day of previous month'));
        $workingMonth = date('M');
        $workingYear = date('Y');

        $this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");

        $url = "https://api.kemsa.co.ke/p_productmovements?filter[where][lmis_tool_id]=1000000&filter[where][startdate]='20190301'";
        $apiKey = '$2y$10$S0JuZi5EAxAsuMaV2r4Nh.1HyC.nIfSW9Pnf1UPkPsapni6Vv/xLC'; // should match with Server key
        $headers = array(
            'apitoken:' . $apiKey
        );
// Send request to Server
        $ch = curl_init($url);
// To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// Get response
        $response = curl_exec($ch);
// Decode
        $result = json_decode($response);
        echo '<pre>';
        print_r($result);
        echo '</pre>';

        echo 'Stock Movement ';
    }

    function stockMovementReader() {
        $this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");

        $objPHPExcel = PHPExcel_IOFactory::load("public/kemsa_templates/stock_movement_may.xlsx");

        $worksheet = $objPHPExcel->getActiveSheet(0);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        for ($row = 6; $row <= $highestRow; ++$row) {
            for ($col = 1; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val = $cell->getCalculatedValue();
                $dataArr[$row][$col] = $val;
            }
        }


        unset($dataArr[1]);

        $year = date('Y');
        echo $month = date('M', strtotime('first day of previous month'));

        $this->db->query("SET @@foreign_key_checks = 0;");
        foreach ($dataArr as $val) {
            $code = $val['1'];
            $this->db->query("UPDATE tbl_procurement SET  open_kemsa = '" . $val['5'] . "',receipts_kemsa = '" . $val['6'] . "',issues_kemsa = '" . $val['7'] . "',close_kemsa = '" . $val['13'] . "',adj_losses = '" . $val['8'] . "',monthly_consumption = '" . $val['14'] . "' WHERE kemsa_code='$code' AND transaction_year='$year' AND transaction_month='$month'");
        }

        echo "Stock Status updated";
    }

    function Reminder() {
        $this->sendReminder();
    }

    function getAllDrugs() {
        $query = "SELECT * FROM `vw_drug_list` WHERE drug_category IN ('ARV','OPPORTUNISTIC INFECTIONS (OIs)') ORDER BY name ASC";
        $this->response($this->db->query($query)->result());
    }

    function getDrugsByName() {
        $drug_name = $this->input->post('phrase');
        $query = "SELECT * FROM `vw_drug_list` WHERE name LIKE '%$drug_name%'  AND stock_status!='8' ORDER BY name ASC";
        echo json_encode($this->db->query($query)->result());
    }

    function getDecision($id) {
        $query = $this->db->query("SELECT id,discussion,recommendation,DATE_FORMAT(decision_date,'%W %D %b, %Y') decision_date FROM `tbl_decision`  WHERE drug_id ='$id' ORDER BY id DESC LIMIT 1")->result();
        $this->response($query);
    }

    function loadMinute($id) {
        $query = $this->db->query("SELECT * FROM tbl_minutes WHERE meeting_id ='$id'")->result();
        $this->response($query);
    }

    function saveEvent() {
        $title = $this->input->post('title');
        $venue = $this->input->post('venue');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $this->db->insert('tbl_meetings', [
            'title' => $title,
            'start_event' => $start,
            'end_event' => $end,
            'venue' => $venue
        ]);
    }

    function FilterAvg() {
        $from = $this->input->post('from');
        $from_ = str_replace("-", '', $from);
        $to = $this->input->post('to');
        $to_ = str_replace("-", '', $to);
        $drug = $this->input->post('drug');
        $drugs = "'" . implode("', '", $drug) . "'";


        $query = $this->db->query("SELECT drug,FORMAT(SUM(issues)/PERIOD_DIFF($to_,$from_),0) issues,FORMAT(SUM(consumption)/PERIOD_DIFF($to_,$from_),0) consumption 
                                FROM vw_procurement_list
                                WHERE data_date BETWEEN '$from-01' AND '$to-01'
                                AND drug IN ($drugs)
                                GROUP BY drug")->result();
        $this->response($query);
    }

    function vLookup() {

        $objPHPExcel = PHPExcel_IOFactory::load("public/MOS.xlsx");

        $worksheet = $objPHPExcel->getActiveSheet(0);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        for ($row = 2; $row <= $highestRow; ++$row) {
            for ($col = 1; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $val = $cell->getCalculatedValue();
                $dataArr[$row][$col] = $val;
            }
        }


        unset($dataArr[1]);

// $year = date('Y');
//  echo $month = date('M', strtotime('first day of previous month'));

        $this->db->query("SET @@foreign_key_checks = 0;");
        foreach ($dataArr as $val) {
            $name = $val['2'];
            $code = $val['1'];
            $sub = $val['4'];
            $dhis = $val['3'];
            $type = strtolower($val['5']);
//  $q = $this->db->query("SELECT mflcode FROM tbl_facility WHERE dhiscode='$code'")->result();


            $this->db->query("REPLACE INTO `tbl_facility` (`name`, `mflcode`, `category`, `dhiscode`, `longitude`, `latitude`, `subcounty_id`, `partner_id`, `parent_id`)
VALUES ('$name', '$code', '$type', '$dhis', NULL, NULL, '$sub', '1', NULL);");
        }

        echo "DHIS CODES UPDATED SUCCESSFULLY";
    }

    function postDiscussions() {
        $meeting_id = $this->input->post('mid');
        $discussion = $this->input->post('disc');
        $recommendation = $this->input->post('rec');
        $meeting_date = $this->input->post('mdt');
        $new_date = explode("/", $meeting_date);
        $drug_id = $this->input->post('drug_id');

        $resp['response'] = '';
        if ($this->checkPostData($drug_id, $meeting_id) > 0) {
            $dis_id = $this->db->where('drug_id', $drug_id)->where('meeting_id', $meeting_id)->get('tbl_decision')->result();
            $this->db->where('drug_id', $drug_id)->where('meeting_id', $meeting_id)->update('tbl_decision', [
                'discussion' => $discussion,
                'recommendation' => $recommendation
            ]);

            $this->db->replace('tbl_decision_log', [
                'description' => 'Updated',
                'created' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('id'),
                'decision_id' => $dis_id[0]->id,
            ]);
            $resp['response'] = 0;
        } else {
            $this->db->replace('tbl_decision', [
                'meeting_id' => $meeting_id,
                'discussion' => $discussion,
                'recommendation' => $recommendation,
                'decision_date' => $new_date[2] . '-' . $new_date[1] . '-' . $new_date[0],
                'drug_id' => $drug_id,
            ]);
            $last_id = $this->db->insert_id();

            $this->db->replace('tbl_decision_log', [
                'description' => 'Created',
                'created' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('id'),
                'decision_id' => $last_id,
            ]);
            $resp['response'] = 1;
        }
        $this->response($resp['response']);
    }

    function checkPostData($did, $mid) {
        $email = $this->db->where('meeting_id', $mid)->where('drug_id', $did)->get('tbl_decision')->result();
        return count($email);
    }

    function loadEvents() {
        $load = $this->db->get('tbl_meetings')->result();
        foreach ($load as $row) {
            $data[] = array(
                'id' => $row->id,
                'title' => $row->title,
                'start' => $row->start_event,
                'end' => $row->end_event,
                'venue' => $row->venue
            );
        }
        $this->response($data);
    }

    function updateEvent($id) {
        $this->db->where('id', $id)->update('tbl_meetings', ['venue' => $this->input->post('venue')]);
    }

    function minuteAdd($id) {
        $resp['response'] = '';
        if ($this->checkMinute($id) > 0) {
            $resp['response'] = 0;
        } else {
            $this->db->insert('tbl_minutes', [
                'meeting_id' => $id
            ]);
            $resp['response'] = 1;
        }
        $this->response($resp['response']);
    }

    function checkMinute($param) {
        $email = $this->db->where('meeting_id', $param)->get('tbl_minutes')->result();
        return count($email);
    }

    function lookForMeeting($id) {
        $resp = $this->db->where('meeting_id', $id)->get('tbl_minutes')->result();
        $count = ['count' => count($resp)];
        $this->response($count);
    }

    function loadMeetingDate($id) {
        $date = $this->db->query("SELECT DATE_FORMAT(start_event,'%d/%m/%Y') meeting_date FROM tbl_meetings WHERE id='$id'")->result();
        $this->response($date);
    }

    function loadMonthYear($id) {
        $date = $this->db->query("SELECT DATE_FORMAT(start_event,'%m-%Y') meeting_date FROM tbl_meetings WHERE id='$id'")->result();
        $this->response($date);
    }

    function updateMinutes($id) {
        header("Cache-Control: no-cache,no-store");
        $start = $this->input->post('start');
        $aob = $this->input->post('aob');
        $this->db->insert('tbl_aob', ['meeting_id' => $id, 'aob' => $aob]);
        $minute = htmlentities($this->input->post('minute'));
        $this->db->where('meeting_id', $id)->update('tbl_minutes', ['minute' => trim($minute), 'start' => $start, 'aob' => $aob]);
        $this->response(['status' => 'success']);
    }

    function getCounty() {
        $toplevel = $this->db->get('tbl_county')->result();
        echo json_encode(['data' => $toplevel]);
    }

    function generateMinute($id) {
        $data['minutes'] = $this->db->where('meeting_id', $id)->get('tbl_minutes')->result();
        $minutes = $this->db->where('id', $id)->get('tbl_meetings')->result();
        $file_date = date('dS_M_Y', strtotime($minutes[0]->start_event));
        $mail_title = date('F-Y', strtotime($minutes[0]->start_event));
        $filename = 'MINUTES_OF_PROCUREMENT_PLANNING_MEETING-' . strtoupper($file_date) . '.pdf';

        $page_builder = $this->load->view('pages/public/pdf_view', $data, true);
        $dompdf = new Dompdf;
        // Load HTML content
        $dompdf->loadHtml($page_builder);
        $dompdf->set_option('isHtml5ParserEnabled', true);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
        // $dompdf->stream();
        $output = $dompdf->output();
        unlink('public/minutes_pdf/' . $filename);
        file_put_contents('public/minutes_pdf/' . $filename, $output);
        $this->sendPlanningEmail($filename, $mail_title);
    }

    function sendPlanningEmail($filename, $mail_title) {

        $list = '';
        $mailing_list = $this->db->get('tbl_mailing_list')->result();
        foreach ($mailing_list as $m) {
            $list .= $m->email . ',';
        }
        $mailinglist = rtrim($list, ",");
        $this->email->sendProcurementEmail($mailinglist, $filename, $mail_title);
    }

    function loadMinutes($id) {
        $tr = '<tr>
                    <td width="193">
                        <p><strong>Product</strong></p>
                    </td>
                    <td width="358">
                        <p><strong>Discussion</strong></p>
                    </td>
                    <td width="350">
                        <p><strong>Recommendations</strong></p>
                    </td>
               </tr>';
        $categories = $this->db->query("SELECT dl.drug_category FROM tbl_decision d
                                        LEFT JOIN vw_drug_list dl ON dl.id = d.drug_id
                                        WHERE d.meeting_id='$id' GROUP BY dl.drug_category ORDER BY FIELD(dl.drug_category,'ARV','OPPORTUNISTIC INFECTIONS (OIs)','CONDOMS_METHADONE','LAB','NUTRITION') ")->result();

        foreach ($categories as $cat):
            $category = $cat->drug_category;
            $tr .= '<tr  style="background:#fbd4b4;"><td colspan="3" width="901"><p><strong>' . $category . '</strong></p></td></tr>';
            $minutes = $this->db->query("SELECT d.*,dl.drug_category,dl.name drug FROM tbl_decision d
                                LEFT JOIN vw_drug_list dl ON dl.id = d.drug_id
                                WHERE d.meeting_id='$id' 
                                AND dl.drug_category='$category' ORDER BY dl.name ASC")->result();
            foreach ($minutes as $min):
                $tr .= '<tr>
                    <td width="193">
                        <p>' . $min->drug . '</p>
                    </td>
                    <td width="358">'
                        . $min->discussion .
                        '</td>
                    <td width="350">'
                        . $min->recommendation .
                        '</td>
                </tr>';
            endforeach;
        endforeach;
        echo $tr;
    }

    function loadMenuData($column, $criteria = '') {
        $id = $this->input->post('id');
        if (empty($criteria)) {
            $res = $this->db->select($column)->group_by($column)->get('vw_csf_list')->result();
        } else {
            $res = $this->db->select($column)->where($criteria, $id)->group_by($column)->order_by($column, 'asc')->get('vw_csf_list')->result();
        }
        echo json_encode(['data' => $res]);
    }

    function getSubLevelMenus($table, $pcol, $id) {
        $res = $this->db->where($pcol, $id)->order_by('name', 'asc')->get($table)->result();
        echo json_encode(['data' => $res]);
    }

    function saveMeetingData() {
        $id = $this->input->post('drug_id');
        $did = $this->input->post('decision_id');
        $category = $this->input->post('drug_category');
        $discussion = $this->input->post('decision');
        $reccommendation = $this->input->post('recommendation');

        $cat = $category[0];
        $date = date('Y-m-d');

        if ($this->checkDecisionSave($cat, $date) > 0) {
            echo 1;

            for ($i = 0; $i < count($id); $i++) {
                $this->db->where('drug_id', $id[$i])->where('decision_date', $date)->update('tbl_decision', [
                    'discussion' => $discussion[$i],
                    'recommendation' => $reccommendation[$i],
                ]);

                $this->db->insert('tbl_decision_log', [
                    'description' => 'Updated',
                    'user_id' => $this->session->userdata('id'),
                    'decision_id' => $did[$i]
                ]);
            }
            $this->updateSysLogs('Updated  (tbl_decision - Meeting Minute  > Discussion and Reccommendations  )');
        } else {
            echo 2;

            for ($i = 0; $i < count($discussion); $i++) {
                $this->db->insert('tbl_decision', [
                    'discussion' => $discussion[$i],
                    'recommendation' => $reccommendation[$i],
                    'decision_date' => date('Y-m-d'),
                    'drug_id' => $id[$i],
                    'drug_category' => $category[$i]
                ]);

                $decision = $this->db->insert_id();

                $this->db->insert('tbl_decision_log', [
                    'description' => 'Created',
                    'user_id' => $this->session->userdata('id'),
                    'decision_id' => $decision
                ]);
            }
            $this->db->where('discussion=', '')->delete('tbl_decision');
            $this->updateSysLogs('Created  (tbl_decision - Meeting Minute  > Discussion and Reccommendations  )');
        }
        echo json_encode(['status' => true]);
    }

    function checkDecisionSave($cat, $date) {
        return $this->db->where('drug_category', $cat)->where('decision_date', $date)->get('tbl_decision')->num_rows();
    }

    function loadLastMinutes() {
        $query = $this->db->query("SELECT *,DATE_FORMAT(meeting_date,'%W %D %b, %Y') minute_date FROM tbl_meeting ORDER BY id DESC ")->result();
        echo json_encode($query);
    }

    function loadLastMinutesHF() {
        $query = $this->db->query("SELECT *,DATE_FORMAT(date,'%W %D %b, %Y') minute_date FROM tbl_minutes ORDER BY id DESC ")->result();
        echo json_encode($query);
    }

    function loadLastMinutesBody($id) {
        echo json_encode($this->db->where('id', $id)->get('tbl_minutes')->result());
    }

    public function getMinutesData() {
        $cat = $this->input->post('category');
        $date = $this->input->post('date');
        $url = 'manager/procurement/minute/' . $cat . '/' . $date;
        $this->session->set_userdata('minute', $url);
        $sql = "SELECT d.*, de.discussion,de.recommendation,de.decision_date,de.id decision_id
                        FROM `vw_drug_list` d
                        LEFT JOIN tbl_decision de ON d.id = de.drug_id
                        WHERE de.decision_date = '$date'
                        AND d.drug_category='$cat'          

                        UNION ALL 

                        SELECT d.*, de.discussion,de.recommendation,de.decision_date,de.id decision_id
                        FROM `vw_drug_list` d
                        LEFT JOIN tbl_decision de ON d.id = de.drug_id
                        WHERE de.decision_date IS NULL
                         AND d.drug_category='$cat' ";
        $table_data = $this->db->query($sql)->result_array();

        $table = '<table   width="100%" class="table table-bordered table-hover"  id="minutesTable">';
        $table .= '<thead><tr><th>COMMODITY </th><th>PACK-SIZE</th><th>DISCUSSION</th><th>RECOMMENDATION</th><th>TRACKER</th></tr></thead>';
        $table .= '<tbody>';
        $i = 0;
        foreach ($table_data as $d) {
            $table .= '<tr>';
            $table .= '<td><input type="hidden" name=decision_id[] value=' . $d['decision_id'] . '><input type="hidden" name=drug_id[] value=' . $d['id'] . '><strong>' . strtoupper($d['name']) . '</strong></td><td>' . $d['pack_size'] . '</td><td><textarea class="textarea" name=decision[] >' . $d['discussion'] . '</textarea></td><td><textarea class="textarea" name=recommendation[]>' . $d['recommendation'] . '</textarea></td><td> 
                           <a class="btn btn-xs btn-primary tracker_drug" data-toggle="modal" data-target="#add_procurement_modal" data-drug_id="' . $d['id'] . '"> 
                            <i class="fa fa-search"></i> View Options
                        </a>
                        <input type="hidden" name=drug_category[] value=' . $d['drug_category'] . '>
                        </td>';
            $table .= '</tr>';

            $i++;
        }
        $table .= '</tbody>'
                . '</table>';

        echo $table;
    }

    function save_minutes() {
        $title = $this->input->post('title');
        $raw_present_names = $this->input->post('present_names');
        $raw_present_emails = $this->input->post('present_emails');
        $raw_absent_names = $this->input->post('absent_names');
        $raw_absent_email = $this->input->post('absent_emails');

        $present_names = $this->sanitize($raw_present_names);
        $present_emails = $this->sanitize($raw_present_emails);
        $absent_names = $this->sanitize($raw_absent_names);
        $absent_email = $this->sanitize($raw_absent_email);
        $opening_description = $this->input->post('opening_description');
        $aob = $this->input->post('aob');

        if ($this->checkMinuteSave(date('Y-m')) > 0) {
            $this->db->query("SET foreign_key_checks = 0");
            echo 'Update';
            $this->db->like('date', date('Y-m'), 'both')->update('tbl_minutes', [
                'title' => $title,
                'present_names' => $present_names,
                'present_emails' => $present_emails,
                'absent_names' => $absent_names,
                'absent_emails' => $absent_email,
                'opening_description' => $opening_description,
                'aob' => $aob,
            ]);
            // echo $this->db->last_query();
            $this->updateSysLogs('Updated  (tbl_minutes - Meeting Minute  > Minute Agenda & A.O.Bs)');
        } else {
            $this->db->query("SET foreign_key_checks = 0");
            echo 'Inserted';
            $this->db->insert('tbl_minutes', [
                'title' => $title,
                'present_names' => $present_names,
                'present_emails' => $present_emails,
                'absent_names' => $absent_names,
                'absent_emails' => $absent_email,
                'opening_description' => $opening_description,
                'aob' => $aob,
            ]);
            $this->updateSysLogs('Created  (tbl_minutes - Meeting Minute  > Minute Agenda & A.O.Bs)');
        }
    }

    function checkMinuteSave($date) {
        return $this->db->like('date', $date, 'both')->get('tbl_minutes')->num_rows();
    }

    function sanitize($array) {
        $final_string = '';
        foreach ($array as $s) {
            $final_string .= $s . ",";
        }
        return rtrim($final_string, ',');
    }

    public function get_test_email() {

        $date = date('Y') . "-" . sprintf("%02d", date('m') - 0);
        $minutes = $this->db->query("SELECT * FROM tbl_minutes WHERE date LIKE '%$date%'")->result();
        $recepients = $minutes[0]->present_emails . ',' . $minutes[0]->absent_emails;

        //$date = date('Y') . "-" . sprintf("%02d", date('m') - 1);
        $drug_ids = "SELECT GROUP_CONCAT(id) id FROM `tbl_decision`";
        $table_ids = $this->db->query($drug_ids)->result_array();
        $drugids_ = $table_ids[0]["id"];
        $sql = "SELECT * FROM vw_drug_list  ORDER BY name ASC";
        $table_data = $this->db->query($sql)->result_array();

        $sql2 = "SELECT 
                        d.id,
                        d.decision_date,
                        d.discussion,
                        d.recommendation,
                        d.drug_id,
                        t.created,
                        CONCAT_WS(' ', u.firstname, u.lastname) user
                    FROM tbl_decision d 
                    INNER JOIN (
                        SELECT *
                        FROM tbl_decision_log l
                        WHERE (l.created, l.decision_id) IN
                        (SELECT MAX(created), decision_id
                        FROM tbl_decision_log 
                        GROUP BY decision_id)
                    ) t ON t.decision_id = d.id
                    INNER JOIN tbl_user u ON u.id = t.user_id 
                    WHERE d.decision_date LIKE '%$date%'                        
                    AND d.deleted = '0'                    
                    ORDER BY d.decision_date DESC";
        $items = $this->db->query($sql2)->result_array();


        foreach ($table_data as &$mt) {
            foreach ($items as $it) {
                if ($it['drug_id'] == $mt['id']) {
                    if (!isset($mt['decisions'])) {
                        $mt['decisions'] = [];
                    }
                    $mt['decisions'][] = $it;
                }
            }
        }
        $present = explode(",", $minutes[0]->present_names);
        $absent = explode(",", $minutes[0]->absent_names);
        $final_string = '<div class="row">
                <p><strong>' . $minutes[0]->title . '</strong></p>
            </div>
            <p><strong>Members Present</strong></p>
            <p></p>
            <ol id="membersPresent">';
        foreach ($present as $p) {
            $final_string .= '<li>' . $p . '</li>';
        }
        $final_string .= '</ol>

            <p><strong>Absent with Apology</strong></p>
            <p></p>
            <ol id="membersAbsent">';
        foreach ($absent as $a) {
            $final_string .= '<li>' . $a . '</li>';
        }
        $final_string .= '</ol>

            <div class="row" style="margin-top:20px;">
               
                <p>' . $minutes[0]->opening_description . '</p>
            </div>';
        $final_string .= '<div class="row" style="margin-top:20px;">
                <p><strong>MINUTE 2: STOCK STATUS PER PRODUCT AND REQUIRED DELIVERIES AND NEW PROCUREMENTS</strong></p>
            </div> ';

        $final_string .= '<table style="border:1px solid black; margin-top:10px;"><tr><th><strong>Product</strong></th><th><strong>Discussion</strong></th><th><strong>Recommendations</strong></th></tr>';
        $final_string .= '<tr colspan="3" style="background:#FBD4B4;border:1px solid black;"><td colspan="3"><strong>Antiretroviral Therapy</strong></td></tr>';

        foreach ($table_data as $d) {
            $final_string .= '<tr style="border:1px solid black;">';
            if (isset($d['decisions'])) {
                $final_string .= '<td style="border:1px solid black;">' . $d['name'] . '</td>';
            } else {
                
            }
            if (isset($d['decisions'])) {
                foreach ($d['decisions'] as $e) {
                    $final_string .= '<td style="border:1px solid black;">' . $e['discussion'] . '</td><td style="border:1px solid black;">' . $e['recommendation'] . '</td>';
                }
            } else {
                
            }
            $final_string .= '</tr>';
        }
        $final_string .= '</table>';

        $final_string .= '
            <p style="margin-top:20px;"><strong>A.O.B</strong></p>
            <div class="row" style="margin-top:10px;">
                <p>' . $minutes[0]->aob . '</p>
            </div> ';
        $this->email_sender->send_email('Procurement', 'Meeting Minutes', $recepients, $names = '', $final_string);
        echo json_encode(['status' => 'success']);
    }

    public function get_commodities() {
        $response = $this->Procurement_model->get_commodity_data();
        echo json_encode(array('data' => $response['data']));
    }

    public function get_tracker($drug_id) {
        $response = $this->Procurement_model->get_tracker_data($drug_id);
        echo json_encode(array('data' => $response['data']));
    }

    function get_transaction_data() {
        $drug_id = $this->input->post('drug_id');
        $data = $this->input->post('data');
        $reporting_months = [];

        foreach ($data[0] as $index => $str) {
            if ($index > 1) {
                $str_arr = explode(' ', $str);
                $year = $str_arr[1];
                $month = $str_arr[0];
                array_push($reporting_months, $month);
            }
        }

        $open_kemsa = $data[1];
        $receipts_kemsa = $data[2];
        $issues_kemsa = $data[3];
        $close_kemsa = $data[4];
        $monthly_consumption = $data[5];

        for ($i = 0; $i < count($reporting_months); $i++) {

            $inner_count = 2;
            $j = $i + $inner_count;

            $update_data = [
                'open_kemsa' => $open_kemsa[$j],
                'receipts_kemsa' => $receipts_kemsa[$j],
                'issues_kemsa' => $issues_kemsa[$j],
                'close_kemsa' => $close_kemsa[$j],
                'monthly_consumption' => $monthly_consumption[$j],
            ];
            $this->db
                    ->where('transaction_year', $year)
                    ->where('transaction_month', $reporting_months[$i])
                    ->where('drug_id', $drug_id)
                    ->update('tbl_procurement', $update_data);
        }

        $this->db->insert('tbl_procurement_log', [
            'description' => 'updated',
            'user_id' => $this->session->userdata('id'),
            'procurement_id' => 1
        ]);
        $this->updateSysLogs('Updated  (Product Tracker Updated)');
        echo 'Data updated Successfully';
    }

    public function save_tracker() {
        $drug_id = $this->input->post('drug_id');
        $receipts = $this->input->post('receipt_qty');
        $transaction_date = $this->input->post('transaction_date');
        $status = $this->input->post('status');
        $funding_agent = $this->input->post('funding_agent');
        $supplier = $this->input->post('supplier');
        $comments = $this->input->post('comment');
        $this->db->query("SET foreign_key_checks = 0");
        $id = $this->db->query("SELECT MAX(id)id FROM tbl_procurement_item")->result()[0]->id + 1;


        if ($status == '1') {
            $column = 'proposed';
        } else if ($status == '2') {
            $column = 'contracted';
        } else if ($status == '3') {
            $column = 'received';
        } else if ($status == '5') {
            $column = 'calldown';
        } else {
            $column = 'pending';
        }

        $year = date('Y', strtotime($transaction_date));
        $month = date('M', strtotime($transaction_date));
        $comm = $comments;
        $qty = $receipts;
        $did = $drug_id;




        $this->db->query("UPDATE tbl_procurement_item SET $column=$column + $qty,comments='$comm' WHERE year='$year' AND month='$month' AND drug_id='$did' ");



        // $this->db->query("CALL proc_save_tracker_history(?, ?, ? ,?, ?, ?, ?,?,?,?,?)", array(
        //   date('Y', strtotime($transaction_date[$key])), date('M', strtotime($transaction_date[$key])), $drug_id, $qty, $funding_agent[$key], $supplier[$key], $status[$key], $this->session->userdata('id'), $comments[$key], $drug_id[$key], $id
        //));
        //}





        $message = '<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>Success!</strong> Procurement was Added</div>';
        $this->updateSysLogs('Updated  (Tracker data updated)');
        $this->session->set_flashdata('tracker_msg', $message);
        $this->response(['status' => 'success']);
    }

    function getDrugDetails($column, $year, $month, $drug_id) {
        $this->response($this->db->query("SELECT $column,comments FROM tbl_procurement_item WHERE year='$year' AND month='$month' AND drug_id='$drug_id'")->result());
    }

    function loadCommentsAndFS($year, $month, $drug_id) {
        $data = [
            'funds' => $this->db->query("SELECT * FROM tbl_funding_agent_amount WHERE year='$year' AND month='$month' AND drug_id='$drug_id'")->result(),
            'contracts' => $this->db->query("SELECT * FROM tbl_supplier_contracted WHERE year='$year' AND month='$month' AND drug_id='$drug_id'")->result(),
            'comments' => $this->db->query("SELECT * FROM tbl_procurement_item WHERE year='$year' AND month='$month' AND drug_id='$drug_id'")->result()
        ];
        $this->response($data);
    }

    function updateDetails($year, $month, $drug_id) {
        $column = $this->input->post('column');
        if ($column == 'proposed' || $column == 'contracted') {
            $comment = $this->input->post('commentp');
        } else {
            $comment = $this->input->post('comment');
        }

        if ($column == 'contracted') {
            $quantity = $this->input->post('contracted');
        } else {
            $quantity = $this->input->post('proposed');
        }
        $this->db->query("UPDATE tbl_procurement_item SET $column='$quantity' ,comments='$comment' WHERE year='$year' AND month='$month' AND drug_id='$drug_id'");

        if ($column == 'proposed') {
            $funder = $this->input->post('funding');
            $quantity = $this->input->post('quantity');
            $this->db->where('drug_id', $drug_id)->where('month', $month)->where('year', $year)->delete('tbl_funding_agent_amount');
            for ($i = 0; $i < count($funder); $i++) {
                $data = [
                    'drug_id' => $drug_id,
                    'year' => $year,
                    'month' => $month,
                    'agent' => $funder[$i],
                    'amount' => $quantity[$i],
                ];
                $this->db->insert('tbl_funding_agent_amount', $data);
            }
        } else if ($column == 'contracted') {
            $supplier = $this->input->post('supplier');
            $this->db->where('drug_id', $drug_id)->where('month', $month)->where('year', $year)->delete('tbl_supplier_contracted');

            for ($i = 0; $i < count($supplier); $i++) {
                $data = [
                    'drug_id' => $drug_id,
                    'year' => $year,
                    'month' => $month,
                    'supplier' => $supplier[$i]
                ];
                $this->db->insert('tbl_supplier_contracted', $data);
            }
        }

        $this->response(['status' => 'success']);
    }

    function save_decision() {
        $drug_id = $this->input->post('drug_id');
        $decision = $this->input->post('discussion');
        $reccomendation = $this->input->post('recommendation');
        $this->db->insert('tbl_decision', [
            'discussion' => $decision,
            'recommendation' => $reccomendation,
            'decision_date' => date('Y-m-d'),
            'drug_id' => $drug_id
        ]);

        $insert_id = $this->db->insert_id();
        $this->db->insert('tbl_decision_log', [
            'description' => 'created',
            'user_id' => $this->session->userdata('id'),
            'decision_id' => $insert_id
        ]);
        $this->updateSysLogs('Created  (tbl_decision > New Discussion/Reccommendation )');
    }

    function edit_decision() {
        $drug_id = $this->input->post('drug_id');
        $decision_id = $this->input->post('decision_id');
        $decision = $this->input->post('discussion');
        $reccomendation = $this->input->post('recommendation');
        $this->db->where('id', $decision_id)->update('tbl_decision', [
            'discussion' => $decision,
            'recommendation' => $reccomendation,
            'drug_id' => $drug_id
        ]);

        $this->db->insert('tbl_decision_log', [
            'description' => 'updated',
            'user_id' => $this->session->userdata('id'),
            'decision_id' => $decision_id
        ]);
        $this->updateSysLogs('Updated  (tbl_decision > Discussion/Reccommendation )');
    }

    public function delete_decision() {
        $decision_id = $this->input->post('decision_id');
        $this->db->where('id', $decision_id)->update('tbl_decision', [
            'deleted' => 1
        ]);

        $this->db->insert('tbl_decision_log', [
            'description' => 'deleted',
            'user_id' => $this->session->userdata('id'),
            'decision_id' => $decision_id
        ]);

        $this->updateSysLogs('Deleted  (tbl_decision > Discussion/Reccommendation )');
    }

    public function get_timeline($id) {

        $drug_ids = "SELECT GROUP_CONCAT(id) id FROM `tbl_decision`";
        $table_ids = $this->db->query($drug_ids)->result_array();
        $drugids_ = $table_ids[0]["id"];
        $sql = "SELECT * FROM vw_drug_list";
        $table_data = $this->db->query($sql)->result_array();

        $sql2 = "SELECT 
                        d.id,
                        d.decision_date,
                        d.discussion,
                        d.recommendation,
                        d.drug_id,
                        t.created,
                        CONCAT_WS(' ', u.firstname, u.lastname) user
                    FROM tbl_decision d 
                    LEFT JOIN (
                        SELECT *
                        FROM tbl_decision_log l
                        WHERE (l.created, l.decision_id) IN
                        (SELECT MAX(created), decision_id
                        FROM tbl_decision_log 
                        GROUP BY decision_id)
                    ) t ON t.decision_id = d.id
                    INNER JOIN tbl_user u ON u.id = t.user_id                    
                    AND d.deleted = '0' 
                    AND d.id='$id'
                    ORDER BY d.decision_date DESC";
        $items = $this->db->query($sql2)->result_array();


        foreach ($table_data as &$mt) {
            foreach ($items as $it) {
                if ($it['drug_id'] == $mt['id']) {
                    if (!isset($mt['decisions'])) {
                        $mt['decisions'] = [];
                    }
                    $mt['decisions'][] = $it;
                }
            }
        }

        $final_string = '';

        foreach ($table_data as $d) {
            if (isset($d['decisions'])) {
                $final_string .= '<p style="font-weight:bold; font-size:16px; color:blue;">' . $d['name'] . '</p>';
            } else {
                
            }
            if (isset($d['decisions'])) {
                foreach ($d['decisions'] as $e) {
                    $final_string .= $e["discussion"] . $e["recommendation"];
                }
            } else {
                
            }
        }

        echo $final_string;
    }

    public function get_decisions($drug_id) {
        $html_timeline = '<table class="table table-bordered table-responsive table-hover"><thead><th>DISCUSSIONS</th><th>RECCOMMENDATIONS</th></thead>';
        $response = $this->Procurement_model->get_decision_data($drug_id);
        foreach ($response['data'] as $values) {
            $html_timeline .= '<tr style="font-weight:bold;text-align:center;color:blue;"><td colspan="2">
            <span class="timeline-balloon-date-day">' . date("d", strtotime($values["decision_date"])) . '</span>
            <span class="timeline-balloon-date-month">' . date("M/y", strtotime($values["decision_date"])) . '</span>
        </tr>
        <tr>
            <td>               
                <span class="causale _disc' . $values["id"] . '">' . strip_tags($values["discussion"]) . '</span> 
            </td>         
            <td>
                <span  class="causale _rec' . $values["id"] . '">' . strip_tags($values["recommendation"]) . '</span> 
            </td>
        </tr>';
        }
        $html_timeline .= '</tbody></table>';
        echo $html_timeline;
    }

    public function get_decisions_byID($drug_id) {
        echo json_encode($this->Procurement_model->get_decision_data_by_id($drug_id));
    }

    function rv($v) {
        $rdata = $v;
        if (empty($v)) {
            $rdata = ' - ';
        } else if ($v == 0) {
            $rdata = ' - ';
        } else {
            $rdata = number_format($v);
        }
        return $rdata;
    }

    function cc($v) {
        $k = (int) $v;
        $class = '';
        if ($k >= 6 && $k <= 9) {
            $class = 'class1';
        } else if ($k >= 4 && $k <= 5) {
            $class = 'class2';
        } else if ($k <= 3) {
            $class = 'class3';
        } else {
            $class = 'class4';
        }
        return $class;
    }

    function newValue($x, $y, $z) {
        return (int) $x - (int) $y + (int) $z;
    }

    function loadFundings($year, $month, $drug_id) {
        $date = $this->db->query("SELECT *  FROM tbl_funding_agent_amount WHERE drug_id='$drug_id' AND month='$month' AND year='$year'")->result();
        $this->response($date);
    }

    function loadSuppliers2($year, $month, $drug_id) {
        $date = $this->db->query("SELECT *  FROM tbl_supplier_contracted WHERE drug_id='$drug_id' AND month='$month' AND year='$year'")->result();
        $this->response($date);
    }

    function loadFunders($id) {
        $date = $this->db->query("SELECT  GROUP_CONCAT(CONCAT_WS(':', agent, amount) SEPARATOR ', ') AS details 
                                  FROM tbl_funding_agent_amount WHERE proc_id='$id'")->result();
        $this->response($date);
    }

    function loadSuppliers($id) {
        $date = $this->db->query("SELECT  GROUP_CONCAT(CONCAT_WS(' ', supplier) SEPARATOR ', ') AS details 
                                  FROM tbl_supplier_contracted WHERE proc_id='$id'")->result();
        $this->response($date);
    }

    function loadSupplier($id) {
        $date = $this->db->query("SELECT *  FROM tbl_supplier_contracted WHERE id='$id'")->result();
        $this->response($date);
    }

    public function get_transaction_table2($drug_id, $period_year) {
        error_reporting(0);
        $column = $this->getTransactionStatus($drug_id, $period_year);
        $month = (int) date('m');
        $previousCount = $month - 1;
        $full = 11;

        $transaction_table = '
<table class="table table-hover table-condensed table-bordered TRACKER">
  <tr style="font-weight:bold !important;">
    <th class="tg-0pky" <strong></th>
    <th class="tg-0pky"><strong>Jan-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Feb-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Mar-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Apr-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>May-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Jun-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Jul-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Aug-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Sep-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Oct-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Nov-' . $period_year . '</strong></th>
    <th class="tg-0pky"><strong>Dec-' . $period_year . '</strong></th>
  </tr>
  <tr>
    <td class="tg-0pky"><strong>Opening Balance</strong></td>';
        //opening balance
        if ($period_year != date('Y')) {
            for ($i = 0; $i <= $full; $i++) {
                $col = (int) $i + 1;
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col . '">' . @$this->rv($column['tracker'][$i]['open_kemsa']) . '</td>';
            }
        } else {

            for ($i = 0; $i <= $previousCount; $i++) {
                $col = (int) $i + 1;
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col . '">' . @$this->rv($column['tracker'][$i]['open_kemsa']) . '</td>';
            }
            for ($i = $month; $i <= $full; $i++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col . '">' . @$this->rv(0) . '</td>';
            }
        }

        /* $transaction_table .= '</tr>
          <tr>

          <td class="tg-0pky"><strong>Proposed Qty.</strong></td>';
          //proposed quantity
          if ($period_year != date('Y')) {
          for ($i = 0; $i <= $full; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata popoverOption proposed tg-0pky col-' . $col . '  data-content="Popup with option trigger" rel="popover" data-placement="bottom" data-original-title=Title">' . @$this->rv($column['status'][$i]['proposed']) . '</td>';
          }
          } else {

          for ($i = 0; $i <= $previousCount; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata proposed tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['proposed']) . '</td>';
          }
          for ($i = $month; $i <= $full; $i++) {
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata proposed tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['proposed']) . '</td>';
          }
          }

          $transaction_table .= '</tr>
          <tr>
          <td class = "tg-0pky"><strong>Contracted Qty.</strong></td>';
          //contracted quantity
          if ($period_year != date('Y')) {
          for ($i = 0; $i <= $full; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata contracted tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['contracted']) . '</td>';
          }
          } else {

          for ($i = 0; $i <= $previousCount; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata contracted tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['contracted']) . '</td>';
          }
          for ($i = $month; $i <= $full; $i++) {
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata contracted tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['contracted']) . '</td>';
          }
          }
          $transaction_table .= '</tr>
          <tr>
          <td class = "tg-0pky"><strong>Call Down Qty.</strong></td>';
          //call down quantity
          if ($period_year != date('Y')) {
          for ($i = 0; $i <= $full; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata calldown tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['calldown']) . '</td>';
          }
          } else {

          for ($i = 0; $i <= $previousCount; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata calldown tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['calldown']) . '</td>';
          }
          for ($i = $month; $i <= $full; $i++) {
          $transaction_table .= '<td data-drug_id="' . $drug_id . '" data-year="' . $period_year . '" data-month="' . $column['status'][$i]['month'] . '" class="tdata calldown tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['calldown']) . '</td>';
          }
          } */
        $transaction_table .= '</tr>

        <tr>
        <td class = "tg-0pky"><strong>Received Qty.</strong></td>';
        //pending quantity
        if ($period_year != date('Y')) {
            for ($n = 0; $n <= $full; $n++) {
                $col10 = (int) $n + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col10 . '">' . @$this->rv($column['tracker'][$n]['receipts_kemsa']) . ' </td>';
            }
        } else {
            for ($n = 0; $n <= $previousCount; $n++) {
                $col100 = (int) $n + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col100 . '">' . @$this->rv($column['tracker'][$n]['receipts_kemsa']) . ' </td>';
            }

            for ($co31 = $month; $co31 <= $full; $co31++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $co31 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>';
        /* <tr>
          <td class = "tg-0pky"><strong>Pending</strong></td>';
          //pending quantity
          if ($period_year != date('Y')) {
          for ($i = 0; $i <= $full; $i++) {
          $col = (int) $i + 1;
          $transaction_table .= '<td  class="tdata tg-0pky col-' . $col . '">' . @$this->rv($column['status'][$i]['pending']) . '</td>';
          }
          } else {
          $pendind = '';
          for ($i = 0; $i <= $previousCount; $i++) {
          $col = (int) $i + 1;
          $pendind = (($column['status'][$i]['contracted'] - $column['status'][$i]['calldown']) + ($column['status'][$i]['calldown'] - $column['status'][$i]['received']));
          $transaction_table .= '<td class="tdata tg-0pky col-' . $col . '">' . @$this->rv(($pendind) > 0 ? $pendind : 0) . '</td>';
          }
          for ($i = $month; $i <= $full; $i++) {
          $pendind1 = (($column['status'][$i]['contracted'] - $column['status'][$i]['calldown']) + ($column['status'][$i]['calldown'] - $column['status'][$i]['received']));

          $transaction_table .= '<td class="tdata tg-0pky col-' . $col . '">' . @$this->rv(($pendind1) > 0 ? $pendind1 : 0) . '</td>';
          }
          }
          $transaction_table .= '</tr> */

        $transaction_table .= '<tr>
        <td class = "tg-0pky" ><strong>Issues to Facility</strong></td>';
        //issues quantity
        if ($period_year != date('Y')) {
            for ($n = 0; $n <= $full; $n++) {
                $col5 = (int) $n + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col5 . '">' . @$this->rv($column['tracker'][$n]['issues_kemsa']) . ' </td>';
            }
        } else {
            for ($n = 0; $n <= $previousCount; $n++) {
                $col5 = (int) $n + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col5 . '">' . @$this->rv($column['tracker'][$n]['issues_kemsa']) . ' </td>';
            }

            for ($co3 = $month; $co3 <= $full; $co3++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col5 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>
        
        <tr>
        <td class = "tg-0pky" ><strong>Adjustments/Losses (+/-)</strong></td>';
        //loss/adj quantity
        if ($period_year != date('Y')) {
            for ($o = 0; $o <= $full; $o++) {
                $col6 = (int) $o + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col6 . '">' . @$this->rv($column['tracker'][$o]['adj_losses']) . ' </td>';
            }
        } else {
            for ($o = 0; $o <= $previousCount; $o++) {
                $col6 = (int) $o + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col6 . '">' . @$this->rv($column['tracker'][$o]['adj_losses']) . ' </td>';
            }

            for ($co7 = $month; $co7 <= $full; $co7++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col6 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>        
        <tr>
        <td class = "tg-0pky" ><strong>Closing Balance</strong></td>';
        //Closing Balance
        if ($period_year != date('Y')) {
            for ($p = 0; $p <= $full; $p++) {
                $col7 = (int) $p + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col7 . '">' . @$this->rv($column['tracker'][$p]['close_kemsa']) . ' </td>';
            }
        } else {
            for ($p = 0; $p <= $previousCount; $p++) {
                $col7 = (int) $p + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col7 . '">' . @$this->rv($column['tracker'][$p]['close_kemsa']) . ' </td>';
            }

            for ($co8 = $month; $co8 <= $full; $co8++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col7 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>       
        <tr>
        <td class = "tg-0pky" ><strong>Monthly Consumption</strong></td>';
        //Monthly Consumptions
        if ($period_year != date('Y')) {
            for ($q = 0; $q <= $full; $q++) {
                $col8 = (int) $q + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col8 . '">' . @$this->rv($column['tracker'][$q]['monthly_consumption']) . ' </td>';
            }
        } else {
            for ($q = 0; $q <= $previousCount; $q++) {
                $col8 = (int) $q + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col8 . '">' . @$this->rv($column['tracker'][$q]['monthly_consumption']) . ' </td>';
            }

            for ($co9 = $month; $co9 <= $full; $co9++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col8 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>  
        
        <tr>
        <td class = "tg-0pky" ><strong>Average Issues</strong></td>';
        //Average Issues
        if ($period_year != date('Y')) {
            for ($r = 0; $r <= $full; $r++) {
                $col9 = (int) $r + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col9 . '">' . @$this->rv($column['tracker'][$r]['avg_issues']) . ' </td>';
            }
        } else {
            for ($r = 0; $r <= $previousCount; $r++) {
                $col9 = (int) $r + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col9 . '">' . @$this->rv($column['tracker'][$r]['avg_issues']) . ' </td>';
            }

            for ($co10 = $month; $co10 <= $full; $co10++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col9 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>  
        
        <tr>
        <td class = "tg-0pky"><strong>Average Consumption</strong></td>';
        //Average Consumption
        if ($period_year != date('Y')) {
            for ($s = 0; $s <= $full; $s++) {
                $coll0 = (int) $s + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $coll0 . '">' . @$this->rv($this->adjustValue($drug_id, @$column['tracker'][$s]['avg_consumption'])) . ' </td>';
            }
        } else {
            for ($s = 0; $s <= $previousCount; $s++) {
                $coll0 = (int) $s + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $coll0 . '">' . @$this->rv($this->adjustValue($drug_id, @$column['tracker'][$s]['avg_consumption'])) . ' </td>';
            }

            for ($co11 = $month; $co11 <= $full; $co11++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $coll0 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr> 
        
        <tr>
        <td class = "tg-0pky" ><strong>MOS (Issues Based)</strong></td>';
        //Issues Based
        if ($period_year != date('Y')) {
            for ($t = 0; $t <= $full; $t++) {
                $col11 = (int) $t + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col11 . ' ' . @$this->cc($column['tracker'][$t]['mos']) . '">' . @$this->rv($column['tracker'][$t]['mos']) . ' </td>';
            }
        } else {
            for ($t = 0; $t <= $previousCount; $t++) {
                $col11 = (int) $t + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col11 . ' ' . @$this->cc($column['tracker'][$t]['mos']) . '">' . @$this->rv($column['tracker'][$t]['mos']) . ' </td>';
            }

            for ($co11 = $month; $co11 <= $full; $co11++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col11 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>         
        <tr>
        <td class = "tg-0pky" ><strong>MOS (Consumption Based)</strong></td>';
        //Consumption Based
        if ($period_year != date('Y')) {
            for ($u = 0; $u <= $full; $u++) {
                $col12 = (int) $t + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col12 . ' ' . @$this->cc($column['tracker'][0]['cmos']) . '">' . @$this->rv($column['tracker'][$u]['cmos']) . ' </td>';
            }
        } else {
            for ($u = 0; $u <= $previousCount; $u++) {
                $col12 = (int) $t + 1;
                $transaction_table .= '<td class = "tdata tg-0pky col-' . $col12 . ' ' . @$this->cc($column['tracker'][0]['cmos']) . '">' . @$this->rv($column['tracker'][$u]['cmos']) . ' </td>';
            }

            for ($co12 = $month; $co12 <= $full; $co12++) {
                $transaction_table .= '<td class="tdata tg-0pky col-' . $col12 . '">' . @$this->rv(0) . '</td>';
            }
        }
        $transaction_table .= '</tr>';





        $prop = 0;
        $contr = 0;
        $calld = 0;
        $recd = 0;
        $count = $month - 2;
        $count12 = $month - 1;
        //$total = 0;
        for ($pro = 0; $pro <= 11; $pro++) {
            $prop = $prop + $column['status'][$pro]['proposed'];
            $contr = $contr + $column['status'][$pro]['contracted'];
            $calld = $calld + $column['status'][$pro]['calldown'];
            $recd = $recd + $column['status'][$pro]['received'];
        }


        $pending = (int) $contr - (int) $recd;
        if ($pending < 0) {
            $d = 0;
        } else {
            $d = $pending;
        }
        (int) $closing_bal = $column['tracker'][$count12]['close_kemsa'];
        (int) $issues = $column['tracker'][$count]['avg_issues'];
        //$column['tracker'][$r]['avg_issues']
        //echo (int) $issues = $column['tracker'][$count]['issues_kemsa'];
        //(int) $call_d = $column['expected'][$count]['quantity'];
        (int) $total = $contr + $closing_bal;
        $results = $total / $issues;
        $column['contracted'][$k]['quantity'];
        // echo '<p><strong>TOTAL QTY PENDING DELIVERY AS OF ' . date('M-Y') . ' : ' . $closing_bal . '-' . $results . '</strong></p>'; // ',c' . $contr . ',cd' . $calld . ',cb' . $closing_bal . ',iss' . $issues . ',recd' . $recd;
        //echo $month . ' Proposed: ' . $prop . '&& Contracted: ' . $contr . '&& Total: ' . $total . '&& Issues: ' . $issues . '&& Closing Bal: ' . $closing_bal. '&& MOS: ' . $res;
        $val = $results;

        if ($val > 0 && $val < 6.0) {
            $color = 'red';
        } else if ($val > 6.0 && $val < 9.0) {
            $color = 'orange';
        } else if ($val > 9) {
            $color = 'green';
        }
        //' . number_format($results, 1) . '
        $transaction_table .= '         
        <tr>
        <td class = "tg-0pky" title="(Contracted + Ending Balance / Issued Amount"><strong>Projected MOS (Contracted Quantity + <br> End Balance Quantity/Issued Quantity)</strong></td><td colspan="12" style="background:' . $color . '"><center><span style="color:white;font-weight:bold; font-size:32px;"></span></center></td>';
        //Consumption Based

        $transaction_table .= '</tr> 

        </table>';
        echo $transaction_table;
    }

//    function getDrugDetails($column, $year, $month, $drug_id) {
//        $this->response($this->db->query("SELECT $column,comments FROM tbl_procurement_item WHERE year='$year' AND month='$month' AND drug_id='$drug_id'")->result());  
//    }

    function getTrackerData($drugid, $year) {
        $day = date('d');
        //if ($day == '1' || $day == '2' || $day == '8' || $day == '12' || $day == '25') {
        $this->updateConsumption($year, $drugid);
        //}
        $rmonths2 = $this->trackerMonthsOpen();
        $query2 = $this->db->query("SELECT                         
                           data_month,                           
                            open_kemsa,
                            receipts_kemsa,
                            issues issues_kemsa,
                            close_kemsa,
                            CASE WHEN drug_id = 42 THEN consumption/5 ELSE consumption END AS monthly_consumption ,
                            avg_issues,
                            CASE WHEN data_month = DATE_FORMAT(NOW(),'%b') THEN 0 ELSE avg_consumption END AS avg_consumption,
                            adj_losses,
                        ROUND(close_kemsa/avg_issues) mos,
                        ROUND(close_kemsa/avg_consumption)cmos
                        FROM vw_procurement_list p
                        WHERE p.drug_id = '$drugid'
                        AND p.data_year = '$year'
                        GROUP BY data_month
                        ORDER BY data_year ASC, 
                        FIELD(data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )")->result_array();


        foreach ($query2 as $q) {
            unset($rmonths2[$q['data_month']]);
        }
        $arr = [];
        for ($i = 0; $i < count($rmonths2); $i++) {
            array_push($arr, $i);
        }
        $new_arr = $this->rename_keys($rmonths2, $arr);
        $final_arr = array_merge($new_arr, $query2);
        usort($final_arr, function ($a, $b) {
            $t1 = strtotime($a['data_month']);
            $t2 = strtotime($b['data_month']);
            return $t1 - $t2;
        });
        return $final_arr;
    }

    function getTrackerDataStatus($drugid, $year) {
        $rmonths2 = $this->trackerMonthsOpen();
        $query2 = $this->db->query("SELECT 
                            month,
                            proposed,                           
                            contracted,
                            calldown,
                            received        
                        FROM tbl_procurement_item p
                        WHERE p.drug_id = '$drugid'
                        AND p.year = '$year'
                        GROUP BY p.month
                        ORDER BY p.year ASC, 
                        FIELD(p.month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )")->result_array();


        foreach ($query2 as $q) {
            unset($rmonths2[$q['month']]);
        }
        $arr = [];
        for ($i = 0; $i < count($rmonths2); $i++) {
            array_push($arr, $i);
        }
        $new_arr = $this->rename_keys($rmonths2, $arr);
        $final_arr = array_merge($new_arr, $query2);
        usort($final_arr, function ($a, $b) {
            $t1 = strtotime($a['month']);
            $t2 = strtotime($b['month']);
            return $t1 - $t2;
        });
//        echo '<pre>';
//        print_r($final_arr);
        return $final_arr;
    }

    function adjustValue($drug_id, $data) {
        if ($drug_id == '42') {
            return ((float) $data / 5);
        } else {
            return $data;
        }
    }

    function getTransactionStatus($drugid, $year) {

        $rmonths = $this->trackeMonths();
        $tracker = $this->getTrackerData($drugid, $year);
        $status = $this->getTrackerDataStatus($drugid, $year);
        /*
          $new_arr = [];
          $transaction_status = $this->db->select('name')->from("tbl_procurement_status")->not_like('name', 'cancelled')->order_by('id', 'asc')->get()->result();
          foreach ($transaction_status as $stat):
          $status = strtolower($stat->name);
          $query[$status] = $this->db->query("SELECT
          transaction_month data_month,
          SUM(quantity) quantity
          FROM tbl_procurement_item pi
          INNER JOIN tbl_procurement p ON p.id = pi.procurement_id
          INNER JOIN tbl_procurement_status ps ON ps.id = pi.procurement_status_id
          LEFT JOIN tbl_funding_agent fa ON fa.id = pi.funding_agent_id
          LEFT JOIN tbl_supplier s ON s.id = pi.supplier_id
          WHERE p.drug_id = '$drugid'
          AND transaction_year='$year'
          AND ps.name LIKE '%$status%'
          GROUP BY data_month
          ORDER BY transaction_year DESC, FIELD(transaction_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )")->result_array();
          endforeach;


          //checking and removing months that already exist from the query
          foreach ($query as $k => $q):
          foreach ($query[$k] as $k) {
          unset($rmonths[$k['data_month']]);
          }
          array_push($new_arr, $rmonths);
          endforeach;


          //Getting number of array element so as to get keys for replacements in the resultant array above from name to index
          $renamed_arrays = [];
          foreach ($new_arr as $ki => $v):
          $arr[$ki] = [];
          for ($i = 0; $i < count($new_arr[$ki]); $i++) {
          array_push($arr[$ki], $i);
          }
          $new_arr1 = $this->rename_keys($new_arr[$ki], $arr[$ki]);
          array_push($renamed_arrays, $new_arr1);
          endforeach;



          //merging and sorting the resultant array by month
          $new_final_array = [];
          foreach ($transaction_status as $key => $stat):
          $status = strtolower($stat->name);
          $merged = array_merge($query[$status], $renamed_arrays[$key]);
          usort($merged, function ($a, $b) {
          $t1 = strtotime($a['data_month']);
          $t2 = strtotime($b['data_month']);
          return $t1 - $t2;
          });
          array_push($new_final_array, $merged);
          endforeach; */

        //assigning the final array the procurement statuses as opposed to the array indexes for easier dynamic referencing``
        $tracker_data = [
            'tracker' => $tracker,
            'status' => $status,
                //'expected' => $this->expected($drugid, $year),
                //'call_down' => $this->call_down($drugid, $year)
        ];
        //$this->response($tracker_data);
        //

        return $tracker_data;
    }

    function expected($drug, $year) {
        $rmonths = $this->trackeMonths();
        $query = $this->db->query("SELECT                 
                    transaction_month data_month,                   
                    quantity quantity                     
                    FROM tbl_procurement_item pi
                    INNER JOIN tbl_procurement p ON p.id = pi.procurement_id
                    INNER JOIN tbl_procurement_status ps ON ps.id = pi.procurement_status_id
                    LEFT JOIN tbl_funding_agent fa ON fa.id = pi.funding_agent_id
                    LEFT JOIN tbl_supplier s ON s.id = pi.supplier_id
                    WHERE p.drug_id = '$drug'
                    AND transaction_year='$year'
                    AND ps.name LIKE '%Call_Down%'
                    GROUP BY transaction_month
                    ORDER BY transaction_year DESC, FIELD(transaction_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )")->result_array();


        foreach ($query as $k) {
            unset($rmonths[$k['data_month']]);
        }
        $renamed_arrays = [];
        $arr = [];
        for ($i = 0; $i < count($rmonths); $i++) {
            array_push($arr, $i);
        }
        $new_arr1 = $this->rename_keys($rmonths, $arr);
        array_push($renamed_arrays, $new_arr1);

        $merged = array_merge($query, $renamed_arrays[0]);
        usort($merged, function ($a, $b) {
            $t1 = strtotime($a['data_month']);
            $t2 = strtotime($b['data_month']);
            return $t1 - $t2;
        });

        return $merged;
    }

    function call_down($drug, $year) {
        $rmonths = $this->trackeMonths();
        $query = $this->db->query("SELECT month data_month, quantity quantity 
                        FROM tbl_procurement_history pi 
                        INNER JOIN tbl_procurement_status ps ON ps.id = pi.procurement_status_id 
                        WHERE pi.drug_id = '$drug' AND year='$year' 
                        AND ps.name LIKE '%Call_Down%'
                        GROUP BY month ORDER BY year DESC, 
                        FIELD(month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
                        ")->result_array();


        foreach ($query as $k) {
            unset($rmonths[$k['data_month']]);
        }
        $renamed_arrays = [];
        $arr = [];
        for ($i = 0; $i < count($rmonths); $i++) {
            array_push($arr, $i);
        }
        $new_arr1 = $this->rename_keys($rmonths, $arr);
        array_push($renamed_arrays, $new_arr1);

        $merged = array_merge($query, $renamed_arrays[0]);
        usort($merged, function ($a, $b) {
            $t1 = strtotime($a['data_month']);
            $t2 = strtotime($b['data_month']);
            return $t1 - $t2;
        });

        //  print_r($merged);

        return $merged;
    }

    function rename_keys($array, $replacement_keys) {
        return array_combine($replacement_keys, array_values($array));
    }

    public function get_transaction_table($drug_id, $period_year) {
        $responses = array();
        $headers = array();
        $widths = array();
        $columns = array();
        $alignments = array();
        $column_indices = array('B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M');
        $drug_name = '';
        $response = $this->Procurement_model->get_transaction_data($drug_id, $period_year);
        //Initial sidebar labels
        $responses = array(
            'open_kemsa' => array('Open Balance'),
            'proposed' => array('Proposed'),
            'contracted' => array('Contracted'),
            'received' => array('Received'),
            'issues_kemsa' => array('Issues to Facility'),
            'close_kemsa' => array('Closing Balance'),
            'monthly_consumption' => array('Monthly Consumption'),
            'adjustments_loss' => array('Adjustment/Loss'),
            'avg_issues' => array('Average Issues'),
            'avg_consumption' => array('Average Consumption'),
            'mos' => array('Months of Stock'),
            'mosbc' => array('MOS on Consumption')
        );
        $headers[] = 'Description';
        $widths[] = '160';
        $columns[] = array('type' => 'text', 'readOnly' => true, 'class' => 'data');
        $alignments[] = 'left';
        foreach ($response['data'] as $key => $value) {
            $headers[] = $value['period'];
            $widths[] = '80';
            $columns[] = array('type' => 'numeric');
            $alignments[] = 'center';
            //Put formaulas
            if ($key == 0) {
                $drug_name = $value['drug'];
                $responses['open_kemsa'][] = $value['open_kemsa'];
            } else {
                $responses['open_kemsa'][] = ' = ' . $column_indices[$key - 1] . '4';
            }
            $responses['proposed'][] = $value['receipts_kemsa'];
            $responses['contracted'][] = $value['receipts_kemsa'];
            $responses['received'][] = $value['receipts_kemsa'];
            $responses['issues_kemsa'][] = $value['issues_kemsa'];
            $responses['close_kemsa'][] = ' = ' . $column_indices[$key] . '1+' . $column_indices[$key] . '2-' . $column_indices[$key] . '3';
            $responses['monthly_consumption'][] = $value['monthly_consumption'];
            $responses['adjustments_loss'][] = $value['monthly_consumption'];
            $responses['avg_issues'][] = $value['avg_issues'];
            $responses['avg_consumption'][] = $value['avg_consumption'];
            $responses['mos'][] = $value['mos'];
            $responses['mosbc'][] = $value['mos'];
        }
        echo json_encode(array(
            'data' => array_values($responses),
            'colHeaders' => $headers,
            'colWidths' => $widths,
            'columns' => $columns,
            'colAlignments' => $alignments,
            'csvFileName' => $drug_name . ' Procurement Tracker for ' . $period_year,
            'columnSorting' => false,
            'csvHeaders' => true
                ), JSON_NUMERIC_CHECK);
    }

    public function get_order_table($drug_id) {
        $response = $this->Procurement_model->get_order_data($drug_id);

        $html_table = '<table class = "table table-condensed table-striped table-bordered order_tbl">';
        $thead = '<thead><tr>';
        $tbody = '<tbody>';
        foreach ($response['data'] as $count => $values) {
            $tbody .= '<tr>';
            foreach ($values as $key => $value) {
                if ($count == 0) {
                    $thead .= '<th>' . $key . '</th>';
                }
                $tbody .= '<td>' . $value . '</td>';
            }
            $tbody .= '</tr>';
        }
        $thead .= '</tr></thead>';
        $html_table .= $thead;
        $html_table .= $tbody;
        $html_table .= '</tbody></table>';
        echo $html_table;
    }

    public function get_order_table_history($drug_id, $year) {
        $response = $this->Procurement_model->get_history_data($drug_id, $year);

        $html_table = '<table class = "table table-condensed table-striped table-bordered order_tbl_history">';
        $thead = '<thead><tr>';
        $tbody = '<tbody id="TransactionData">';
        foreach ($response['data'] as $count => $values) {
            $tbody .= '<tr>';
            foreach ($values as $key => $value) {
                if ($count == 0) {
                    $thead .= '<th>' . $key . '</th>';
                }
                $tbody .= '<td>' . $value . '</td>';
            }
            $tbody .= '</tr>';
        }
        $thead .= '</tr></thead>';
        $html_table .= $thead;
        $html_table .= $tbody;
        $html_table .= '</tbody></table>';
        echo $html_table;
    }

    public function get_order_table_history_($drug_id) {
        $response = $this->Procurement_model->get_history_data_($drug_id);

        $html_table = '<table class = "table table-condensed table-striped table-bordered order_tbl_history_">';
        $thead = '<thead><tr>';
        $tbody = '<tbody id="TransactionData_">';
        foreach ($response['data'] as $count => $values) {
            $tbody .= '<tr>';
            foreach ($values as $key => $value) {
                if ($count == 0) {
                    $thead .= '<th>' . $key . '</th>';
                }
                $tbody .= '<td>' . $value . '</td>';
            }
            $tbody .= '</tr>';
        }
        $thead .= '</tr></thead>';
        $html_table .= $thead;
        $html_table .= $tbody;
        $html_table .= '</tbody></table>';
        echo $html_table;
    }

    public function get_log_table($drug_id) {
        $response = $this->Procurement_model->get_log_data($drug_id);
        $html_table = '<table class = "table table-condensed table-striped table-bordered log_tbl">';
        $thead = '<thead><tr>';
        $tbody = '<tbody>';
        foreach ($response['data'] as $count => $values) {
            $tbody .= '<tr>';
            foreach ($values as $key => $value) {
                if ($count == 0) {
                    $thead .= '<th>' . $key . '</th>';
                }
                $tbody .= '<td>' . $value . '</td>';
            }
            $tbody .= '</tr>';
        }
        $thead .= '</tr></thead>';
        $html_table .= $thead;
        $html_table .= $tbody;
        $html_table .= '</tbody></table>';
        echo $html_table;
    }

    public function get_default_period() {
        $default_period = array(
            'year' => $this->config->item('data_year'),
            'month' => $this->config->item('data_month'),
            'drug' => $this->config->item('drug'));
        echo json_encode($default_period);
    }

    public function get_chart() {
        $chartname = $this->input->post('name');
        $selectedfilters = $this->get_filter($chartname, $this->input->post('selectedfilters'));
        //Set filters based on role and scope
        $role = $this->session->userdata('role');
        if (!in_array($role, array('admin', 'nascop'))) {
            $selectedfilters[$role] = $this->session->userdata('scope_name');
        }
        //Get chart configuration
        $data['chart_name'] = $chartname;
        $data['chart_title'] = $this->config->item($chartname . '_title');
        $data['chart_yaxis_title'] = $this->config->item($chartname . '_yaxis_title');
        $data['chart_xaxis_title'] = $this->config->item($chartname . '_xaxis_title');
        $data['chart_source'] = $this->config->item($chartname . '_source');
        //Get data
        $main_data = array('main' => array(), 'drilldown' => array(), 'columns' => array());
        $main_data = $this->get_data($chartname, $selectedfilters);
        if ($this->config->item($chartname . '_has_drilldown')) {
            $data['chart_drilldown_data'] = json_encode(@$main_data['drilldown'], JSON_NUMERIC_CHECK);
        } else {
            $data['chart_categories'] = json_encode(@$main_data['columns'], JSON_NUMERIC_CHECK);
        }
        $data['selectedfilters'] = htmlspecialchars(json_encode($selectedfilters), ENT_QUOTES, 'UTF-8');
        $data['chart_series_data'] = json_encode($main_data['main'], JSON_NUMERIC_CHECK);
        //Load chart
        $this->load->view($this->config->item($chartname . '_chartview'), $data);
    }

    public function get_filter($chartname, $selectedfilters) {
        $filters = $this->config->item($chartname . '_filters_default');
        $filtersColumns = $this->config->item($chartname . '_filters');

        if (!empty($selectedfilters)) {
            foreach (array_keys($selectedfilters) as $filter) {
                if (in_array($filter, $filtersColumns)) {
                    $filters[$filter] = $selectedfilters[$filter];
                }
            }
        }
        return $filters;
    }

    public function get_data($chartname, $filters) {
        if ($chartname == 'live_stock_receipts_trend_chart') {
            $main_data = $this->Procurement_model->live_stock_receipts_trend($filters);
        } else if ($chartname == 'live_stock_issues_trend_chart') {
            $main_data = $this->Procurement_model->live_stock_issues_trend_chart($filters);
        } else if ($chartname == 'actual_consumption_issues_chart') {
            $main_data = $this->Procurement_model->get_procurement_actual_consumption_issues($filters);
        } else if ($chartname == 'kemsa_soh_chart') {
            $main_data = $this->Procurement_model->get_procurement_kemsa_soh($filters);
        } else if ($chartname == 'adult_patients_on_drug_chart') {
            $main_data = $this->Procurement_model->get_procurement_adult_patients_on_drug($filters);
        } else if ($chartname == 'paed_patients_on_drug_chart') {
            $main_data = $this->Procurement_model->get_procurement_paed_patients_on_drug($filters);
        } else if ($chartname == 'stock_status_chart') {
            $main_data = $this->Procurement_model->get_procurement_stock_status($filters);
        } else if ($chartname == 'expected_delivery_chart') {
            $main_data = $this->Procurement_model->get_procurement_expected_delivery($filters);
        }
        return $main_data;
    }

    public function edit_order() {
        //get_order_table_historyecho $this->input->post('drug_id');
        $input = $this->input->post();
        //Evaluate type of action
        if ($input['action'] == 'edit') {
            unset($input['action']);
            $this->Procurement_model->edit_procurement_item($input);
            $this->updateSysLogs('Updated (Order edited )');
            $input['action'] = 'edit';
        } else if ($input['action'] == 'delete') {
            unset($input['action']);
            $this->Procurement_model->delete_procurement_item($input['id']);
            $input['action'] = 'delete';
            $this->updateSysLogs('Deleted (Order deleted )');
        }
        echo json_encode($input);
    }

    function get_order_items() {
        header("Content-Type: application/json; charset=UTF-8");
        $response = array();
        $item_urls = array(
            'status' => $this->getStatuses('procurement_status'),
            'funding' => $this->getStatuses('funding_agent'),
            'supplier' => $this->getStatuses('supplier')
        );

        foreach ($item_urls as $key => $item_url) {
            if ($key != 'status') {
                $response[$key][0] = 'Select one';
            }
            foreach (json_decode(json_encode($item_url), TRUE) as $values) {
                $response[$key][$values['id']] = $values['name'];
            }
        }
        echo json_encode($response);
    }

    function getStatuses($table) {
        // header("Content-Type: application/json; charset=UTF-8");
        $resp = $this->db->get('tbl_' . $table)->result();
        return $resp;
    }

    function memberUpdates() {
        $present = $this->input->post('present');
        $absent = $this->input->post('absent');

        for ($i = 0; $i < count($present); $i++) {

            $this->db->where('email', $present[$i])->update('tbl_mailing_list', ['present' => 0]);
        }
        for ($j = 0; $j < count($absent); $j++) {
            $this->db->where('email', $absent[$j])->update('tbl_mailing_list', ['present' => 1]);
        }
        $success = ['success' => 1];
        $this->response($success);
    }

    function membersListAdd() {
        $resp['response'] = '';
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        if ($this->checkEmail($email) > 0) {
            $resp['response'] = 0;
        } else {
            $this->db->insert('tbl_mailing_list', [
                'name' => $name,
                'email' => $email,
                'email_type' => 1,
                'sent_date' => date('Y-m-d H:i:s'),
                'status' => 1,
                'present' => 0,
            ]);
            $resp['response'] = 1;
        }
        $this->response($resp['response']);
    }

    function checkEmail($param) {
        $email = $this->db->where('email', $param)->get('tbl_mailing_list')->result();
        return count($email);
    }

    function getEmails() {

        $present = $this->db->where('present', '0')->get('tbl_mailing_list')->result();
        $absent = $this->db->where('present', '1')->get('tbl_mailing_list')->result();
        $result = ['present' => $present, 'absent' => $absent];
        $this->response($result);
    }

    function response($result) {
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($result);
    }

    function createTrackerYearly() {
        $year = date('Y');
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $query = $this->db->query("SELECT * FROM `vw_drug_list` ORDER BY id ASC")->result();
        foreach ($query as $d):
            $drug = $d->id;
            $kemsa_code = $d->kemsa_code;
            for ($i = 0; $i < count($months); $i++) {
                $this->db->query("SET FOREIGN_KEY_CHECKS = 0 ");
                $this->db->query("INSERT INTO `tbl_procurement` (`open_kemsa`, `receipts_kemsa`, `receipts_usaid`, `receipts_gf`, `receipts_cpf`, `issues_kemsa`, `close_kemsa`, `monthly_consumption`, `transaction_year`, `transaction_month`, `drug_id`, `adj_losses`, `kemsa_code`)
             VALUES ('0', '0', '0', '0', '0', '0', '0', '0', '$year', '$months[$i]', '$drug', '0', '$kemsa_code');");
            }
        endforeach;

        echo 'Updated Tracker Year';
    }

    function createTracker() {

        $year = date('Y');
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $query = $this->db->query("SELECT * FROM `vw_drug_list` ORDER BY id ASC")->result();
        foreach ($query as $d):
            $drug_id = $d->id;
            for ($i = 0; $i < count($months); $i++) {
                $this->db->query("SET FOREIGN_KEY_CHECKS = 0 ");
                $this->db->query("INSERT INTO `tbl_procurement` (`open_kemsa`, `receipts_kemsa`, `receipts_usaid`, `receipts_gf`, `receipts_cpf`, `issues_kemsa`, `close_kemsa`, `monthly_consumption`, `transaction_year`, `transaction_month`, `drug_id`, `adj_losses`, `kemsa_code`)
             VALUES ('0', '0', '0', '0', '0', '0', '0', '0', '$year', '$months[$i]', '$drug_id', '0', '');");
            }
        endforeach;
        echo 'Updated Tracker Year';
    }

    function createProcurement() {
        $this->db->query("TRUNCATE tbl_procurement_item");
        $year = date('Y');
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $query = $this->db->query("SELECT * FROM `vw_drug_list` ORDER BY id ASC")->result();
        foreach ($query as $d):
            $drug_id = $d->id;
            for ($i = 0; $i < count($months); $i++) {
                $this->db->query("SET FOREIGN_KEY_CHECKS = 0 ");
                $this->db->query("INSERT INTO `tbl_procurement_item` (`year`, `month`, `drug_id`)
             VALUES ('$year', '$months[$i]', '$drug_id');");
            }
        endforeach;
        //$this->db->query("UPDATE tbl_procurement p SET kemsa_code = (SELECT kemsa_code FROM tbl_drug  WHERE id = p.drug_id)");
        echo 'Updated Tracker Procurement Year';
    }

    function updateConsumption() {
        $year = date('Y');
        //$months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $this->db->query("SET FOREIGN_KEY_CHECKS = 0 ");
        $drug_ = $this->db->query("SELECT id,name FROM vw_drug_list")->result();

        foreach ($drug_ as $d) {
            $drug_name = $d->name;
            $drug = $d->id;
            $consumption = $this->db->query("SELECT data_month,SUM(total) consumption
                FROM `dsh_consumption`
                WHERE `data_year` = '$year'  AND `drug` = '$drug_name'
                GROUP BY data_month
                ORDER BY  FIELD(data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )")->result();
            for ($i = 0; $i < count($consumption); $i++) {
                $cons = $consumption[$i]->consumption;
                $month = $consumption[$i]->data_month;
                $this->db->query("UPDATE `tbl_procurement` SET `monthly_consumption` = '$cons' WHERE transaction_year='$year' AND transaction_month='$month' AND drug_id='$drug'");
            }
        }
        echo 'Consumption Updated';
    }

    function updateProcurement($year, $drug) {
        $drug_ = $this->db->query("SELECT * FROM vw_procurement_totals WHERE drug_id='$drug' AND cyear='$year'")->result();
        for ($i = 0; $i < count($drug_); $i++) {
            $total = $drug_[$i]->total;
            $month = $drug_[$i]->cmonth;
            $this->db->query("UPDATE `tbl_procurement_item` SET `calldown` = '$total' WHERE year='$year' AND month='$month' AND drug_id='$drug'");
        }
    }

    function dhisCentral() {
        //error_reporting(0);
        $url = "https://hiskenya.org/api/organisationUnitGroupSets/R0JQ0AqdsZf/organisationUnitGroups?fields=:id,name,code&paging=false";

// Send request to Server
        $ch = curl_init($url);
// To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "aochieng:Chai@254");
// Get response
        $response = curl_exec($ch);
// Decode
        $result = json_decode($response);
        $final_res = $result->organisationUnitGroups;
        //$this->prettyPrint($final_res);


        for ($i = 0; $i < count($final_res); $i++) {
            $code = trim(str_replace('TB_SubCountyStore_', '', $final_res[$i]->code));
            $dhis_code = $final_res[$i]->id;
            $facilities = $this->db->where('mflcode', $code)->get('tbl_facility')->result()[0]->mflcode;
            if ($facilities == $code) {
                //echo $code . '<br>';
                $this->db->query("UPDATE tbl_facility SET dhiscode='$dhis_code' WHERE mflcode='$code'");
            }
        }
        echo 'Updated ';
        //echo $code . ' Rows Affected';
    }

    /* SELECT f.mflcode,UPPER(f.name) facility,ci.qty_allocated
      FROM tbl_cdrr cd
      INNER JOIN tbl_cdrr_item ci ON cd.id = ci.cdrr_id
      INNER JOIN tbl_facility f ON cd.facility_id = f.id
      INNER JOIN vw_drug_list d ON ci.drug_id = d.id
      WHERE ci.drug_id='40'
      AND cd.period_begin >='2019-03-01'
      AND ci.qty_allocated IS NOT NULL
      AND ci.qty_allocated != 0
      ORDER BY ci.qty_allocated DESC */

    function prettyPrint($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die;
    }

}
