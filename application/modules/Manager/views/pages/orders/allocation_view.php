<div id="page-wrapper">
    <!--row-->
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb page-header">
                <li><a href="<?php echo base_url('manager/dashboard'); ?>">Dashboard</a></li>
                <li><a href="<?php echo base_url('manager/orders/reports'); ?>">Orders</a></li>
                <li class="active breadcrumb-item"><i class="white-text" aria-hidden="true"></i> <?php echo ucwords($page_name); ?></li>
                <li><span class="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#helpModal"></span></li>

            </ol>
        </div>
    </div><!--end row-->
    <!--row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default"><!--panel default-->
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <?php if ($this->session->userdata('role') == 'nascop') { ?>
                        <a href="#uploadTemplateGuide" style="margin: 10px;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" data-keyboard="false" data-backdrop="static">Upload KEMSA MOS Template Guide</a>

                    <?php } else { ?>

                    <?php } ?>

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-listing">
                        <thead>
                            <tr>
                                <?php
                                foreach ($columns as $column) {
                                    echo "<th>" . ucwords($column) . "</th>";
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>

                    <span style="color:blue; font-weight: bold;">
                        **Description "Report Incomplete" - Either D-CDRR, D-MAPS, F-CDRR, F-MAPS has not been reported <br>
                        All the **4 reports **must first be dully entered in DHIS2 so as to enable Allocation.
                    </span>
                </div>
            </div><!--end panel default-->
        </div>
    </div><!--end row-->
</div><!--end page wrapper--->



<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Template</h4>
            </div>
            <div class="modal-body">
                <form id="myform" method="post">
                    <div class="form-group">
                        <input  type="file" id="myfile" />
                    </div>
                    <div class="form-group">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
                        </div>

                        <div class="msg"></div>

                    </div>

                    <input type="button" id="btn" class="btn-success" value="Upload" />
                </form>
            </div>
            <!--                <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>-->
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        current = "<?php echo current_url(); ?>";
        window.localStorage;
        localStorage.setItem("return_url_sc", current);

        d = new Date();
        n = d.getFullYear();
        var monthNames = ["January-" + n, "February-" + n, "March-" + n,
            "April-" + n, "May-" + n, "June-" + n,
            "July-" + n, "August-" + n, "September-" + n,
            "October-" + n, "November-" + n, "December-" + n];

        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "month-name-asc": function (a, b) {
                var xPos = 0, yPos = 0;
                $.each(monthNames, function (k, v) {
                    if (a == v)
                        xPos = k;
                    if (b == v)
                        yPos = k;
                });
                return ((xPos < yPos) ? -1 : ((xPos > yPos) ? 1 : 0));
            },

            "month-name-desc": function (a, b) {
                var xPos = 0, yPos = 0;
                $.each(monthNames, function (k, v) {
                    if (a == v)
                        xPos = k;
                    if (b == v)
                        yPos = k;
                });
                return ((xPos > yPos) ? -1 : ((xPos < yPos) ? 1 : 0));
            }
        });




        var role = "<?php echo $this->session->userdata('role'); ?>";



        if (role == 'nascop' || role == 'county' || role == 'partner') {
            //alert(1)
            $('#dataTables-listing').DataTable({
                responsive: true,
                ordering: false,
                pagingType: "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: "<?php echo base_url() . 'Manager/Orders/get_allocation'; ?>",
                initComplete: function () {
                    this.api().columns([0, 2]).every(function () {
                        var column = this;
                        var select = $('<br/><select><option value="">Show all</option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            var val = $('<div/>').html(d).text();
                            select.append('<option value="' + val + '">' + val + '</option>');
                        });
                    });
                }
            });
        } else {
            dtable = $('#dataTables-listing').DataTable({
                responsive: true,
                ordering: false,
                pagingType: "full_numbers",
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: "<?php echo base_url() . 'Manager/Orders/get_allocation'; ?>",
                initComplete: function () {
                    this.api().columns([0, 1, 2]).every(function () {
                        var column = this;
                        var select = $('<br/><select><option value="">Show all</option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            var val = $('<div/>').html(d).text();
                            select.append('<option value="' + val + '">' + val + '</option>');
                        });
                    });
                    //Show reporting rate
                    var thecount = dtable.rows().column(4).data()
                            .filter(function (value, index) {
                                return value !== 'PENDING';
                            }).length;
                    var reporting_rate = Math.ceil((thecount / dtable.rows().count()) * 100);
                    // var reporting_rate = Math.ceil(($("#dataTables-listing td:nth-child(5):contains('allocated'),#dataTables-listing td:nth-child(5):not(:contains('PENDING'))").length / this.api().data().rows().count()) * 100)
                    $('.panel-heading').html('Allocation Rate: <b>' + reporting_rate + '%</b><div class="progress"><div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="' + reporting_rate + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + reporting_rate + '%;">' + reporting_rate + '%</div></div>')
                }
            });
        }

        //Show Allocation sidemenu
        $(".allocation").closest('ul').addClass("in");
        $(".allocation").addClass("active active-page");

        $('#btn').click(function () {
            $('.myprogress').css('width', '0');
            $('.msg').text('');
            $('#PROGRESS').show();

            var formData = new FormData();
            formData.append('myfile', $('#myfile')[0].files[0]);
            formData.append('filename', 'inventory');
            $('#btn').attr('disabled', 'disabled').hide();
            img = '<img src="<?php echo base_url(); ?>public/spinner.gif" width="50px"/>';
            $('.msg').html(img + " Updating Template Please Wait...");
            $.ajax({
                url: "<?php echo base_url(); ?>Manager/Orders/upload",
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                // this part is progress bar
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    alert(data)
                    if (data === 'Template Successfully Uploaded') {
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('#btn').addAttr('disabled');
                    } else {
                        $('.myprogress').css('width', '0');
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('.myprogress').text(0 + '%');
                        $('#btn').show();
                        $('#btn').removeAttr('disabled');

                    }
                }
            });
        });



        $('#btn_tracker').click(function () {
            $('.myprogress').css('width', '0');
            $('.msg').text('');
            $('#PROGRESS').show();

            var formData = new FormData();
            formData.append('myfile_tracker', $('#myfile_tracker')[0].files[0]);
            formData.append('filename', 'inventory');
            $('#btn_tracker').attr('disabled', 'disabled').hide();
            img = '<img src="<?php echo base_url(); ?>public/spinner.gif" width="50px"/>';
            $('.msg').html(img + " Updating Template Please Wait...");
            $.ajax({
                url: "<?php echo base_url(); ?>Manager/Orders/tracker",
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                // this part is progress bar
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    if (data === 'Template Successfully Uploaded') {
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('#btn_tracker').addAttr('disabled');
                        swal({
                            title: "Tracker Update Status",
                            text: data,
                            icon: "success",
                        }).then(function () {

                        });
                    } else {
                        $('.myprogress').css('width', '0');
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('.myprogress').text(0 + '%');
                        $('#btn_tracker').show();
                        $('#btn_tracker').removeAttr('disabled');

                    }
                }
            });
        });



        $('#btn_forecast_tracker').click(function () {
            $('.myprogress').css('width', '0');
            $('.msg').text('');
            $('#PROGRESS').show();

            var formData = new FormData();
            formData.append('myfile_forecast', $('#myfile_forecast')[0].files[0]);
            formData.append('filename', 'inventory');
            $('#btn_tracker').attr('disabled', 'disabled').hide();
            img = '<img src="<?php echo base_url(); ?>public/spinner.gif" width="50px"/>';
            $('.msg').html(img + " Updating Template Please Wait...");
            $.ajax({
                url: "<?php echo base_url(); ?>Manager/Orders/forecast",
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                // this part is progress bar
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    if (data === 'Template Successfully Uploaded') {
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('#btn_tracker').addAttr('disabled');
                        swal({
                            title: "Tracker Update Status",
                            text: data,
                            icon: "success",
                        }).then(function () {

                        });
                    } else {
                        $('.myprogress').css('width', '0');
                        $('.msg').html('');
                        $('.msg').html('');
                        $('.msg').html(data);
                        $('.myprogress').text(0 + '%');
                        $('#btn_forecast_tracker').show();
                        $('#btn_forecast_tracker').removeAttr('disabled');

                    }
                }
            });
        });
    });
</script>