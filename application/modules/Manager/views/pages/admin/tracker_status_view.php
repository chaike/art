<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">FUNDING</label>
                            <div class="col-md-9">
                                <select class="form-control" id="fundings" name="fund_id"></select>
                                <span class="help-block"></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3">TENDER NUMBER</label>
                            <div class="col-md-9">
                                <input class="form-control" id="tender" name="tender_number" placeholder="Tender Number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">ITEM</label>
                            <div class="col-md-9">
                                <select class="form-control" id="drugs_load" name="drug_id"></select>
                                <span class="help-block"></span>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-md-3">CONTRACT DATE</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="date" class="form-control" id="setup_date_" name="contract_date" placeholder="Contracted Date" required="">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">DUE DATE</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="date" class="form-control" id="upgrade_date_" name="due_date" placeholder="Due Date" required="">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label class="control-label col-md-3">SUPPLIER</label>
                            <div class="col-md-9">
                                <select class="form-control" id="supplier" name="supplier_id"></select>
                                <span class="help-block"></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-3">PROPOSED QUANTITY</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="proposed" name="proposed_qty" placeholder="Proposed Quantity">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">CONTRACTED QUANTITY</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="contracted" name="contracted_qty" placeholder="Contracted Quantity">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">DELIVERIES</label>
                            <div class="col-md-9">
                                <table class="table table-bordered table-condensed">
                                    <thead>
                                    <th>Qty Delivered</th>
                                    <th>Date Delivered</th>
                                    <th><a href="#add" id="ADD_ROW" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a></th>
                                    </thead>
                                    <tbody id="deliveriesTable">
                                        <tr>
                                            <td><input type="text" name="quantities[]" class="form-control"></td>
                                            <td><input type="date"  name="dates[]"  class="form-control SELECTEDDATES"></td>
                                            <td><a href="#remove" class="btn btn-sm btn-danger REM_ROW"><i class="fa fa-minus"></i></a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">DELIVERED QUANTITY</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="delivered" name="delivered_qty" placeholder="Delivered Quantity">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">PENDING QUANTITY</label>
                            <div class="col-md-9">
                                <input class="form-control" type="number" id="pending"  name="pending_qty" placeholder="Pending Quantity">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">ACTUAL DUE DATE</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="date" class="form-control" id="actual_delivery_date" name="actual_delivery_date" placeholder="Actual Due Date" required="">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-md-3">STATUS</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text"  id="status_of_tender" name="status_of_tender" placeholder="Tender Status">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">COMMENTS</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="comments" name="comments" placeholder="comments"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>                    
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btnSave" onclick="save()">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div><!--End Modal install Site-->

<script type="text/javascript">
    var userURL = '../../API/user';

    $(function () {
        
          

        //Gets all users
        $.getJSON("<?php echo base_url(); ?>Manager/Procurement/loadStatuses", function (data) {
            $("#fundings").empty();
            $("#fundings").append($("<option value=''>Select Funding Agent</option>"));
            $.each(data, function (i, v) {
                $("#fundings").append($("<option value='" + v.id + "'>" + v.name.toUpperCase() + "</option>"));
            });
        });

        $('#delivered,#contracted').keyup(function () {
            contracted = parseInt($('#contracted').val());
            delivered = parseInt($('#delivered').val());
            if (contracted !== delivered) {
                $('#pending').val(contracted - delivered);
                $('#status_of_tender').val('PENDING');
            } else {
                $('#pending').val(0);
                $('#status_of_tender').val('FULLY DELIVERED');

            }

        });

        $.getJSON("<?php echo base_url(); ?>Manager/Procurement/loadProducts", function (data) {
            $("#drugs_load").empty();
            $("#drugs_load").append($("<option value=''>Select Item</option>"));
            $.each(data, function (i, v) {
                $("#drugs_load").append($("<option value='" + v.id + "'>" + v.name.toUpperCase() + ' | ' + v.drug_category.toUpperCase() + "</option>"));
            });
        });

        $.getJSON("<?php echo base_url(); ?>Manager/Procurement/loadSuppliers_", function (data) {
            $("#supplier").empty();
            $("#supplier").append($("<option value=''>Select Supplier</option>"));
            $.each(data, function (i, v) {
                $("#supplier").append($("<option value='" + v.id + "'>" + v.name.toUpperCase() + "</option>"));
            });
        });

        $('#addRow').click(function () {
            tr = '<tr> <td> <select class="form-control select2" name="status_of_tender[]" > <option value="">-Select Option-</option> <option value="Fully Delivered">Fully Delivered</option><option value="Delivered">Delivered</option> <option value="Pending">Pending</option> </option> <option value="Cancelled">Cancelled</option> </select> </td> <td> <input class="form-control" type="number" id="quantity" name="quantity[]" placeholder="Quantity"> </td> <td> <input class="form-control" type="date" id="status_date" name="status_date[]" placeholder="date"> </td> <td><span class="btn btn-sm btn-danger remRow">-Rem</span></td> </tr>';
            $('#mainArea').append(tr);
        });

        $(document).on('click', '.remRow', function () {
            $(this).closest('tr').remove();
        });
        $('#ADD_ROW').click(function () {
            tr = '<tr><td><input type="text" name="quantities[]" class="form-control"></td> <td><input type="date"  name="dates[]"  class="form-control SELECTEDDATES"></td> <td><a href="#remove" class="btn btn-sm btn-danger REM_ROW"><i class="fa fa-minus"></i></a></td> </tr>';
            $('#deliveriesTable').append(tr);
        });

        $(document).on('click', '.REM_ROW', function () {
            $(this).closest('tr').remove();
        });

        //EMRS Used multiselect
        //$('#emrs_used').multiselect();

        //Deselect on-mouse click
        $('select option').on('mousedown', function (e) {
            this.selected = !this.selected;
            e.preventDefault();
        });

        //Date picker setup_date
        $.fn.datepicker.defaults.format = "yyyy/mm/dd";
        $.fn.datepicker.defaults.endDate = '0d';
        $('#setup_date').datepicker({
        });

        //Date picker upgrade_date
        $.fn.datepicker.defaults.format = "yyyy/mm/dd";
        $.fn.datepicker.defaults.endDate = '0d';
        $('#upgrade_date').datepicker({
        });

        //$('.emrs_option').hide(true);
    });
</script>