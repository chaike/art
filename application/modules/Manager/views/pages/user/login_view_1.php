<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Twitter -->
        <meta name="twitter:site" content="@themepixels">
        <meta name="twitter:creator" content="@themepixels">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Azia">
        <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
        <meta name="twitter:image" content="http://themepixels.me/azia/img/azia-social.png">

        <!-- Facebook -->
        <meta property="og:url" content="http://themepixels.me/azia">
        <meta property="og:title" content="Azia">
        <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

        <meta property="og:image" content="http://themepixels.me/azia/img/azia-social.png">
        <meta property="og:image:secure_url" content="http://themepixels.me/azia/img/azia-social.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="600">

        <!-- Meta -->
        <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
        <meta name="author" content="ThemePixels">

        <title>ART | Login</title>

        <!-- vendor css -->
        <link href="<?php echo base_url(); ?>public/manager_v2/azia/lib/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/manager_v2/azia/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/manager_v2/azia/lib/typicons.font/typicons.css" rel="stylesheet">

        <!-- azia CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/manager_v2/azia/css/azia.css">

    </head>
    <body class="az-body">

        <div class="az-signin-wrapper">
            <div class="az-card-signin">
                <div class="az-logo"><img src="<?php echo base_url(); ?>public/manager_v2/azia/img/nascoplogin.png" alt="NASCOP Logo"/></div>
                <div class="az-signin-header">
                    <?php if (empty($this->session->flashdata('user_msg'))) { ?>
                        <h2>Welcome back!</h2>
                        <h4>Please sign in to continue</h4>
                    <?php } else { ?>
                        <div class="alert alert-outline-danger"><?php echo $this->session->flashdata('user_msg'); ?></div>  
                    <?php } ?>

                    <form  action="<?php echo base_url() . 'user/authenticate'; ?>" method="POST">
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email_address" type="email"  class="form-control" placeholder="Enter your email" placeholder="demo@gmail.com">
                        </div><!-- form-group -->
                        <div class="form-group">
                            <label>Password</label>
                            <input name="password" type="password"  class="form-control" placeholder="Enter your password" placeholder="thisisademo">
                        </div><!-- form-group -->
                        <button class="btn btn-az-primary btn-block">Sign In</button>
                    </form>
                </div><!-- az-signin-header -->
                <div class="az-signin-footer">
                    <p><a href="">Forgot password?</a></p>
                    <p>Don't have an account? <a href="#Request-Account" data-toggle="modal" data-target="#RequestAccount">Request For an Account</a></p>
                </div><!-- az-signin-footer -->
            </div><!-- az-card-signin -->
        </div><!-- az-signin-wrapper -->

        <div id="RequestAccount" class="modal fade effect-rotate-bottom" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Request Account</h4>
                    </div>
                    <div class="modal-body">

                        <form>
                            <div class="form-group">
                                <label for="email">Your Name:</label>
                                <input type="text" class="form-control" id="NAME" placeholder="Enter your name">
                            </div>
                            <div class="form-group">
                                <label for="email">Phone:</label>
                                <input type="text" class="form-control" id="TPhone" placeholder="Enter Your Phone">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address:</label>
                                <input type="email" class="form-control" id="yemail" placeholder="Enter Your Email">
                            </div>
                            <div class="form-group">
                                <label for="email">County</label>
                                <input type="email" class="form-control" id="countyy" placeholder="Enter County if there is..">
                            </div>
                            <div class="form-group">
                                <label for="email">Sub-County</label>
                                <input type="email" class="form-control" id="subcountyy" placeholder="Enter Sub County if there is..">
                            </div>
                            <div class="form-group">
                                <label for="reason">Reason for requesting:</label>
                                <textarea id="yreason" class="form-control" placeholder="State why you need this account"></textarea>
                            </div>


                        </form> 

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default btn-primary" id="sendEmail">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <script src="<?php echo base_url(); ?>public/manager_v2/azia/lib/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/manager_v2/azia/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url(); ?>public/manager_v2/azia/lib/ionicons/ionicons.js"></script>

        <script src="<?php echo base_url(); ?>public/manager_v2/azia/js/azia.js"></script>
        <script>
            $(function () {
                $('#sendEmail').click(function () {

                    email = $('#yemail').val();
                    message = $('#yreason').val();
                    name = $('#NAME').val();
                    phone = $('#TPhone').val();
                    county = $('#countyy').val();
                    subcounty = $('#subcountyy').val();

                    if (email == '' || message == '' || name == '' || phone == '' || county == '' || subcounty == '') {
                        alert('Please fill all fields');
                    } else {

                        data = {
                            email: email,
                            message: message,
                            name: name,
                            phone: phone,
                            county: county,
                            subcounty: subcounty
                        };
                        $(this).prop('disabled', true);
                        $(this).prop('value', 'Please Wait...');
                        $.post("<?= base_url(); ?>Manager/newAccountRequestForOpening/", data, function (resp) {
                            if (resp.status == 'success') {
                                alert('We have received your request and will respond as soon as possible. \nThank you.');
                                window.location.href = "";
                            } else {
                                alert('Error: An error occured while submitting your request. Please try again later');
                            }

                        }, 'json');
                    }
                });

            });
        </script>
    </body>
</html>
