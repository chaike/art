<style>
    .highcharts-figure .chart-container {
        width: 300px;
        height: 200px;
        float: left;
    }

    .highcharts-figure, .highcharts-data-table table {
        width: 600px;
        margin: 0 auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    @media (max-width: 600px) {
        .highcharts-figure, .highcharts-data-table table {
            width: 100%;
        }
        .highcharts-figure .chart-container {
            width: 300px;
            float: none;
            margin: 0 auto;
        }

    }
</style>
<div class="az-content-breadcrumb">
    <span>SUMMARY <span class="MONTHLY_TITLE"> REPORT AS AT END OCT-2019</span></span>
    <span>2 Pager</span>
</div>
<h2 class="az-content-title">Stock Status</h2>

<div class="row row-sm ">
    <div class="col-sm-6 col-xl-3">
        <div class="card card-dashboard-twentytwo">
            <div class="media">
                <div class="media-icon bg-purple"><i class="typcn typcn-chart-line-outline"></i></div>
                <div class="media-body">
                    <h6><?php echo number_format($all_reports / $ordering_points * 100, 2); ?>%<small class="up"><?php echo $all_reports . '/' . $ordering_points; ?></small></h6>
                    <span>Reporting Rate</span>
                </div>
            </div>
            <div class="chart-wrapper">
                <div id="flotChart1" class="flot-chart"></div>
            </div><!-- chart-wrapper -->
        </div><!-- card -->
    </div><!-- col -->
    <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
        <div class="card card-dashboard-twentytwo">
            <div class="media">
                <div class="media-icon bg-primary"><i class="typcn typcn-chart-line-outline"></i></div>
                <div class="media-body">
                    <h6><?php echo $kemsa . '/' . $ordering_points; ?><small class="up"><?php echo number_format($kemsa / $ordering_points * 100, 2); ?>%</small></h6>
                    <span>Submitted to KEMSA</span>
                </div>
            </div>
            <div class="chart-wrapper">
                <div id="flotChart2" class="flot-chart"></div>
            </div><!-- chart-wrapper -->
        </div><!-- card -->
    </div><!-- col-3 -->
    <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
        <div class="card card-dashboard-twentytwo">
            <div class="media">
                <div class="media-icon bg-red"><i class="fa fa-stop"></i></div>
                <div class="media-body">
                    <h6><?php echo $rejected . '/' . $ordering_points; ?><small class="down"><?php echo number_format($rejected / $ordering_points * 100, 2); ?>%</small></h6>
                    <span>Rejected</span>
                </div>
            </div>
            <div class="chart-wrapper">
                <div id="flotChart3" class="flot-chart"></div>
            </div><!-- chart-wrapper -->
        </div><!-- card -->
    </div><!-- col -->
    <div class="col-sm-3 col-xl-3 mg-t-20 mg-xl-t-0">
        <div class="card card-dashboard-twentytwo">
            <div class="media">
                <div class="media-icon bg-teal"><i class="typcn typcn-chart-line-outline"></i></div>
                <div class="media-body">
                    <h4><?php echo number_format($art); ?></h4>
                    <span>Current ART Patients</span>
                </div>
                <div class="media-body">
                    <h4><?php echo number_format($pmtct); ?></h4>
                    <span>PMTCT Patients</span>
                </div>
            </div>
            <div class="chart-wrapper">
                <div id="flotChart4" class="flot-chart"></div>
            </div><!-- chart-wrapper -->
        </div><!-- card -->
    </div>

</div>

<div class="row row-sm mt-4">
    <div class="col-lg-4">
        <div class="filter form-control" id="year-filter">
            <input type="hidden" name="filter_year" id="filter_year" value="<?php echo date('Y'); ?>" />
            Year: 
            <?php
            $start = (int) date('Y') - 4;
            $current = (int) date('Y');
            for ($i = $start; $i <= $current; $i++) {
                ?>                                              
                <a href="#" class="filter-year" data-value="<?= $i; ?>"> <?= $i; ?> </a>|                                 
            <?php } ?>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="filter form-control" id="month-filter">
            <input type="hidden" name="filter_month" id="filter_month" value="" />
            Month: 
            <a href="#" class="filter-month" data-value="Jan"> Jan </a>|
            <a href="#" class="filter-month" data-value="Feb"> Feb </a>|
            <a href="#" class="filter-month" data-value="Mar"> Mar </a>|
            <a href="#" class="filter-month" data-value="Apr"> Apr </a>|
            <a href="#" class="filter-month" data-value="May"> May </a>|
            <a href="#" class="filter-month" data-value="Jun"> Jun </a>|
            <a href="#" class="filter-month" data-value="Jul"> Jul </a>|
            <a href="#" class="filter-month" data-value="Aug"> Aug </a>|
            <a href="#" class="filter-month" data-value="Sep"> Sep </a>| 
            <a href="#" class="filter-month" data-value="Oct"> Oct </a>|
            <a href="#" class="filter-month" data-value="Nov"> Nov </a>|
            <a href="#" class="filter-month" data-value="Dec"> Dec</a>
        </div>
    </div>
    <div class="col-lg-3">
        <button id="btn_clear" class="btn btn-danger btn-md"><span class="fa fa-recycle"></span></button> 
        <button id="btn_filter" class="btn btn-warning btn-md"><span class="fa fa-filter"></span></button> 
    </div>
</div>




<div class="row row-sm mt-4">
    <div class="col-lg-6">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">National MoS Adults (Facilities + CENTRAL STORE)</h6>
                <span class="MONTHLY_TITLE"> REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">                
                <div class="chart-wrapper">
                    <div id="mos_fadult_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
    <div class="col-lg-6 mg-t-20 mg-lg-t-0 ">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">National MoS Paeds (Facilities + CENTRAL STORE) </h6>
                <span class="MONTHLY_TITLE">REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">   

                <div class="chart-wrapper">
                    <div id="mos_fpaeds_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
</div>


<div class="row row-sm mt-4">
    <div class="col-lg-6">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">CENTRAL STORE MoS Adults </h6>
                <span class="MONTHLY_TITLE"> REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">                
                <div class="chart-wrapper">
                    <div id="mos_adult_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
    <div class="col-lg-6 mg-t-20 mg-lg-t-0">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">CENTRAL STORE MoS Paeds  </h6>
                <span class="MONTHLY_TITLE">REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">   

                <div class="chart-wrapper">
                    <div id="mos_paeds_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
</div>

<div class="row row-sm mt-4">
    <div class="col-lg-6">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">Facility MoS Adults </h6>
                <span class="MONTHLY_TITLE"> REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">                
                <div class="chart-wrapper">
                    <div id="facility_mos_adult_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
    <div class="col-lg-6 mg-t-20 mg-lg-t-0 ">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">Facility MoS Paeds  </h6>
                <span class="MONTHLY_TITLE">REPORT AS AT END OCT-2019</span>
            </div><!-- card-header -->
            <div class="card-body">   

                <div class="chart-wrapper">
                    <div id="facility_mos_paeds_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
</div>


<div class="row row-sm mt-4">
    <div class="col-lg-12">
        <div class="card card-dashboard-twentyfour">
            <div class="card-header">
                <h6 class="card-title">Stock Status Per County</h6>
                <span class="MONTHLY_TITLE">REPORT AS AT END OCT-2019</span><br>
                <div><span><a href="#advancedFilter" class="btn btn-warning" data-toggle="modal" data-target="#advancedFilterModal">Advanced Filter</a></span></div>

            </div><!-- card-header -->
            <div class="card-body">
                <div class="card-body-top">
                    <div>

                        <label class="mos_county_chart_heading heading"</label>
                    </div>

                </div><!-- card-body-top -->

                <div class="d-flex justify-content-between mg-b-15">
                    <label class="az-content-label"></label>
                    <div class="chart-legend">
                    </div><!-- chart-legend -->
                </div>
                <div class="chart-wrapper">
                    <div id="mos_county_chart" class="flot-chart"></div>
                </div><!-- chart-wrapper -->
            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col -->
</div>




<!--Advanced Filter Modal -->
<div id="advancedFilterModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <input id="filterStatus" type="hidden"/>
        <!-- Filter Modal-->
        <div class="modal-content">
            <form id="AdvancedFilter">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Advanced Filter</b></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="scope_id" value="<?php echo $this->session->userdata('scope'); ?>"/>
                    <input type="hidden" id="scope" value="<?php echo $this->session->userdata('scope_name'); ?>"/>
                    <input type="hidden" id="role" value="<?php echo $this->session->userdata('role'); ?>"/>
                    <div class="form-group">
                        <select class="form-control drug subcounty_default county_default nascop_default hidden filter_item" data-item="drug" name="drug" size="2"></select>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal"  class="btn btn-warning" id="advFilterTTTTTTTT" ><i class="fa fa-filter"></i> Filter</button>
                </div>
            </form>
        </div>

    </div>
</div>


<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/datatables/js/jquery-1.12.4.js'; ?>"></script>

<!--dashboard-->
<script type="text/javascript" src="<?php echo base_url() . 'public/manager/js/2pager.js'; ?>"></script>
<script>

</script>
