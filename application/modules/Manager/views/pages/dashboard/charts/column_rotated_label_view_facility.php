<style>

</style>
<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>



<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>';
        Highcharts.wrap(Highcharts.Axis.prototype, 'getPlotLinePath', function (proceed) {
            var path = proceed.apply(this, Array.prototype.slice.call(arguments, 1));
            if (path) {
                path.flat = false;
            }
            return path;
        });

        Highcharts.chart(chartDIV, {
            legend: {
                enabled: true
            },
            chart: {
                type: 'column',
                height:600
            },

            colors: ['green', 'red', 'blue'],

            title: {
                text: ''
            },

            xAxis: {
                categories: <?php echo $chart_categories; ?>,
                crosshair: true,
                labels: {
                    rotation: 0,
                    style: {
                        // color: 'red',
                        fontSize: '9px'
                    }
                }

            },
            yAxis: [{
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    title: {
                        text: 'Months of Stock'
                    }
                }, {
                    min: 0,
                    max: 24,
                    tickInterval: 1,
                    opposite: true, //optional, you can have it on the same side.
                    title: {
                        text: 'Months of Stock'
                    },
                    plotLines: [{
                            value: 1,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            x: -10,
                            label: {
                                text: 'Under Stocked'
                            }
                        }, {
                            value: 3,
                            color: 'black',
                            dashStyle: 'shortdash',
                            width: 4,
                            zIndex: 10,
                            label: {
                                text: 'Over Stocked'
                            }
                        }],
                }],

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: 'yellow',
                color: 'white',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> <br/>',
                footerFormat: 'Total: <b>{point.total:,.1f}</b>',
                shared: true,
                useHTML: true,
                zIndex: 1000
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 15,
                    dataLabels: {
                        enabled: true,
                        verticalAlign: 'bottom',
                        style: {
                            color: 'black'
                        }
                    },
                    enableMouseTracking: true,
                    borderWidth: 0
                },
                marker: {                 
                    
                    radius: 300
                }
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data; ?>
        });
    })
</script>
