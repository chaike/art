<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
	function popitup(url) {
				//submits the form
				//document.getElementById("order_comments").submit();
				//Opening the window that prints the order

				newwindow=window.open(url,'name','height=540,width=950,top=80,left=200');
				if (window.focus) {newwindow.focus()}
				return false;
	}
    $(function () {
        base_url = "<?= base_url(); ?>manager/"
        base_url2 = "<?= base_url(); ?>Manager/"
        counter = 1;
        category = '';
        var chartDIV = '<?php echo $chart_name . "_container"; ?>'
        myStorage = window.localStorage;
        Highcharts.setOptions({
            global: {
                useUTC: false,

            },
            lang: {
                decimalPoint: '.',
                thousandsSep: ','
            }
        });

        Highcharts.chart(chartDIV, {
            chart: {
                type: 'bar',
                events: {

                    drilldown: function (e) {
                        category = e.point.name;
                        counter++;
                        if (counter === 2) {
                           // alert(category + counter);
                            myStorage.setItem('cat', category);
                        }


                        if (!e.seriesOptions) {

                            var chart = this,
                                    drilldowns = <?php echo $chart_drilldown_data; ?>,
                                    series = drilldowns[e.point.name];

                            // Show the loading label
                            chart.showLoading('loading...');

                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);

                        }


                    },

                    drillup: function (e) {
                        counter -= 1;

                    }
                }

            },

            colors: ['#5cb85c', '#434348', '#5bc0de', '#f7a35c', '#8085e9', '#ff4d4d', '#bdb76b'],
            title: {
                text: '<?php echo $chart_title; ?>'
            },
            subtitle: {
                text: '<?php echo $chart_source; ?>'
            },
            credits: false,
            xAxis: {
                type: 'category'
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<?php echo $chart_yaxis_title; ?>'
                }
            },
            tooltip: {
                pointFormat: '{point.key} <b>{point.y:,.0f}</b>',
                shared: true,
                useHTML: true
            },
            plotOptions: {

                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true,
                    dataLabels: {
                        enabled: true
                    }
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    },
                    showInLegend: true
                },
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: [{
                    name: '<?php echo $chart_yaxis_title; ?>',
                    colorByPoint: true,
                    data: <?php echo $chart_series_data; ?>
                }],
            drilldown: {
                series: <?php echo $chart_drilldown_data; ?>,

            },
            exporting: {
                enabled: true,
                menuItemDefinitions: {
                    // Custom definition
                    Excel: {
                        onclick: function () {
                            filter = {
                                data_year: $('#filter_year').val(),
                                data_month: $('#filter_month').val(),
                                cat: '',
                                subcat: ''
                            }

                            if (counter === 1) {
                                post('general', filter, 'patients');

                            } else if (counter === 2) {
                                filter.cat = category;
                                post('regcat', filter, 'patients');

                            } else if (counter === 3) {
                                filter.cat = myStorage.getItem('cat');
                                filter.subcat = category;
                                post('linegroup', filter, 'patients');
                            }

                        },
                        text: '<span onclick="return popitup(\''+base_url2+'excel_export_regimen?year='+$('#filter_year').val()+'&month='+$('#filter_month').val()+'\')">Export to excel'+$('#filter_year').val()+'</span>'
                    }
                },
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadSVG', 'separator', 'Excel']
                    }
                }

            }
        });


    });

    function post(url, data, file) {
        $.post(base_url + url, data, function () {

        }).done(function () {
             window.location.href = "<?= base_url(); ?>public/temp/" + file + '.xlsx';
        });
    }
</script>