<?php
/**
 * Created by PhpStorm.
 * User: Ombego
 * Date: 09-06-2019
 * Time: 10:02 PM
 */
 
ini_set('display_errors', 1);
error_reporting(E_ALL);	
?>
</html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Comodity Manager - Excel Export</title>

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/manager/lib/sbadmin2/vendor/bootstrap/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/manager/lib/sbadmin2/vendor/font-awesome/css/font-awesome.min.css" media="screen">
	<style type="text/css">

</style>
	<script type="text/javascript" src="<?php echo base_url() . 'public/manager/js/jquery_excel.js'; ?>"></script>
	<script type="text/javascript">
		$(document).ready(function() {			
			$("#btnExport").click(function(e) {
			e.preventDefault();
			//getting data from our table
			var data_type = 'data:application/vnd.ms-excel';
			var table_div = document.getElementById('table_wrapper');
			var table_html = table_div.outerHTML.replace(/ /g, '%20');

			var a = document.createElement('a');
			document.body.appendChild(a);
			a.href = data_type + ', ' + table_html;
			a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
			a.click();
		  });
		  
		});
	</script>
</head>
<body>
<br>
						<div class="row">
							<div class="col-md-1 col-sm-1 col-xs-12"></div>
							<div class="col-md-10 col-sm-10 col-xs-12">
								
								<button id="btnExport" class="btn btn-success btn-xs pull-right"><i class="fa fa-file-excel-o"></i> Download Excel File</button><br>
								<div id="table_wrapper">
                                    <span class="h4"><u>Patients by Regimen </u>(<?php echo $month."-".$year?>)</span>
								<table id="table_wrapper" class="table table-bordered table-condensed" border="1" cellpadding="0" cellspacing="0" width="100%">									
									<tbody>
										<?php foreach($units as $u): ?>
											<tr>
												<td><?php echo $u->name; ?></td>
												<td><?php echo number_format($u->y); ?></td>
											</tr>
											<tr>
												<td></td>
												<td>
													<table class="table table-striped table-condensed" border="1" cellpadding="0" cellspacing="0" width="100%">
														<?php foreach($categories as $c): if($c->regimen_service==$u->name):?>
															<tr>
																<td width="30%"><?php echo $c->category; ?></td>
																<td><?php echo $c->y; ?></td>
															</tr>
															<tr>
																<td></td>
																<td>
																	<table  class="table table-condensed" border="0" cellpadding="0" cellspacing="0" width="100%">
																		<?php foreach($sub_categories as $s): if($s->regimen_service==$u->name and $s->regimen_category==$c->category):?>
																			<tr>
																				<td><?php echo $s->regimen; ?></td>
																				<td><?php echo $s->y; ?></td>
																			</tr>
																		<?php endif; endforeach; ?>
																	</table>
																</td>
															</tr>
														<?php endif; endforeach; ?>
													</table>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
								</div>
								<button id="btnExport" class="btn btn-primary btn-xs"><i class="fa fa-file-excel-o"></i> Download Excel File</button>
							</div>
						</div>
								
</body>
</html>