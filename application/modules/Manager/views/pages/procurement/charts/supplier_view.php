<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>'

        Highcharts.setOptions({
            global: {
                useUTC: false,

            },
            lang: {
                decimalPoint: '.',
                thousandsSep: ','
            }
        });

        Highcharts.chart(chartDIV, {
            chart: {
                type: 'line'
            },
            title: {
                text: '<?php echo $chart_title; ?>'
            },
            subtitle: {
                text: '<?php echo $chart_source; ?>'
            },
            credits: false,
            xAxis: {
                categories: <?php echo $chart_categories; ?>
            },
            yAxis: {
                title: {
                    text: '<?php echo $chart_yaxis_title; ?>'
                },
                plotLines: [{
                        value: 3,
                        color: 'green',
                        dashStyle: 'shortdash',
                        width: 2,
                        zIndex: 10,
                        x: -10,
                        label: {
                            text: 'Minimal Delay'
                        }
                    }, {
                        value: 9,
                        color: 'blue',
                        dashStyle: 'shortdash',
                        width: 2,
                        zIndex: 10,
                        label: {
                            text: 'Optimal Delay'
                        }
                    }, {
                        value: 15,
                        color: 'red',
                        dashStyle: 'shortdash',
                        width: 2,
                        zIndex: 30,
                        label: {
                            text: 'Chronic Delay'
                        }
                    }],
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '<span>{series.name}</span>: <b>{point.y}</b><br/>',
                shared: true
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: <?php echo $chart_series_data; ?>,
            exporting: {
                enabled: true
            }
        });

    });
</script>