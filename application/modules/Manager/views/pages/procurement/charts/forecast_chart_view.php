<style>
    .highcharts-axis-labels highcharts-xaxis-labels{
        height: 100px !important;
        width: 100px !important;
    } 
</style>
<!--chart_container-->
<div id="<?php echo $chart_name; ?>_container"></div>
<input type="hidden" data-filters="<?php echo $selectedfilters; ?>" id="<?php echo $chart_name; ?>_filters"/>

<!--highcharts_configuration-->
<script type="text/javascript">
    $(function () {
        var chartDIV = '<?php echo $chart_name . "_container"; ?>';

        Highcharts.setOptions({
            colors: ['#5cb85c', '#f0ad4e', '#5bc0de', '#808080', '#ac5353', '#0080ff', '#ff4000']
        });

        Highcharts.chart(chartDIV, {
            legend: {
                enabled: true,
                reversed: false
            },

            chart: {
                type: 'column',
                height: 800
            },

            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },

            xAxis: {
                categories: <?php echo $chart_categories; ?>,
                crosshair: true
            },

            yAxis: {
                allowDecimals: false,
                min: 0,
                max: 5000000,
                tickInterval: 500,
                title: {
                    text: '<?php echo $chart_yaxis_title; ?>',
                }
            },

            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                //footerFormat: 'Total: <b>{point.total:,.0f}</b>',
                shared: true
            },

            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                    }
                }, series: {
                    pointWidth: 10,
                    enableMouseTracking: true,
                    borderWidth: 0,
                    pointPadding: 0,
                    groupPadding: 1,
                },
            },

            credits: {
                enabled: false
            },

            series: <?php echo $chart_series_data; ?>,
        });
    })
</script>
