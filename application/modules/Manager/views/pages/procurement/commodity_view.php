<link href="<?php echo base_url() . 'public/manager/css/procurement.css'; ?>" rel="stylesheet">

<style>
    .tdata{text-align: right !important;}
    .tdata:hover{background: yellow}  
    .class1{background:#32CD32}
    .class2{background:#CCCC00}
    .class3{background:#DC143C}
    .class4{background:#87CEFA}
    .TRACKER th{text-align: center;}
    .col-<?php echo date('m') - 1 ?>{
        // background:#FFDEAD;
    }
    .tt{
        display:none;  
    } 

    .ouma{}

    .tracker_drug{
       // display: block;
    }


    /*#transaction_tbl tr:gt(5){background:red};*/
</style>

<!-- Modal -->
<div id="stockStatus" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Download Stock Status</h4>
            </div>
            <div class="modal-body">
                <form id="stockGenerator" method="post" action="<?php echo base_url() . 'Manager/Procurement/createSOHMOS'; ?>">
                    <span>Select Drug</span><br> 
                    <p><select id="DrugListGenerator" name="drugs[]" class="form-control" multiple style="width:300px;"></select></p>
                    <p><select id="year" name="data_year" class="form-control"  style="width:300px;"><option value="2019">2019</option> </select></p>
                    <p><select id="month" name="data_month" class="form-control"  style="width:300px;">
                            <option value="Jan">Jan</option> 
                            <option value="Feb">Feb</option> 
                            <option value="Mar">Mar</option> 
                            <option value="Apr">Apr</option> 
                            <option value="May">May</option> 
                            <option value="Jun">Jun</option> 
                            <option value="Jul">Jul</option> 
                            <option value="Aug">Aug</option> 
                            <option value="Sep">Sep</option> 
                            <option value="Oct">Oct</option> 
                            <option value="Nov">Nov</option> 
                            <option value="Dec">Dec</option> 
                        </select></p>
                    <p> <button type="submit" class="btn btn-primary" id="Generate" >Generate</button></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--this is the body-->
<div id="page-wrapper">
    <!--row-->
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb page-header">
                <li><a href="<?php echo base_url('manager/dashboard'); ?>">Dashboard</a></li>
                <li><a href="#">Procurement</a></li>
                <li class="active breadcrumb-item"><i class="white-text" aria-hidden="true"></i> <?php echo ucwords($page_name); ?></li>
                <li><span class="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#helpModal"></span></li>
                <?php echo $this->session->flashdata('tracker_msg'); ?>
            </ol>
        </div>
    </div><!--end row-->
    <!--row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default"><!--panel default-->
                <div class="panel-heading">
                </div>
                <div class="panel-body">
                    <button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#stockStatus" id="DownloadStockFile"><i class="icon icon-download"></i> Download Stock Status File</button>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="procurement-listing">
                        <thead>
                            <tr>
                                <th>Commodity</th>
                                <th>Packsize</th>
                                <th>Category</th>
                                <th>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!--end panel default-->
        </div>
    </div><!--end row-->   
</div><!--end page wrapper--->
<!--modal(s)-->
<div class="modal fade" id="add_procurement_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p><span style="font-size: 16px;" class="label label-info " id="productTitle2">Product - Month</span></p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div><!--/modal-header-->
            <div class="modal-body">
                <ul class="nav nav-tabs pull-right">                   
                    <li class="active"><a data-toggle="tab" href="#drug_procurement">Procurement</a></li>
                    <li><a data-toggle="tab" href="#drug_transactions">Product Tracker</a></li>
                    <li><a data-toggle="tab" href="#decisions">Decisions</a></li>
                </ul>
                <div class="tab-content">
                    <div id="drug_procurement" class="tab-pane fade in active">

                        <h3>Procurement Form</h3>           


                        <form  id="PROC_FORM" class="form-horizontal" role="form">
                            <div class="form-group row">                           
                                <div class="col-sm-9">
                                    <input type="hidden" readonly class="form-control" id="drug_id" name="drug_id" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="receipts_usaid" class="col-sm-3 col-form-label">Stock on Hand (SOH)</label>
                                <div class="col-sm-3">
                                    <input type="text" readonly class="form-control" id="commodity_soh">
                                </div>
                                <label for="receipts_usaid" class="col-sm-3 col-form-label">Months of Stock (MOS)</label>
                                <div class="col-sm-3">
                                    <input type="number" readonly class="form-control" id="commodity_mos">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="receipts_usaid" class="col-sm-3 col-form-label">System Calculated Order Quantity</label>
                                <div class="col-sm-3">
                                    <input type="text" readonly class="form-control" id="expected_qty">
                                </div>

                            </div>
                            <hr>
                            <p><strong>Procurement Quantities</strong></p>
                            <hr>

                            <div class="form-group row" id="commodity_frm">                              
                                <div class="col-sm-12">

                                </div>
                            </div>



                            <div id="procurement_loader"></div>
                            <div class="row procurement_history">
                                <div class="col-sm-12">
                                    <div class="table-responsive">

                                        <div id="procurement_history">
                                            <img src="<?php echo base_url(); ?>public/spinner.gif" alt="Loading Please Wait, Please wait ..."> Loading History Data...
                                        </div>

                                    </div>
                                </div>                              
                            </div>


                        </form>
                        <div class="row col-lg-12">


                        </div>

                    </div>
                    <div id="drug_transactions" class="tab-pane fade">
                        <h3>Transactions</h3>
                        <p>
                        <div class="form-control" id="year-filter">
                            <input type="hidden" name="filter_year" id="filter_year" value="" />
                            Year: 
                            <a href="#" class="filter-year" data-value="2015"> 2015 </a>|
                            <a href="#" class="filter-year" data-value="2016"> 2016 </a>|
                            <a href="#" class="filter-year" data-value="2017"> 2017 </a>|
                            <a href="#" class="filter-year" data-value="2018"> 2018 </a>|
                            <a href="#" class="filter-year" data-value="2019"> 2019 </a>|
                            <a href="#" class="filter-year" data-value="2020"> 2020 </a>|
                            <a href="#" class="filter-year" data-value="2021"> 2021 </a>
                        </div>
                        </p>
                        <p>
                        <div id="transaction_tbl" class="table-responsive"></div>

                    </div>
                    <div id="decisions" class="tab-pane fade">
                        <h3>Decisions</h3>
                        <div id="orders_tbl" class="table-responsive">



                        </div>
                    </div>

                </div>
            </div><!--/modal-body-->
            <div class="modal-footer">
            </div><!--/modal-footer-->
        </div><!--/modal-content-->
    </div><!--/modal-dialog-->
</div><!--/modal-->






<div id="Proposals" class="modal fade" role="dialog" style="">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Proposal for <span id="PERIOD"></span></h4>
            </div>
            <div class="modal-body">
                <form id="fundingAgents">
                    <div class="col-md-12">
                        <div class="col-md-5 " >
                            Proposed Quantity
                        </div>
                        <div class="col-md-7 " >
                            <input type="hidden" class="form-control form-group" id="columnp" name="column" value="proposed"/>
                            <input type="number" class="form-control form-group" id="proposedp" name="proposed" value="" placeholder="Proposed Quantity"/>
                        </div>
                        <div class="col-md-12 ">

                            <input id="fundId" name="fundID" type="hidden"/>
                            <table class="table table-striped">
                                <thead>
                                <th>Agent</th>
                                <th>Quantity</th>
                                <th></th>
                                </thead>
                                <tbody id="fundingTable">
                                    <tr class="fundingTable">

                                    </tr>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>
<!--                                            <input type="button" class="btn btn-sm btn-primary theAdd" value="+" title="Add" />-->
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>


                        </div>
                        <div class="col-md-5 " >
                            Contracted Quantity
                        </div>
                        <div class="col-md-7 " >
                            <input type="hidden" class="form-control form-group" id="columnp" name="column" value="contracted"/>
                            <input type="number" class="form-control form-group" id="proposedp" name="contracted" value="" placeholder="Contracted Quantity"/>
                        </div>
                        <div class="col-md-12 ">
                            <textarea class="form-control form-control" id="commentp" name="commentp" placeholder="Any Comments.."></textarea>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                            <!--                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal" >Close</button>
                                                        <button type="button" class="btn btn-primary pull-right" id="saveProposal"  style="margin-right: 10px;">Set/Update</button>-->

                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<div id="Contracts" class="modal fade" role="dialog" style="">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Contracted in <span id="PERIOD" class="PERIOD"></span></h4>
            </div>
            <div class="modal-body">
                <form id="supplierForm">
                    <div class="col-md-12">
                        <div class="col-md-12 " >
                            <input type="hidden" class="form-control form-group" id="columnc" name="column" value="contracted"/>
                            <input type="number" class="form-control form-group" id="contractedc" name="contracted" value="" placeholder="Contracted Quantity"/>
                        </div>
                        <div class="col-md-12 ">

                            <input id="supplierID" name="supplierID" type="hidden"/>
                            <table class="table table-striped">
                                <thead>
                                <th>Contracted Supplier</th>                        
                                <th></th>
                                </thead>
                                <tbody id="fundingSupplier">


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>
<!--                                            <input type="button" class="btn btn-sm btn-primary theAddSupplier2" value="+" title="Add" />-->
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>


                        </div>
                        <div class="col-md-12 ">
                            <textarea class="form-control form-control" id="commentc" name="commentp" placeholder="Any Comments.."></textarea>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                            <!--                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal" >Close</button>
                                                        <button type="button" class="btn btn-primary pull-right" id="saveContracted"  style="margin-right: 10px;">Set/Update</button>-->

                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<div id="CallDown" class="modal fade" role="dialog"  >
    <div class="modal-dialog"  style="">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Commodity Procurement Manager </h4><br>
                <center><span id="procHead" class="label label-info productTitle" style="font-weight: bold;font-size: 17px;"></span></center>
            </div>
            <div class="modal-body">

                <form id="mainForm" class="form-horizontal">
                    <input type="hidden" id="old_trans_id" name="old_trans_id"/>

                    <div class="col-md-12">
                        <fieldset>
                            <legend>Proposed Quantities.</legend>
                            <div class="row">
                                <div class="col-md-3">
                                    <select class="form-control form-group" id="P_YEAR" name="proposed_year">
                                        <option value="">-Year-</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                    </select>
                                </div>
                                <div class="col-md-3 ">
                                    <select class="form-control form-group" name="proposed_month" title="Call-Down Month" id="P_MONTH" >
                                        <option value="">-Month-</option>
                                        <option value="Jan">Jan</option>
                                        <option value="Feb">Feb</option>
                                        <option value="Mar">Mar</option>
                                        <option value="Apr">Apr</option>
                                        <option value="May">May</option>
                                        <option value="Jun">Jun</option>
                                        <option value="Jul">Jul</option>
                                        <option value="Aug">Aug</option>
                                        <option value="Sep">Sep</option>
                                        <option value="Oct">Oct</option>
                                        <option value="Nov">Nov</option>
                                        <option value="Dec">Dec</option>
                                    </select>
                                </div>
                                <div class="col-md-6 ">
                                    <input type="number" class="form-control form-group" id="P_PROPOSED" name="proposed_qty"  placeholder="Proposed Qty.">
                                </div>
                            </div> 


                            <div class="row">
                                <table class="table table-striped">
                                    <thead>
                                    <th>Funding body</th>
                                    <th>Quantity</th>
                                    <th><button type="button" class="btn btn-sm btn-primary" id="fundingAgentsAdd"><i class="fa fa-plus-circle" title="Add"></i></button></th>
                                    </thead>
                                    <tbody id="funders">

                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Contracted Quantities.</legend>
                            <div class="row">							
                                <div class="col-md-12"><hr></div>
                            </div>
                            <div class="col-md-3">
                                <select class="form-control form-group" id="C_YEAR" name="contacting_year">
                                    <option value="">-Year-</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </select>
                            </div>
                            <div class="col-md-3 ">
                                <select class="form-control form-group" name="contracting_month" title="Call-Down Month" id="C_MONTH" >
                                    <option value="">-Month-</option>
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </div>
                            <div class="col-md-6 ">
                                <input type="number" class="form-control form-group" name="contracting_qty" value="" id="C_AMOUNT" placeholder="Contracted Qty.">
                            </div>

                            <div id="MAIN_BEBA">
                                <table class="table table-striped" id="levelOne">
                                    <thead>
                                    <th>Fund Source</th>
                                    <th>Contractor</th>
                                    <th>Quantity</th>
                                    <th><a href="#" class="btn btn-sm btn-primary" id="levelOneAdd"><i class="fa fa-plus-circle" title="Add"></i></a></th>
                                    </thead>
                                    <tbody >                                    
                                        <tr>
                                            <td>
                                                <select class="form-control form-group FAgent123" name="fund[]">

                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control form-group FSupplier123" name="contractor[]">

                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control form-group" name="contracted_qty[]" value="" placeholder="Quantity">

                                            </td>
                                            <td>
                                                <input type="text" class="form-control form-group HASH_" name="hash_id[]" value="" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table class="table table-striped">
                                                    <thead>
                                                    <th>CallDown</th>
                                                    <th></th>
                                                    <th>Receipt</th>
                                                    <th></th>
                                                    <th><a href="#" class="btn btn-sm btn-success pull-right levelTwoAdd" ><i class="fa fa-plus-circle" title="Add"></i></a></th>
                                                    </thead>
                                                    <tbody class="levelTwo">
                                                        <tr>
                                                            <td>
                                                                <select class="form-control form-group" name="calldown_month[]" title="Call-Down Month" style="width:80px;" >
                                                                    <option value="">-Month-</option>
                                                                    <option value="Jan">Jan</option>
                                                                    <option value="Feb">Feb</option>
                                                                    <option value="Mar">Mar</option>
                                                                    <option value="Apr">Apr</option>
                                                                    <option value="May">May</option>
                                                                    <option value="Jun">Jun</option>                                                                    
                                                                    <option value="Jul">Jul</option>
                                                                    <option value="Aug">Aug</option>
                                                                    <option value="Sep">Sep</option>
                                                                    <option value="Oct">Oct</option>                                                                    
                                                                    <option value="Dec">Dec</option
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="number" class="form-control form-group" name="calldown_qty[]" value="" placeholder="Qty" style="width:100px;">
                                                            </td>
                                                            <td>
                                                                <select class="form-control form-group" name="received_month[]" style="width:80px;">
                                                                    <option value="">-Month-</option>
                                                                    <option value="Jan">Jan</option>
                                                                    <option value="Feb">Feb</option>
                                                                    <option value="Mar">Mar</option>
                                                                    <option value="Apr">Apr</option>
                                                                    <option value="May">May</option>
                                                                    <option value="Jun">Jun</option>                                                                    
                                                                    <option value="Jul">Jul</option>
                                                                    <option value="Aug">Aug</option>
                                                                    <option value="Sep">Sep</option>
                                                                    <option value="Oct">Oct</option>                                                                    
                                                                    <option value="Dec">Dec</option
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="number" class="form-control form-group" name="received_qty[]" value="" placeholder="Qty" style="width:100px;">
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control form-group HASH_" name="hash_id_child[]" value="" >
                                                            </td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-md-12 ">
                                    <textarea class="form-control form-control" id="NewComment" name="commentp" placeholder="Any Comments.."></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary pull-right" id="saveProposalMain" style="margin-right: 10px;">Set/Update</button>

                            </div>
                        </fieldset>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
</div>

<div id="Received" class="modal fade" role="dialog" style="">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Received in <span id="PERIOD" class="PERIOD"></span></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="col-md-12 " >
                        <input type="hidden" class="form-control form-group" id="columnr" name="column" value="received"/>
                        <input type="number" class="form-control form-group" id="received" name="received" value="" placeholder="Received Quantity"/>
                    </div>
                    <div class="col-md-12 ">

                    </div>
                    <div class="col-md-12 ">
                        <textarea class="form-control form-control" id="commentr" name="commentr" placeholder="Any Comments.."></textarea>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal" >Close</button>
                        <button type="button" class="btn btn-primary pull-right" id="saveReceived"  style="margin-right: 10px;">Set/Update</button>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<script type="text/javascript" src="<?= base_url(); ?>public/jquery.pleaseWait.js"></script>
<script>

    id = '';
    now = new Date();
    funding_options = '';
    supplier_options = '';
    currentYear = (new Date).getFullYear();
    currentMonth = GetMonthName((new Date).getMonth());
    newdate = new Date(now.setMonth(now.getMonth() - 1));
    lastMonth = GetMonthName((newdate.getMonth()));
    YEAR = '';
    MONTH = '';
    DRUG_ID = '';
    $(function () {

        tinymce.init({
            selector: 'textarea.reports',
            height: 200,
            theme: 'modern',
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true

        });

        $.getJSON("<?= base_url(); ?>Manager/Procurement/getAllDrugs", function (result) {
            $.each(result, function (i, d) {
                $("#DrugListGenerator").append($("<option value='" + d.id + "'>" + d.name.toUpperCase() + ' (' + d.pack_size + ") </option>"));
            })
            $('#DrugListGenerator').multiselect({
                enableFiltering: true,
                includeSelectAllOption: true,
                nonSelectedText: 'Please select drugs'

            });
        });


        /*$('#Generate').click(function () {
         //$('#add_procurement_modal').pleaseWait();
         data2 = $('#stockGenerator').serialize();
         $.post("<?= base_url(); ?>Manager/Procurement/createSOHMOS/", data2, function (result) {
         
         }).done(function () {
         //$('#CallDown').modal('hide');
         //getTransactionsTable(DRUG_ID, currentYear, "#transaction_tbl");
         //  $('#add_procurement_modal').pleaseWait('stop');
         });
         });*/

        //$('#add_procurement_modal').pleaseWait();

        $(document).on("keyup change", "input.faqty", function () {
            var total = 0;
            $("input.faqty").each(function () {
                var value = parseInt($(this).val(), 10);
                if (!isNaN(value)) {
                    total = total + value
                }
                if (total > parseInt($('#P_PROPOSED').val())) {
                    swal({
                        title: "Excess Funds",
                        text: "Funding bodies proposed quantities cannot exeed proposed quantity (" + $('#P_PROPOSED').val() + ")",
                        icon: "error",
                    });
                    $(this).val('');
                } else {

                }
            })
        });
        $('#P_PROPOSED').keyup(function () {
            $('#C_AMOUNT').val($(this).val());
        });

    });
    function Generator() {}


    Generator.prototype.rand = Math.floor(Math.random() * 9999999999999) + Date.now();
    Generator.prototype.getId = function () {
        return this.rand++;
    };
    var idGen = new Generator();
    $('.HASH_').val(idGen.getId());



    DEF = '<table class="table table-striped" id="levelOne"> <thead> <th>Fund Source</th> <th>Contractor</th> <th>Quantity</th> <th><a href="#" class="btn btn-sm btn-primary" id="levelOneAdd"><i class="fa fa-plus-circle" title="Add"></i></a></th> </thead> <tbody > <tr> <td> <select class="form-control form-group FAgent" name="fund[]">' + funding_options + '</select> </td> <td> <select class="form-control form-group FSupplier" name="contractor[]"> <option value="">-Contractor-</option>' + supplier_options + '</select> </td> <td> <input type="number" class="form-control form-group" name="contracted_qty[]" value="" placeholder="Quantity"> </td> <td> <input type="hidden" class="form-control form-group HASH_" name="hash_id[]" value="" > </td> </tr> <tr> <td colspan="4"> <table class="table table-striped"> <thead> <th>CallDown</th> <th></th> <th>Receipt</th> <th></th> <th><a href="#" class="btn btn-sm btn-success pull-right levelTwoAdd" ><i class="fa fa-plus-circle" title="Add"></i></a></th> </thead> <tbody class="levelTwo"> <tr> <td> <select class="form-control form-group" name="calldown_month[]" title="Call-Down Month" style="width:80px;" > <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option> </select> </td> <td> <input type="number" class="form-control form-group" name="calldown_qty[]" value="" placeholder="Qty" style="width:100px;"> </td> <td> <select class="form-control form-group" name="received_month[]" style="width:80px;"> <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option </select> </td> <td> <input type="number" class="form-control form-group" name="received_qty[]" value="" placeholder="Qty" style="width:100px;"> </td> <td> <input type="hidden" class="form-control form-group HASH_" name="hash_id_child[]" value="" > </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>';
    $(document).on('click', '.levelTwoAdd', function () {
        hash = $(this).closest('tbody').find('.HASH_').val();
        levelTwo = '<tr><td> <select class="form-control form-group" name="calldown_month[]" title="Call-Down Month" > <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option> </select> </td> <td> <input type="number" class="form-control form-group" name="calldown_qty[]" value="" placeholder="Quantity"> </td> <td> <select class="form-control form-group" name="received_month[]"> <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option </select> </td> <td> <input type="number" class="form-control form-group" name="received_qty[]" value="" placeholder="Quantity"> </td> <td><input type="hidden" class="form-control form-group HASH_" name="hash_id_child[]" value="' + hash + '" ><a href="#" class="btn btn-sm btn-warning removeLevelOneSub"><i class="fa fa-minus-circle" title="Remove"></i></a> </td> </tr>';
        $(this).closest('table').append(levelTwo);
    });
    $(document).on('click', '.levelOneRem', function () {
        $(this).closest('table').remove();
        $(this).closest('hr').remove();
    });
    $(document).on('click', '.removeLevelOneSub', function () {
        $(this).closest('tr').remove();
    });
    function GetMonthName(monthNumber) {
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return months[monthNumber];
    }
    if (lastMonth == 'December') {
        currentYear = currentYear - 1;
    }


    $(document).on('click', '#levelOneAdd', function () {
        hash_unique = idGen.getId();
        levelOne = '<table class="table table-striped" id="levelOne"> <thead> <th>Fund Source</th> <th>Contractor</th> <th>Quantity</th> <th><a href="#rem" title="Remove Section" class="btn btn-sm btn-danger levelOneRem"><i class="fa fa-minus"></i></a></th> </thead> <tbody > <tr> <td> <select class="form-control form-group FAgent" name="fund[]">' + funding_options + '</select></td><td><select class="form-control form-group FSupplier" name="contractor[]">' + supplier_options + '</select> </td> <td> <input type="number" class="form-control form-group" name="contracted_qty[]" value="" placeholder="Quantity"> </td> <td><input type="hidden" class="form-control form-group HASH_" name="hash_id[]" value="' + hash_unique + '" ></td> </tr> <tr> <td colspan="4"> <table class="table table-striped"> <thead> <th>CallDown</th> <th></th> <th>Receipt</th> <th></th> <th><a href="#" class="btn btn-sm btn-success levelTwoAdd" ><i class="fa fa-plus-circle" title="Add"></i></a></th> </thead> <tbody class="levelTwo"> <tr> <td> <select class="form-control form-group" name="calldown_month[]" title="Call-Down Month" > <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option> </select> </td> <td> <input type="number" class="form-control form-group" name="calldown_qty[]" value="" placeholder="Quantity"> </td> <td> <select class="form-control form-group" name="received_month[]"> <option value="">-Month-</option> <option value="Jan">Jan</option> <option value="Feb">Feb</option> <option value="Mar">Mar</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Apr">Apr</option> <option value="May">May</option> <option value="Jun">Jun</option> <option value="Jul">Jul</option> <option value="Aug">Aug</option> <option value="Sep">Sep</option> <option value="Oct">Oct</option> <option value="Oct">Oct</option> <option value="Dec">Dec</option </select> </td> <td> <input type="number" class="form-control form-group" name="received_qty[]" value="" placeholder="Quantity"> </td> <td><input type="hidden" class="form-control form-group HASH_" name="hash_id_child[]" value="' + hash_unique + '" > <a href="#" class="btn btn-sm btn-warning removeLevelOneSub"><i class="fa fa-minus-circle" title="Remove"></i></a> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>';
        $('#MAIN_BEBA').append(levelOne);
    });
    $(document).on('click', 'td.proposed', function () {
        $('#add_procurement_modal').pleaseWait();
        var row = $(this).text();
        year = $(this).attr('data-year');
        month = $(this).attr('data-month');
        drug_id = $(this).attr('data-drug_id');
        $('#PERIOD').text(month + '-' + year);
        YEAR = year;
        MONTH = month;
        DRUG_ID = drug_id;
        $.getJSON(fundingURL, function (res) {
            $(".FAgent").empty();
            funding_options = '';
            $.each(res, function (i, d) {
                $(".FAgent").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
                funding_options += "<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>";
            });
        });
        $.getJSON("<?= base_url(); ?>Manager/Procurement/getDrugDetails/proposed/" + year + '/' + month + '/' + drug_id, function (result) {
            $('#proposedp').val(result[0].proposed);
            $('#commentp').val(result[0].comments);
            $('#fundingTable').empty();
            $.getJSON("<?php echo base_url() . 'Manager/Procurement/loadFundings/'; ?>" + year + '/' + month + '/' + drug_id, function (res) {
                if (res.length > 0) {
                    $('#fundingTable').empty();
                }
                $.each(res, function (i, d) {
                    $('#fundingTable ').append('<tr class="fundingTable"><td><select name="funding[]" class="form-control" id="FAgent"><option value="' + d.agent + '">' + d.agent + '<option></select></td><td><input type="number" value="' + d.amount + '" name="quantity[]" placeholder="Enter Quantity" class="form-control faqty"/></td><td><input type="button" class="btn btn-sm btn-primary theRem" value="-" /></td></tr>');
                });
                $('#Proposals').modal();
                $('#add_procurement_modal').pleaseWait('stop');
            });
        });
        // alert(row);
    });
    $('#saveProposalMain').click(function () {
        $('#add_procurement_modal').pleaseWait();
        data2 = $('#mainForm').serialize();
        $.post("<?= base_url(); ?>Manager/saveProcurementData/" + DRUG_ID + '/' + YEAR + '/' + MONTH, data2, function (result) {

        }).done(function () {
            $('#CallDown').modal('hide');
            getTransactionsTable(DRUG_ID, currentYear, "#transaction_tbl");
            $('#add_procurement_modal').pleaseWait('stop');
        });
    });
    $(document).on('click', 'td.contracted', function () {
        $('#add_procurement_modal').pleaseWait();
        var row = $(this).text();
        year = $(this).attr('data-year');
        month = $(this).attr('data-month');
        drug_id = $(this).attr('data-drug_id');
        $('.PERIOD').text(month + '-' + year);
        YEAR = year;
        MONTH = month;
        DRUG_ID = drug_id;
        $.getJSON(fundingURL, function (res) {
            $(".FAgent").empty();
            $.each(res, function (i, d) {
                $(".FAgent").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
            });
        });
        // $('#CallDown').modal();
        $.getJSON("<?= base_url(); ?>Manager/loadMainHash/" + year + '/' + month + '/' + drug_id, function (result) {
            $('#P_YEAR').val(result['proposed'][0].year);
            $('#P_MONTH').val(result['proposed'][0].month);
            $('#P_PROPOSED').val(result['proposed'][0].proposed);
            $('#old_trans_id').val(result['proposed'][0].trans_id);
            $('#NewComment').val(result['proposed'][0].comments);
            $('#funders').empty();
            $.each(result['funding_agents'], function (i, d) {
                $('#funders ').append('<tr class="fundingTable"><td><select name="funding[]" class="form-control" id="FAgent"><option value="' + d.agent + '">' + d.agent + '<option></select></td><td><input type="number" value="' + d.amount + '" name="quantity[]" placeholder="Enter Quantity" class="form-control faqty"/></td><td><input type="button" class="btn btn-sm btn-primary theRem REMOVEFUND" value="-" /></td></tr>');
            });
            if (result['contracted'].length < 1) {
            } else {
                $('#C_YEAR').val(result['contracted'][0].year);
                $('#C_MONTH').val(result['contracted'][0].month);
                $('#C_AMOUNT').val(result['contracted'][0].amount);
            }

            if (result['calldown_body'].length < 1) {
                $('#MAIN_BEBA').empty();
                $('#MAIN_BEBA').append(DEF);
                $('.HASH_').val(idGen.getId());
                $.getJSON(fundingURL, function (res) {
                    $(".FAgent").empty();
                    $.each(res, function (i, d) {
                        $(".FAgent").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
                        //funding_options += "<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>";
                    });
                });

                $.getJSON(supplierURL, function (res) {
                    $(".FSupplier").empty();
                    $.each(res, function (i, d) {
                        $(".FSupplier").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
                        //supplier_options += "<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>";
                    });
                });
            } else {
                $('#MAIN_BEBA').empty();
                $('#MAIN_BEBA').append(result['calldown_body']);
            }

            $('#CallDown').modal();
            $("#fundingAgentsAdd").trigger("click");
            $('#add_procurement_modal').pleaseWait('stop');
        }, 'json');
    });
    $('#saveContracted').click(function () {
        $('#add_procurement_modal').pleaseWait();
        data = {
            column: $('#columnc').val(),
            proposed: $('#contractedc').val(),
            comment: $('#commentc').val(),
        };
        data2 = $('#supplierForm').serialize();
        $.post("<?= base_url(); ?>Manager/Procurement/updateDetails/" + YEAR + '/' + MONTH + '/' + DRUG_ID, data2, function (result) {

        }).done(function () {
            $('#Contracts').modal('hide');
            getTransactionsTable(DRUG_ID, currentYear, "#transaction_tbl");
            $('#add_procurement_modal').pleaseWait('stop');
        });
        // alert(row);
    });
//    $(document).on('click', 'td.calldown', function () {
//        $('#add_procurement_modal').pleaseWait();
//        var row = $(this).text();
//        year = $(this).attr('data-year');
//        month = $(this).attr('data-month');
//        drug_id = $(this).attr('data-drug_id');
//        $('.PERIOD').text(month + '-' + year);
//        YEAR = year;
//        MONTH = month;
//        DRUG_ID = drug_id;
//        $.getJSON("<?= base_url(); ?>Manager/Procurement/getDrugDetails/calldown/" + year + '/' + month + '/' + drug_id, function (result) {
//            $('#calldownd').val(result[0].calldown);
//            $('#commentd').val(result[0].comments);
//            $('#CallDown').modal();
//            $('#add_procurement_modal').pleaseWait('stop');
//        });
//        // alert(row);
//    });
    $('#saveCallDown').click(function () {
        $('#add_procurement_modal').pleaseWait();
        data = {
            column: $('#columnd').val(),
            proposed: $('#calldownd').val(),
            comment: $('#commentd').val(),
        };
        $.post("<?= base_url(); ?>Manager/Procurement/updateDetails/" + YEAR + '/' + MONTH + '/' + DRUG_ID, data, function (result) {

        }).done(function () {
            $('#CallDown').modal('hide');
            getTransactionsTable(DRUG_ID, currentYear, "#transaction_tbl");
            $('#add_procurement_modal').pleaseWait('stop');
        });
        // alert(row);
    });
    $(document).on('click', 'td.received', function () {
        $('#add_procurement_modal').pleaseWait();
        var row = $(this).text();
        year = $(this).attr('data-year');
        month = $(this).attr('data-month');
        drug_id = $(this).attr('data-drug_id');
        $('.PERIOD').text(month + '-' + year);
        YEAR = year;
        MONTH = month;
        DRUG_ID = drug_id;
        $.getJSON("<?= base_url(); ?>Manager/Procurement/getDrugDetails/received/" + year + '/' + month + '/' + drug_id, function (result) {
            $('#received').val(result[0].received);
            $('#commentr').val(result[0].comments);
            $('#Received').modal();
            $('#add_procurement_modal').pleaseWait('stop');
        });
        // alert(row);
    });
    $('#saveReceived').click(function () {
        $('#add_procurement_modal').pleaseWait();
        data = {
            column: $('#columnr').val(),
            proposed: $('#received').val(),
            comment: $('#commentr').val(),
        };
        $.post("<?= base_url(); ?>Manager/Procurement/updateDetails/" + YEAR + '/' + MONTH + '/' + DRUG_ID, data, function (result) {

        }).done(function () {
            $('#Received').modal('hide');
            getTransactionsTable(DRUG_ID, currentYear, "#transaction_tbl");
            $('#add_procurement_modal').pleaseWait('stop');
        });
        // alert(row);
    });
    var totalPercentage = 0;
    formTemplate = $('#ReceiveForm').clone()[0];
    var swalConfig = {
        title: 'Received Form',
        content: formTemplate,
        button: {
            text: 'Submit',
            closeModal: false
        }
    };
    var statusURL = '<?= base_url(); ?>API/procurement_status';
    var fundingURL = '<?= base_url(); ?>API/funding_agent';
    var supplierURL = '<?= base_url(); ?>API/supplier';
    $(document).ready(function () {

        $(document).on('click', '.tabledit-edit-button', function () {
            $('[name="drug_id"]').prop('readonly', 'readonly');
        });
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            //  startDate: '1d'
        });
        $('#SaveThe').click(function () {
            data = $('#Prourement').serialize();
            $.post("<?= base_url(); ?>Manager/Procurement/saveProcurementData", data, function () {

            });
        });
        $(document).on("click", '.dropdown-submenu a.test', function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
        $('#saveDecision').click(function () {
            $(this).text('Please Wait...');
            $(this).prop('disabled', true);
            $.post("<?php echo base_url() . 'Manager/Procurement/save_decision'; ?>", $('#decisionForm').serialize(), function () {
                window.location.href = "<?php echo base_url() . 'manager/procurement/commodity'; ?>";
            });
        });
        $('#editDecision').click(function () {
            $(this).text('Please Wait...');
            $(this).prop('disabled', true);
            $.post("<?php echo base_url() . 'Manager/Procurement/edit_decision'; ?>", $('#editdecisionForm').serialize(), function () {
                window.location.href = "<?php echo base_url() . 'manager/procurement/commodity'; ?>";
            });
        });
        $('#deleteDecisionBtn').click(function () {
            $(this).text('Please Wait...');
            $(this).prop('disabled', true);
            $.post("<?php echo base_url() . 'Manager/Procurement/delete_decision'; ?>", {decision_id: $('#discID').val()}, function () {
                window.location.href = "<?php echo base_url() . 'manager/procurement/commodity'; ?>";
            });
        });
        $(document).on('click', '.trash', function () {
            id = $(this).attr('data-id');
            $('#discID').val(id);
        });
        /*  $(document).on('mouseenter', '.mainContent', function () {
         clearTimeout($(this).data('timeoutId'));
         $(this).find(".edit").show();
         $(this).find(".trash").show();
         }).mouseleave(function () {
         var someElement = $(this),
         timeoutId = setTimeout(function () {
         someElement.find(".edit").hide();
         $(this).find(".trash").hide();
         }, 650);
         //set the timeoutId, allowing us to clear this trigger if the mouse comes back over
         someElement.data('timeoutId', timeoutId);
         });*/

        $(document).on('click', '.edit', function () {
            $('#Disc_ID ').val('');
            $('#Drug_IDEdit').val('');
            $('#decisionsEdit').val('Loading, Please Wait....');
            $('#reccomendationsEdit').val('Loading, Please Wait....')
            drug_id = '';
            disc_id = $(this).attr('data-id');
            $.getJSON("<?php echo base_url() . 'Manager/Procurement/get_decisions_byID/'; ?>" + disc_id, function (res) {
                $('#Disc_ID ').val(disc_id);
                $('#Drug_IDEdit').val(res.data[0].drug_id);
                $('#decisionsEdit').val(res.data[0].discussion);
                $('#reccomendationsEdit').val(res.data[0].recommendation);
            }, 'json');
            //console.log('its me' +disc_id)

            //console.log(disc)
        });
        $.getJSON("<?php echo base_url() . 'Manager/Procurement/getAllDrugs/'; ?>", function (res) {
            drug_list = $('#DrugList');
            drug_list.append('<option value="">--Select Drug--</option>');
            $.each(res, function (i, d) {
                drug_list.append('<option value="' + d.id + '">' + d.name + '</option>');
            });
        }, 'json');
        $('#DrugList').on('change', function () {
            var id = $(this).val();
            LoadSpinner("#procurement_loader");
            var trackerURL = "<?php echo base_url() . 'Manager/Procurement/get_tracker/'; ?>" + id;
            $.getJSON(trackerURL, function (json) {
                $.each(json.data, function (key, value) {
                    $("#" + key).val(value)
                });
                value = $('#commodity_mos').val();
                if (value < 0) {
                    $('#commodity_mos').val(0);
                }


                if (json.data.length < 1) {
                    $('#productTitle1,#productTitle2,#productTitle3,#productTitle4').text("No data found for this product.");
                    $('#decisionModalTitle').text('Add Decisions and Reccomendations for this product');
                } else {
                    $('#productTitle1,#productTitle2,#productTitle3,#productTitle4').text(json.data.commodity_name + " | " + lastMonth + "-" + currentYear + " Tracker");
                    $('.decisionModalTitle').text('Edit Decisions and Reccomendations for ' + json.data.commodity_name + " | " + lastMonth + "-" + currentYear + " Tracker");
                }
                $("#procurement_loader").empty(); //Kill Loader
                $('#actual_qty').val('');
            });
            $('.productTitle').empty();
            getDecisions(id, "#decision_tbl_procurement");
            getTransactionsTable(id, currentYear, "#transaction_tbl");
            //getDrugOrders(id, "#orders_tbl")
            getDrugOrdersHistory(id, '#procurement_history', currentYear);
            // getDrugOrdersHistory_(id, '#procurement_history_');
            //getDrugLogs(id, "#logs_tbl");
        })






        $('#procurement-listing').DataTable({
            responsive: true,
            order: [[0, "asc"]],
            pagingType: "full_numbers",
            ajax: "<?php echo base_url() . 'Manager/Procurement/get_commodities'; ?>",
            initComplete: function () {
                this.api().columns([2]).every(function () {
                    var column = this;
                    var select = $('<br/><select><option value="">Show all</option></select>')
                            .appendTo($(column.header()))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var val = $('<div/>').html(d).text();
                        select.append('<option value="' + val + '">' + val + '</option>');
                    });
                });
            }
        });
        //Show tracker sidemenu
        $(".commodity").closest('ul').addClass("in");
        $(".commodity").addClass("active active-page");
        //Load Commodity Data when Modal shown
        $("#add_procurement_modal").on("shown.bs.modal", function (e) {
            //Load Spinner
            LoadSpinner("#procurement_loader")

            drugID = $(e.relatedTarget).data('drug_id');
            $('#Drug_ID,#drug_id_').val(drugID);
            $('.productTitle').empty();
            //Load TrackerInfo
            var trackerURL = "<?php echo base_url() . 'Manager/Procurement/get_tracker/'; ?>" + drugID;
            $.getJSON(trackerURL, function (json) {
                $.each(json.data, function (key, value) {
                    $("#" + key).val(value)
                });
                value = $('#commodity_mos').val();
                if (value < 0) {
                    $('#commodity_mos').val(0);
                }

                if (json.data.length < 1) {
                    $('#productTitle1,#productTitle2,#productTitle3,#productTitle4').text("No data found for this product");
                    $('#decisionModalTitle').text('Add Decisions and Reccomendations for this product');
                } else {
                    $('#productTitle1,#productTitle2,#productTitle3,#productTitle4').text(json.data.commodity_name + " | " + lastMonth + "-" + currentYear + " Tracker");
                    $('.decisionModalTitle').text('Edit Decisions and Reccomendations for ' + json.data.commodity_name + " | " + lastMonth + "-" + currentYear + " Tracker");
                    $('#procHead').text(json.data.commodity_name);
                }
                $("#procurement_loader").empty(); //Kill Loader
                $('#actual_qty').val('');
            });
            //Load Drug Data
            getDecisionsTimeline(drugID, "#decision_tbl");
            getDecisions(drugID, "#decision_tbl_procurement");
            getTransactionsTable(drugID, currentYear, "#transaction_tbl");
            //getDrugOrders(drugID, "#orders_tbl")
            getDrugOrdersHistory(drugID, '#procurement_history', +currentYear);
            //getDrugOrdersHistory_(drugID, '#procurement_history_');
            //getDrugLogs(drugID, "#logs_tbl");

        });
        //Clean up fields when modal is closed
        $('#add_procurement_modal').on('hidden.bs.modal', function (e) {
            // $('select,input').val('');
        });
        //Get dropdown data
        getDropdown(statusURL, 'procurement_status')
        getDropdown(fundingURL, 'funding_agent')
        getDropdown(supplierURL, 'supplier')

        //Add datepicker
        $(".transaction_date").datepicker({
            format: 'yyyy-mm-dd',
            // startDate: '1d'
        });
        /* $("#actual_qty").on('keyup', function () {
         var curr_element = parseInt($(this).val());
         var overall_qty = $("#expected_qty").val();
         var new_ = overall_qty.replace(",", "");
         
         if (curr_element > parseInt(new_)) {
         swal({
         title: "Excess Quantity!",
         text: "Quantity cannot be more than System Calculated Order Quantity!",
         icon: "error",
         });
         $(this).val('')
         } else {
         }
         
         });*/

        $(document).on('click', '.receipt_qty,.receipt_qty_percentage', function () {
            var overall_qty = $("#actual_qty").val()
            if (overall_qty === '') {
                swal({
                    title: "No Proposed Quantity!",
                    text: "Please enter value for Proposed Actual Quantity",
                    icon: "error",
                });
            }
        })


        //Validate receipt_qty
        $(".receipt_qty").on('keyup', function () {
            var curr_element = $(this)
            var sum = 0;
            var overall_qty = parseInt($("#actual_qty").val());
            $('.receipt_qty').each(function () {
                sum += parseInt(this.value);
                if (sum > overall_qty) {
                    swal({
                        title: "Excess Quantity!",
                        text: "Quantity cannot be more than Proposed Order Quantity!",
                        icon: "error",
                    });
                } else {
                    var bal = overall_qty - sum;
                }
            });
        });
        $(".receipt_qty").on('focusout', function () {
            var curr_element = $(this)
            var sum = 0;
            var overall_qty = parseInt($("#actual_qty").val());
            $('.receipt_qty').each(function () {
                sum += parseInt(this.value);
                if (sum > overall_qty) {
                    swal({
                        title: "Excess Quantity!",
                        text: "Quantity cannot be more than Proposed Order Quantity!",
                        icon: "error",
                    });
                } else {
                    var bal = overall_qty - sum;
                    $('#REMID').text('You have  ' + bal + '(qty.) left to assign.').show();
                }
            });
        });
        $(".receipt_qty_percentage").on('focusout', function () {
            var curr_element = $(this);
            var sum = 0;
            var overall_qty = 100;
            $('.receipt_qty_percentage').each(function () {
                sum += parseInt(this.value);
                if (sum > overall_qty) {
                    swal({
                        title: "Excess Quantity!",
                        text: "Precentage cannot exceed 100%",
                        icon: "error",
                    });
                } else {
                    bal = overall_qty - sum;
                    if (isNaN(bal)) {
                        bal = 0;
                    }
                    ;
                    $('#REMID').text('You have  ' + bal + '% left to assign.').show();
                }
            });
        });
        //Add row to table
        $(".add").click(function () {
            var sum = 0;
            $(".receipt_qty_percentage").each(function () {
                sum += +$(this).val();
            });
            if (sum > 100) {
                swal({
                    title: "Quantity Error!",
                    text: "You have exeeded the value of proposed quantity !",
                    icon: "error",
                });
                return false;
            } else {


                var last_row = $(this).closest('tr');
                if (last_row.find(".receipt_qty").val() == "" || last_row.find(".transaction_date").val() == "" || last_row.find(".procurement_status").val() == "") {
                    swal({
                        title: "Required!",
                        text: "All values must be entered/selected!",
                        icon: "error",
                    });
                } else {
                    $(".transaction_date").datepicker('destroy');
                    var cloned_row = last_row.clone(true);
                    cloned_row.find('select,input').val('');
                    cloned_row.insertAfter(last_row);
                    $(".transaction_date").datepicker({
                        format: 'yyyy-mm-dd',
                        //  startDate: '1d'
                    });
                    cloned_row.find('.contracted').hide();
                }
            }
        });
        $(document).on('keyup', '.receipt_qty_percentage', function () {
            var row_data = $(this).closest('tr');
            var actual_qty = $('#actual_qty').val();
            var qty_ = row_data.find('.receipt_qty');
            var percentage = row_data.find('.receipt_qty_percentage').val();
            var calculated_qty = (parseInt(percentage) / 100) * parseInt(actual_qty);
            qty_.val(Math.ceil(calculated_qty));
        });
        $(document).on('keyup', '.receipt_qty', function () {
            var row_data = $(this).closest('tr');
            var actual_qty = $('#actual_qty').val();
            var qty_ = row_data.find('.receipt_qty').val();
            var percentage = row_data.find('.receipt_qty_percentage');
            var calculated_perc = ((parseInt(qty_) / parseInt(actual_qty)) * 100);
            percentage.val(Math.ceil(calculated_perc));
        });
        //Remove row from table
        $(".remove").click(function () {
            var rows = $("#procurement_tbl > tbody").find("tr").length;
            if (rows > 1) {
                swal({
                    title: "Remove Alert",
                    text: "Are you sure, You want to delete this row?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('tr').remove();
                    }
                });
            } else {
                swal({
                    title: "Invalid!",
                    text: "You cannot remove the last row!",
                    icon: "error",
                });
            }
        });
        //Filter transaction year
        $(".filter-year").click(function () {
            var periodYear = $(this).data('value');
            var drugID = $("#drug_id").val();
            getTransactionsTable(drugID, periodYear, "#transaction_tbl")
        });
        //Hide contracted fields
        $(".contracted").hide()

        //Transaction table download
        $('#trans_download').on('click', function () {
            $('#transaction_tbl').jexcel('download');
        });
        //Transaction table save
        $('#trans_save').on('click', function () {
            $('#trans_save').prop('disabled', true);
            $('#trans_save').prop('value', 'Please Wait....');
            tbl = html2json();
            newdata = JSON.parse(tbl);
            $.post("<?php echo base_url() . 'Manager/Procurement/get_transaction_data/'; ?>", {data: newdata, drug_id: drugID}, function () {
                $('#transaction_tbl').empty();
            }).done(function () {
                getTransactionsTable(drugID, '2018', "#transaction_tbl");
                $('#trans_save').prop('disabled', false);
                $('#trans_save').prop('value', 'Update Changes');
            });
        });
    }
    );
    $("#actual_qty").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
            //display error message

            return false;
        }
    });
    $('#SAVE_ORDER').click(function () {
        //alert(1)



        order = $('#actual_qty').val();
        qty = $('#qty').val();
        date = $('#dateT').val();
        transaction = $('#status').val();
        //alert(transaction);

        if (order == '' || qty == '' || date == '' || transaction == '') {
            swal({
                title: "Input Error!",
                text: "Please enter all fields to proceed!",
                icon: "error",
            });
            return false;
        } else {
            var $this = $(this);
            $this.button('loading');
            var drug_id = $('#drug_id').val();
            $.post("<?php echo base_url() . 'manager/save_procurement'; ?>", $('#PROC_FORM').serialize(), function (resp) {
                if (resp.status == 'success') {
                    var drug_id = $('#drug_id').val();
                    getTransactionsTable(drug_id, currentYear, "#transaction_tbl");
                    getDrugOrdersHistory(drug_id, '#procurement_history', currentYear);
                    // getDrugOrdersHistory_(drug_id, '#procurement_history_');
                } else {
                    swal({
                        title: "Save Error",
                        text: "We could not save at the moment, please try again later",
                        icon: "error",
                    });
                }
            }, 'json').done(function () {
                $this.button('reset');
            });
        }
        // $('#SAVE_ORDER').prop('disabled', false);
        return false;
    });
    function html2json() {
        var json = '{';
        var otArr = [];
        var tbl2 = $('#transaction_tbl tr').each(function (i) {
            x = $(this).children();
            var itArr = [];
            x.each(function () {
                itArr.push('"' + $(this).text() + '"');
            });
            otArr.push('"' + i + '": [' + itArr.join(',') + ']');
        })
        json += otArr.join(",") + '}'

        return json;
    }

    function getDropdown(dataURL, elementClass) {
        $.getJSON(dataURL, function (data) {
            $("." + elementClass + " option").remove();
            $("." + elementClass).append($("<option value=''>Select One</option>"));
            $.each(data, function (i, v) {
                $("." + elementClass).append($("<option value='" + v.id + "'>" + v.name.toUpperCase() + "</option>"));
            });
        });
    }

    function getDecisionsTimeline(drugID, divID) {
        //Load Spinner
        LoadSpinner(divID)
        //Load Table
        var decisionsURL = "<?php echo base_url() . 'Manager/Procurement/get_timeline/'; ?>" + drugID
        $.get(decisionsURL, function (timeline) {
            //tinymce.get('reports').setContent(timeline);
            // alert(timeline)
            //$('#orders_tbl').html(timeline)
        });
    }

    function getDecisions(drugID, divID) {
        //Load Spinner
        LoadSpinner('#orders_tbl')
        //Load Table
        var decisionsURL = "<?php echo base_url() . 'Manager/Procurement/get_decisions/'; ?>" + drugID
        $.get(decisionsURL, function (timeline) {
            $('#orders_tbl').html(timeline)
        });
    }

    $(document).on('mouseover', "#transaction_tbl td", function () {

        var column_num = parseInt($(this).index()) + 1;
        var row_num = parseInt($(this).parent().index()) + 1;
        $("#transaction_tbl td:lt(6):gt(3)", this).css('background', 'black');
        // alert("Row_num =" + row_num + "  ,  Rolumn_num =" + column_num);
    });
    $(document).on('click', '.tabledit-save-button', function () {

        getDrugOrdersHistory(drugID, '#procurement_history', currentYear);
        // getDrugOrdersHistory_(drugID, '#procurement_history_');
        getTransactionsTable(drugID, currentYear, "#transaction_tbl");
    });
    function getTransactionsTable(drugID, periodYear, tableID) {
        month = "<?php echo date('m') - 1; ?>";
        next = parseInt(month) + 1;
        //Load Spinner
        LoadSpinner(tableID)
        //Load tableData
        var transactionURL = "<?php echo base_url() . 'Manager/Procurement/get_transaction_table2/'; ?>" + drugID + '/' + periodYear
        $.get(transactionURL, function (tableData) {
            $('#transaction_tbl').empty();
            $('#transaction_tbl').append(tableData);
            //$("#transaction_tbl tr:eq(8)").find('td:gt(' + month + ')').css('background', '#c6ff00');
            $("#transaction_tbl tr:eq(8)").find('td:lt(' + month + '):gt(0)').css('background', '#fff176');
            $("#transaction_tbl ").find('tr').each(function () {
                var td = $(this).children('td').eq(month);
                var nexttd = $(this).children('td').eq(next);
                td.css("background", 'yellow');
                td.css("color", 'black');
                td.css("font-weight", 'bold');
                // nexttd.text("-");
                // nexttd.css("background", 'white');
            });
            //$('#transaction_tbl').find('td').eq(3).css( "background", "red" );
            // $( "#transaction_tbl td:eq(1 )" ).css( "color", "red" );

        });
    }

    function getDrugOrders(drugID, tableID) {
        //Load Spinner
        LoadSpinner(tableID)
        //Load Table
        var ordersURL = "<?php echo base_url() . 'Manager/Procurement/get_order_table/'; ?>" + drugID
        var editOrderURL = "<?php echo base_url() . 'Manager/Procurement/edit_order/'; ?>"
        var itemURL = "<?php echo base_url() . 'Manager/Procurement/get_order_items/'; ?>"
        $.get(ordersURL, function (table) {
            $.getJSON(itemURL, function (jsondata) {
                $(tableID).html(table)
                $(".order_tbl").Tabledit({
                    url: editOrderURL,
                    editButton: false,
                    deleteButton: false,
                    hideIdentifier: true,
                    columns: {
                        identifier: [0, 'id'],
                        editable: [
                            [1, 'year', '{"2018": "2018", "2019": "2019", "2020": "2020", "2021": "2021"}'],
                            [2, 'month', '{"Jan": "Jan", "Feb": "Feb", "Mar": "Mar", "Apr": "Apr", "May": "May", "Jun": "Jun", "Jul": "Jul", "Aug": "Aug", "Sep": "Sep", "Oct": "Oct", "Nov": "Nov", "Dec": "Dec"}'],
                            // [3, 'date_added', JSON.stringify(jsondata.date_added)],
                            [3, 'quantity'],
                            [4, 'procurement_status_id', JSON.stringify(jsondata.status)],
                            [5, 'funding_agent_id', JSON.stringify(jsondata.funding)],
                            [6, 'supplier_id', JSON.stringify(jsondata.supplier)]]
                    },
                    buttons: {
                        edit: {
                            class: 'btn btn-sm btn-default',
                            html: '<span class="fa fa-pencil-square-o"></span>',
                            action: 'edit'
                        },
                        delete: {
                            class: 'btn btn-sm btn-default',
                            html: '<span class="fa fa-trash-o"></span>',
                            action: 'delete'
                        }
                    },
                    onSuccess: function (data, textStatus, jqXHR) {
                        getDrugOrders(drugID, tableID)


                    }
                });
            });
        });
        // $('.tabledit-delete-button').hide();

    }




    function getDrugOrdersHistory(drugID, divID, year) {

        var ordersURL = "<?php echo base_url() . 'Manager/Procurement/get_order_table_history/'; ?>" + drugID + '/' + year;
        var editOrderURL = "<?php echo base_url() . 'Manager/Procurement/edit_order/'; ?>"
        var itemURL = "<?php echo base_url() . 'Manager/Procurement/get_order_items/'; ?>"
        $.getJSON(itemURL, function (jsondata) {
            $.get(ordersURL, function (table) {
                $(divID).empty();
                $(divID).append(table);
                $(".order_tbl_history").Tabledit({
                    url: editOrderURL,
                    hideIdentifier: true,
                    deleteButton: false,
                    columns: {
                        identifier: [0, 'id'],
                        /* editable: [
                         [1, 'drug_id'],
                         [2, 'year'],
                         [3, 'month' ],
                         [4, 'proposed'],
                         [5, 'contracted'],
                         [6, 'calldown'],
                         [7, 'received'],
                         [8, 'comments']
                         ]*/

                    },
                    buttons: {
                        edit: {
                            class: 'btn btn-sm btn-default',
                            html: '<span class="fa fa-pencil-square-o"></span>',
                            action: 'edit'
                        },
                        delete: {
                            class: 'btn btn-sm btn-default',
                            html: '<span class="fa fa-trash-o"></span>',
                            action: 'delete'
                        }
                    },
                    onSuccess: function (data, textStatus, jqXHR) {
                        getDrugOrdersHistory(drugID, divID, year)
                        // getDrugOrdersHistory_(drugID, divID)


                    }
                });
            });
            // $('#FAgent').empty();

        });
    }



    function getDrugOrdersHistory_(drugID, divID) {

        var ordersURL = "<?php echo base_url() . 'Manager/Procurement/get_order_table_history_/'; ?>" + drugID;
        var editOrderURL = "<?php echo base_url() . 'Manager/Procurement/edit_order/'; ?>"
        var itemURL = "<?php echo base_url() . 'Manager/Procurement/get_order_items/'; ?>"
        $.getJSON(itemURL, function (jsondata) {
            $.get(ordersURL, function (table) {
                $('#procurement_history_').empty();
                $('#procurement_history_').append(table);
            });
        });
    }

    $.getJSON(fundingURL, function (res) {
        $(".FAgent").empty();
        funding_options = '';
        $.each(res, function (i, d) {
            $(".FAgent").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
            funding_options += "<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>";
        });
    });
    $.getJSON(supplierURL, function (res) {
        $(".FSupplier").empty();
        supplier_options = '';
        $.each(res, function (i, d) {
            $(".FSupplier").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
            supplier_options += "<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>";
        });
    });
    $(document).on('dblclick', '#TransactionData td', function () {
        value = $(this).closest('td').index(); //.find('.tabledit-span').text();       
        $('#fundingTable').empty();
        $('#fundingSupplier').empty();
        row = $(this).closest('tr');
        if (value == '7') {
            $('#fundId').val(row.find('td').eq('0').text());
            $.getJSON("<?php echo base_url() . 'Manager/Procurement/loadFundings/'; ?>" + $('#fundId').val(), function (res) {
                if (res.length > 0) {
                    $('#fundingTable').empty();
                }
                $.each(res, function (i, d) {
                    $('#fundingTable ').append('<tr class="fundingTable"><td><select name="funding[]" class="form-control" id="FAgent"><option value="' + d.agent + '">' + d.agent + '<option></select></td><td><input type="number" value="' + d.amount + '" name="quantity[]" placeholder="Enter Quantity" class="form-control faqty"/></td><td><input type="button" class="btn btn-sm btn-primary theRem" value="-" /></td></tr>');
                });
                $.getJSON("<?php echo base_url() . 'Manager/Procurement/loadFunders/'; ?>" + $('#fundId').val(), function (res) {
                    $('#FAGENTS').text(res[0].details);
                });
                $.getJSON(fundingURL, function (res) {
                    $(".FAgent").empty();
                    $.each(res, function (i, d) {
                        $(".FAgent").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
                    });
                });
            }, 'json');
            $('#myFundingAgentModal').modal('show');
        } else if (value == '8') {
            $('#supplierID').val(row.find('td').eq('0').text());
            $.getJSON("<?php echo base_url() . 'Manager/Procurement/loadSupplier/'; ?>" + $('#supplierID').val(), function (res) {
                if (res.length > 0) {
                    $('#fundingSupplier').empty();
                }
                $.each(res, function (i, d) {
                    $('#fundingSupplier ').append('<tr class="fundingTable"><td><select name="supplier[]" class="form-control" id="FAgent"><option value="' + d.supplier + '">' + d.supplier + '<option></select></td></td><td><input type="button" class="btn btn-sm btn-primary theRem" value="-" /></td></tr>');
                });
                $.getJSON("<?php echo base_url() . 'Manager/Procurement/loadSuppliers/'; ?>" + $('#fundId').val(), function (res) {
                    $('#SUPPLIERS').text(res[0].details);
                });
                $.getJSON(supplierURL, function (res) {
                    $("#FSupplier").empty();
                    $.each(res, function (i, d) {
                        $("#FSupplier").append($("<option value='" + d.name + "'>" + d.name.toUpperCase() + "</option>"));
                    });
                });
            }, 'json');
            $('#mySupplierModal').modal('show');
        } else {
            return false;
        }

    });
    $(document).on('click', '.tabledit-save-button tr', function () {
        getDrugOrdersHistory_(drug_id, '#procurement_history_');
    });
    $('#fundingAgentsAdd').click(function () {
        row = '<tr class="fundingTable"><td><select name="funding[]" class="form-control" class="FAgent"><option value=""></option>' + funding_options + '</select></td><td><input type="number" name="quantity[]" placeholder="Enter Quantity" class="form-control faqty"/></td><td><input type="button" class="btn btn-sm btn-primary theRem" value="-" /></td></tr>';
        $("#funders").append(row);
    });
    $(document).on('click', '.theRem', function () {
        if ($('#funders tr').length > 1) {
            $(this).closest('tr').remove();
        }

    });
    $('.theAddSupplier2').click(function () {

        row = '<tr class="fundingSupplier"><td><select name="supplier[]" class="form-control" class="FSupplier"><option value=""></option>' + supplier_options + '</select></td><td><input type="button" class="btn btn-sm btn-primary theRemSupplier" value="-" /></td></tr>';
        $("#fundingSupplier").append(row);
    });
    $(document).on('click', '.theRemSupplier', function () {
        if ($('#fundingSupplier tr').length > 1) {
            $(this).closest('tr').remove();
        }

    });
    $('#SubData').click(function () {
        data = $('#fundingAgents').serialize();
        $.post('<?= base_url(); ?>Manager/Procurement/saveFundings', data, function () {

        }).done(function () {
            $('#myFundingAgentModal').modal('toggle');
            var drug_id = $('#drug_id').val();
            getTransactionsTable(drug_id, currentYear, "#transaction_tbl");
            getDrugOrdersHistory(drug_id, '#procurement_history', currentYear);
            // getDrugOrdersHistory_(drug_id, '#procurement_history_');
        });
    });
    $('#SubDataSupplier').click(function () {
        //data in the house
        data = $('#supplierForm').serialize();
        $.post('<?= base_url(); ?>Manager/Procurement/saveSupplier', data, function () {

        }).done(function () {
            $('#mySupplierModal').modal('toggle');
            var drug_id = $('#drug_id').val();
            getTransactionsTable(drug_id, currentYear, "#transaction_tbl");
            getDrugOrdersHistory(drug_id, '#procurement_history', currentYear);
            // getDrugOrdersHistory_(drug_id, '#procurement_history_');
        });
    })

    function getDrugLogs(drugID, tableID) {
        //Load Spinner
        LoadSpinner(tableID)
        //Load Table
        var logsURL = "<?php echo base_url() . 'Manager/Procurement/get_log_table/'; ?>" + drugID
        $.get(logsURL, function (table) {
            $(tableID).html(table)
            $('.log_tbl').DataTable({
                pagingType: "full_numbers",
                order: [[1, "desc"]],
                initComplete: function () {
                    this.api().columns([1, 2, 3]).every(function () {
                        var column = this;
                        var select = $('<br/><select><option value="">Show all</option></select>')
                                .appendTo($(column.header()))
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            var val = $('<div/>').html(d).text();
                            select.append('<option value="' + val + '">' + val + '</option>');
                        });
                    });
                }
            });
            //$('#procurement_loader').hide();
        });
        $('#actual_qty').val('');
    }

    function LoadSpinner(divID) {
        var spinner = new Spinner().spin()
        $(divID).empty('')
        $(divID).height('auto')
        $(divID).append(spinner.el)
    }
</script>