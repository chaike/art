<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Default values
$config['data_year'] = date('Y', strtotime('-1 month'));
$config['data_month'] = date('M', strtotime('-1 month'));
$config['data_date'] = date('Y-m-01', strtotime('-1 month'));
$config['drug'] = 'Dolutegravir (DTG) 50mg Tabs';

//reporting_rates_chart
$config['reporting_rates_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view';
$config['reporting_rates_chart_title'] = 'Reporting Rates Trend';
$config['reporting_rates_chart_yaxis_title'] = 'No. of Orders';
$config['reporting_rates_chart_source'] = 'Source: www.commodities.nascop.org';
$config['reporting_rates_chart_has_drilldown'] = FALSE;
$config['reporting_rates_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility');
$config['reporting_rates_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
);

//patients_by_regimen_chart
$config['patients_by_regimen_chart_chartview'] = 'pages/dashboard/charts/bar_drilldown_view';
$config['patients_by_regimen_chart_title'] = 'Regimen Patient Numbers';
$config['patients_by_regimen_chart_yaxis_title'] = 'No. of Patients';
$config['patients_by_regimen_chart_source'] = 'Source: www.commodities.nascop.org';
$config['patients_by_regimen_chart_has_drilldown'] = TRUE;
$config['patients_by_regimen_chart_filters'] = array('data_year', 'data_month', 'county', 'sub_county', 'facility');
$config['patients_by_regimen_chart_filters_default'] = array(
    'data_year' => $config['data_year'],
    'data_month' => $config['data_month']
);

//drug_consumption_allocation_trend_chart
$config['drug_consumption_allocation_trend_chart_chartview'] = 'pages/dashboard/charts/combined_column_line_view';
$config['drug_consumption_allocation_trend_chart_title'] = 'Commodity Consumption/Allocation Trend';
$config['drug_consumption_allocation_trend_chart_yaxis_title'] = 'No. of Packs';
$config['drug_consumption_allocation_trend_chart_source'] = 'Source: www.commodities.nascop.org';
$config['drug_consumption_allocation_trend_chart_has_drilldown'] = FALSE;
$config['drug_consumption_allocation_trend_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['drug_consumption_allocation_trend_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//issues against consumption
$config['issues_consumption_chart_chartview'] = 'pages/dashboard/charts/issues_consumption_view';
$config['issues_consumption_chart_title'] = 'Issues / Consumption Trend';
$config['issues_consumption_yaxis_title'] = 'Average Issues Against Consumption';
$config['issues_consumption_chart_source'] = 'Source: www.commodities.nascop.org';
$config['issues_consumption_chart_has_drilldown'] = FALSE;
$config['issues_consumption_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['issues_consumption_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//stock_status_trend_chart
$config['stock_status_trend_chart_chartview'] = 'pages/dashboard/charts/combined_column_line_view_1';
$config['stock_status_trend_chart_title'] = 'Stock On Hand Trend';
$config['stock_status_trend_yaxis_title'] = 'Number of Packs';
$config['stock_status_trend_chart_source'] = 'Source: www.commodities.nascop.org';
$config['stock_status_trend_chart_has_drilldown'] = FALSE;
$config['stock_status_trend_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['stock_status_trend_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//stock_status_trend_chart
$config['stock_status_trend_hb_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_1';
$config['stock_status_trend_hb_chart_title'] = 'Patient Trends Per Regimen Per County';
$config['stock_status_trend_hb_yaxis_title'] = 'Number of patients';
$config['stock_status_trend_hb_chart_source'] = 'Source: www.commodities.nascop.org';
$config['stock_status_trend_hb_chart_has_drilldown'] = FALSE;
$config['stock_status_trend_hb_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['stock_status_trend_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//MoS Adult Chart
$config['mos_adult_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_amos';
//$config['mos_adult_chart_title'] = 'MoS Adult';
//$config['mos_adult_yaxis_title'] = 'Stock Status';
$config['mos_adult_chart_source'] = 'Source: www.commodities.nascop.org';
$config['mos_adult_chart_has_drilldown'] = FALSE;
$config['mos_adult_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_adult_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//MoS Adult Chart
$config['mos_fadult_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_amos_1';
//$config['mos_adult_chart_title'] = 'MoS Adult';
//$config['mos_adult_yaxis_title'] = 'Stock Status';
$config['mos_fadult_chart_source'] = 'Source: www.commodities.nascop.org';
$config['mos_fadult_chart_has_drilldown'] = FALSE;
$config['mos_fadult_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_fadult_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//MoS Paeds Chart
$config['mos_paeds_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_amos';
$config['mos_paeds_chart_title'] = 'MoS Paeds';
$config['mos_paeds_yaxis_title'] = 'Stock Status';
$config['mos_paeds_chart_source'] = 'Source: www.commodities.nascop.org';
$config['mos_paeds_chart_has_drilldown'] = FALSE;
$config['mos_paeds_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_paeds_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//Facility Paeds Chart
$config['mos_fpaeds_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_amos_1';
$config['mos_fpaeds_chart_title'] = 'MoS Paeds';
$config['mos_fpaeds_yaxis_title'] = 'Stock Status';
$config['mos_fpaeds_chart_source'] = 'Source: www.commodities.nascop.org';
$config['mos_fpaeds_chart_has_drilldown'] = FALSE;
$config['mos_fpaeds_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_fpaeds_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//MoS County By County Chart
$config['mos_county_chart_chartview'] = 'pages/dashboard/charts/column_rotated_stock_status';
//$config['mos_adult_chart_title'] = 'MoS Adult';
//$config['mos_adult_yaxis_title'] = 'Stock Status';
$config['mos_county_chart_source'] = '';
$config['mos_county_chart_has_drilldown'] = FALSE;
$config['mos_county_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_county_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//MoS County By Consumption Chart
$config['mos_county_consumption_chart_chartview'] = 'pages/dashboard/charts/column_rotated_stock_status_1';
//$config['mos_adult_chart_title'] = 'MoS Adult';
//$config['mos_adult_yaxis_title'] = 'Stock Status';
$config['mos_county_consumption_chart_source'] = '';
$config['mos_county_consumption_chart_has_drilldown'] = FALSE;
$config['mos_county_consumption_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['mos_county_consumption_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//forecast_chart
$config['forecast_chart_chartview'] = 'pages/procurement/charts/forecast_chart_view';
$config['forecast_chart_title'] = '';
$config['forecast_chart_yaxis_title'] = 'No. of Packs';
$config['forecast_chart_chart_source'] = 'Source: www.commodities.nascop.org';
$config['forecast_chart_chart_has_drilldown'] = FALSE;
$config['forecast_chart_filters'] = array('data_date', 'drug');
$config['forecast_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => array(
        $config['drug']
    )
);

//Supplier Delay
$config['supplier_chart_chartview'] = 'pages/procurement/charts/supplier_view';
$config['supplier_chart_title'] = 'Supplier Delay Times';
$config['supplier_chart_yaxis_title'] = 'No. of Days';
$config['supplier_chart_chart_source'] = 'Source: www.commodities.nascop.org';
$config['supplier_chart_chart_has_drilldown'] = FALSE;
$config['supplier_chart_filters'] = array('data_date', 'drug');
$config['supplier_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => array(
        $config['drug']
    )
);


$config['forecast_table_chartview'] = 'pages/dashboard/charts/table_view';
$config['forecast_table_title'] = '';
$config['forecast_table_yaxis_title'] = 'No. of Packs';
$config['forecast_table_source'] = '';
$config['forecast_table_has_drilldown'] = FALSE;
$config['forecast_table_filters'] = array('data_year', 'forecast', 'bought', 'fbpc', 'consumed', 'fcpc');
$config['forecast_table_filters_default'] = array(
    'data_year' => $config['data_year'],
    'data_month' => $config['data_month'],
    'drug' => $config['drug']
);

//MoS Facility Paeds Chart
$config['facility_mos_adult_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_facility';
$config['facility_mos_adult_chart_title'] = 'MoS Paeds';
$config['facility_mos_adult_yaxis_title'] = 'Stock Status';
$config['facility_mos_adult_chart_source'] = 'Source: www.commodities.nascop.org';
$config['facility_mos_adult_chart_has_drilldown'] = FALSE;
$config['facility_mos_adult_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['facility_mos_adult_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//MoS Facility Adults Chart
$config['facility_mos_paeds_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_view_facility';
$config['facility_mos_paeds_chart_title'] = 'MoS Paeds';
$config['facility_mos_paeds_yaxis_title'] = 'Stock Status';
$config['facility_mos_paeds_chart_source'] = 'Source: www.commodities.nascop.org';
$config['facility_mos_paeds_chart_has_drilldown'] = FALSE;
$config['facility_mos_paeds_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['facility_mos_paeds_hb_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);

//Two Pager Adults Chart
$config['two_pager_adult_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_two_pager';
$config['two_pager_adult_chart_title'] = 'MoS Paeds';
$config['two_pager_adult_yaxis_title'] = 'Stock Status';
$config['two_pager_adult_chart_source'] = 'Source: www.commodities.nascop.org';
$config['two_pager_adult_chart_has_drilldown'] = FALSE;
$config['two_pager_adult_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['two_pager_adult_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//Two Pager Paeds Chart
$config['two_pager_paeds_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_two_pager';
$config['two_pager_paeds_chart_title'] = 'MoS Paeds';
$config['two_pager_paeds_yaxis_title'] = 'Stock Status';
$config['two_pager_paeds_chart_source'] = 'Source: www.commodities.nascop.org';
$config['two_pager_paeds_chart_has_drilldown'] = FALSE;
$config['two_pager_paeds_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['two_pager_paeds_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);


//Two Pager Paeds Chart
$config['two_pager_oi_chart_chartview'] = 'pages/dashboard/charts/column_rotated_label_two_pager';
$config['two_pager_oi_chart_title'] = 'MoS Paeds';
$config['two_pager_oi_yaxis_title'] = 'Stock Status';
$config['two_pager_oi_chart_source'] = 'Source: www.commodities.nascop.org';
$config['two_pager_oi_chart_has_drilldown'] = FALSE;
$config['two_pager_oi_chart_filters'] = array('data_date', 'county', 'sub_county', 'facility', 'drug');
$config['two_pager_oi_chart_filters_default'] = array(
    'data_date' => $config['data_date'],
    'drug' => $config['drug']
);






//low_mos_commodity_table
$config['low_mos_commodity_table_chartview'] = 'pages/dashboard/charts/table_view';
$config['low_mos_commodity_table_title'] = 'Low MOS Commodities in Facilities';
$config['low_mos_commodity_table_yaxis_title'] = 'No. of Packs';
$config['low_mos_commodity_table_source'] = 'Source: www.commodities.nascop.org';
$config['low_mos_commodity_table_has_drilldown'] = FALSE;
$config['low_mos_commodity_table_filters'] = array('data_year', 'data_month', 'county', 'sub_county', 'facility', 'drug');
$config['low_mos_commodity_table_filters_default'] = array(
    'data_year' => $config['data_year'],
    'data_month' => $config['data_month'],
    'drug' => $config['drug']
);

//high_mos_commodity_table
$config['high_mos_commodity_table_chartview'] = 'pages/dashboard/charts/table_view';
$config['high_mos_commodity_table_title'] = 'High MOS Commodities in Facilities';
$config['high_mos_commodity_table_yaxis_title'] = 'No. of Packs';
$config['high_mos_commodity_table_source'] = 'Source: www.commodities.nascop.org';
$config['high_mos_commodity_table_has_drilldown'] = FALSE;
$config['high_mos_commodity_table_filters'] = array('data_year', 'data_month', 'county', 'sub_county', 'facility', 'drug');
$config['high_mos_commodity_table_filters_default'] = array(
    'data_year' => $config['data_year'],
    'data_month' => $config['data_month'],
    'drug' => $config['drug']
);
