<?php

defined('BASEPATH') OR exit('No direct script access allowed');
if (!class_exists("PHPExcel")) {
    require APPPATH . 'libraries/PHPExcel.php';
}

class Manager_model extends CI_Model {

    public $Cache;
    public $year = '';
    public $month = '';

    public function __construct() {
        parent::__construct();
        if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
            $client = new PDO('mysql:dbname=' . $this->config->item('all_db') . ';host=127.0.0.1', $this->config->item('local_user'), $this->config->item('local_pass'));
            $this->Cache = new \MatthiasMullie\Scrapbook\Adapters\MySQL($client);
        } else {
            $client = new PDO('mysql:dbname=' . $this->config->item('all_db') . ';host=127.0.0.1', $this->config->item('remote_user'), $this->config->item('remote_pass'));
            $this->Cache = new \MatthiasMullie\Scrapbook\Adapters\MySQL($client);
        }

        $this->year = date('Y');
        $this->month = date('M', strtotime('-2 Months'));
    }

    public $adult = "SELECT name FROM `vw_drug_list` where regimen_category like '%adult%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id in('3','8','65','44','63','38','67','69','25','31','41','60','62','98','64','61')";
    public $paed = "SELECT name FROM `vw_drug_list` where regimen_category like '%paed%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('71','2','52','30','31','32','68','15','20','19','26','69','16','17','18','21','49','43','85')";
    public $oi = "SELECT name FROM `vw_drug_list` where regimen_category like '%oi%' and kemsa_code is not null and kemsa_code !='0'";

    public function get_reporting_rates($filters) {
        $columns = array();
        $reporting_data = array(
            array('type' => 'column', 'name' => 'Reported', 'data' => array()),
            array('type' => 'column', 'name' => 'Not Reported', 'data' => array())
        );

//Get total_ordering_sites
        $scope_id = $this->session->userdata('scope');
        $role = $this->session->userdata('role');
        $this->db->from('tbl_facility f');
        $this->db->join('tbl_subcounty sc', 'sc.id = f.subcounty_id', 'inner');
        $this->db->join('tbl_county c', 'c.id = sc.county_id', 'inner');
        $this->db->where_in("category", array('central', 'standalone'));
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'county') {
                    $this->db->where_in('c.name', $filter);
                } else if ($category == 'sub_county') {
                    $this->db->where_in('sc.name', $filter);
                } else if ($category == 'facility') {
                    $this->db->where_in('f.name', $filter);
                }
            }
        }
        if ($role == 'subcounty') {
            $this->db->where('f.subcounty_id', $scope_id);
        } else if ($role == 'county') {
            $this->db->where('sc.county_id', $scope_id);
        } else if ($role == 'partner') {
            $this->db->where('f.partner_id', $scope_id);
        }
        $total_ordering_sites = $this->db->get()->num_rows();

        $this->db->select("CONCAT_WS('/', data_month, data_year) period, COUNT(*) reported, ($total_ordering_sites - COUNT(*)) not_reported", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'data_date') {
                    $this->db->where("data_date >= ", date('Y-01-01', strtotime($filter . "- 1 year")));
                    $this->db->where("data_date <=", $filter);
                    continue;
                }
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->where_in("facility_category", array('central', 'standalone'));
        $this->db->group_by('period');
        $this->db->order_by("data_year ASC, FIELD( data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )");
        $query = $this->db->get('dsh_order');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['period'];
                foreach ($reporting_data as $index => $report) {
                    if ($report['name'] == 'Reported') {
                        array_push($reporting_data[$index]['data'], $result['reported']);
                    } else if ($report['name'] == 'Not Reported') {
                        if ($result['not_reported'] < 0) {
                            $result['not_reported'] = 0;
                        }
                        array_push($reporting_data[$index]['data'], $result['not_reported']);
                    }
                }
            }
        }

        return array('main' => $reporting_data, 'columns' => $columns);
    }

    function createExcel() {
        $month = $this->input->post('data_month');
        $year = $this->input->post('data_year');
        unlink("public/temp/patients.xlsx");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("public/template_1.xlsx");
        $objPHPExcel->getActiveSheet(0);

        $worksheet = $objPHPExcel->getActiveSheet();
        $styleArray = array('font' => array('size' => 16, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => 'ff0000')));
        $styleArray2 = array('font' => array('size' => 14, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => '800000')));

        $row2 = 4;
        $i = 1;
        $col = 0;

        $units = $this->db->query("SELECT regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    GROUP BY `name` ORDER BY `y` DESC;")->result();

        foreach ($units as $u) {
            $worksheet->setCellValueByColumnAndRow(0, $row2++, $u->name)
                    ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($u->y, 0));
            $int = (int) $row2 - 1;
            $worksheet->getStyle('A' . $int . ':B' . $int)->applyFromArray($styleArray);
            $category = $this->db->query("SELECT regimen_category category, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$u->name'
                                    GROUP BY `regimen_category` ORDER BY `y` DESC;")->result();

            foreach ($category as $c) {
                $worksheet->setCellValueByColumnAndRow(0, $row2++, $c->category)
                        ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($c->y));
                $int2 = (int) $row2 - 1;
                $worksheet->getStyle('A' . $int2 . ':B' . $int2)->applyFromArray($styleArray2);
                $sub = $this->db->query("SELECT regimen, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$u->name' AND regimen_category ='$c->category'
                                    GROUP BY `regimen`  ORDER BY `y` DESC;")->result();

                foreach ($sub as $s) {
                    $worksheet->setCellValueByColumnAndRow(0, $row2++, $s->regimen)
                            ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($s->y));
                }
                $worksheet->setCellValueByColumnAndRow(0, $row2++, ' ');
            }
            $row2++;
            $i++;
        }

        $objPHPExcel->getActiveSheet()->setCellValue('A2', $month . '-' . $year);
        $objPHPExcel->getActiveSheet()->setTitle('Patient Numbers');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("public/temp/patients.xlsx");
// redirect("public/temp/patients.xlsx");
        echo 'Data exported';
    }

    function get_units($year, $month) {
        return $this->db->query("SELECT regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('" . $year . "') 
                                    AND `data_month` IN('" . $month . "') 
                                    GROUP BY `name` ORDER BY `y` DESC;")->result();
    }

    public function get_categories($year, $month) {
        return $this->db->query("SELECT regimen_category category,regimen_service, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('" . $year . "') 
                                    AND `data_month` IN('" . $month . "') 
                                    GROUP BY `regimen_category` ORDER BY `y` DESC;")->result();
    }

    public function get_sub_categories($year, $month) {
        return $this->db->query("SELECT regimen,regimen_service,regimen_category, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('" . $year . "') 
                                    AND `data_month` IN('" . $month . "') 
                                    GROUP BY `regimen`  ORDER BY `y` DESC;")->result();
    }

    function createExcelLevel2() {
        $month = $this->input->post('data_month');
        $year = $this->input->post('data_year');
        $cat = $this->input->post('cat');
        unlink("public/temp/patients.xlsx");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("public/template_1.xlsx");
        $objPHPExcel->getActiveSheet(0);

        $worksheet = $objPHPExcel->getActiveSheet();
        $styleArray = array('font' => array('size' => 16, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => 'ff0000')));
        $styleArray2 = array('font' => array('size' => 14, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => '800000')));

        $row2 = 4;
        $i = 1;
        $col = 0;

        $units = $this->db->query("SELECT regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND  regimen_service = '$cat'
                                    GROUP BY `name` ORDER BY `y` DESC;")->result();

        foreach ($units as $u) {
            $worksheet->setCellValueByColumnAndRow(0, $row2++, $u->name)
                    ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($u->y, 0));
            $int = (int) $row2 - 1;
            $worksheet->getStyle('A' . $int . ':B' . $int)->applyFromArray($styleArray);
            $category = $this->db->query("SELECT regimen_category category, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$u->name'
                                    GROUP BY `regimen_category` ORDER BY `y` DESC;")->result();

            foreach ($category as $c) {
                $worksheet->setCellValueByColumnAndRow(0, $row2++, $c->category)
                        ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($c->y));
                $int2 = (int) $row2 - 1;
                $worksheet->getStyle('A' . $int2 . ':B' . $int2)->applyFromArray($styleArray2);
                $sub = $this->db->query("SELECT regimen, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$u->name' AND regimen_category ='$c->category'
                                    GROUP BY `regimen`  ORDER BY `y` DESC;")->result();

                foreach ($sub as $s) {
                    $worksheet->setCellValueByColumnAndRow(0, $row2++, $s->regimen)
                            ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($s->y));
                }
                $worksheet->setCellValueByColumnAndRow(0, $row2++, ' ');
            }
            $row2++;
            $i++;
        }

        $objPHPExcel->getActiveSheet()->setCellValue('A2', $month . '-' . $year);
        $objPHPExcel->getActiveSheet()->setTitle('Patient Numbers');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("public/temp/patients.xlsx");
// redirect("public/temp/patients.xlsx");
        echo 'Data exported';
    }

    function createExcelLevel3() {
        $month = $this->input->post('data_month');
        $year = $this->input->post('data_year');
        $cat = $this->input->post('cat');
        $subcat = $this->input->post('subcat');
        unlink("public/temp/patients.xlsx");
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load("public/template_1.xlsx");
        $objPHPExcel->getActiveSheet(0);

        $worksheet = $objPHPExcel->getActiveSheet();
        $styleArray = array('font' => array('size' => 16, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => 'ff0000')));
        $styleArray2 = array('font' => array('size' => 14, 'style' => 'italic', 'bold' => true, 'color' => array('rgb' => '800000')));

        $row2 = 4;
        $i = 1;
        $col = 0;

        $units = $this->db->query("SELECT regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND  regimen_service = '$cat'                                   
                                    GROUP BY `name` ORDER BY `y` DESC;")->result();

        foreach ($units as $u) {
            $worksheet->setCellValueByColumnAndRow(0, $row2++, $u->name)
                    ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($u->y, 0));
            $int = (int) $row2 - 1;
            $worksheet->getStyle('A' . $int . ':B' . $int)->applyFromArray($styleArray);
            $category = $this->db->query("SELECT regimen_category category, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$cat' AND regimen_category = '$subcat'                                   
                                    GROUP BY `regimen_category` ORDER BY `y` DESC;")->result();

            foreach ($category as $c) {
                $worksheet->setCellValueByColumnAndRow(0, $row2++, $c->category)
                        ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($c->y));
                $int2 = (int) $row2 - 1;
                $worksheet->getStyle('A' . $int2 . ':B' . $int2)->applyFromArray($styleArray2);
                $sub = $this->db->query("SELECT regimen, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown 
                                    FROM `dsh_patient` 
                                    WHERE `data_year` IN('$year') 
                                    AND `data_month` IN('$month') 
                                    AND regimen_service ='$u->name' AND regimen_category = '$subcat'
                                    GROUP BY `regimen`  ORDER BY `y` DESC;")->result();

                foreach ($sub as $s) {
                    $worksheet->setCellValueByColumnAndRow(0, $row2++, $s->regimen)
                            ->setCellValueByColumnAndRow(1, $row2 - 1, number_format($s->y));
                }
                $worksheet->setCellValueByColumnAndRow(0, $row2++, ' ');
            }
            $row2++;
            $i++;
        }

        $objPHPExcel->getActiveSheet()->setCellValue('A2', $month . '-' . $year);
        $objPHPExcel->getActiveSheet()->setTitle('Patient Numbers');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save("public/temp/patients.xlsx");
// redirect("public/temp/patients.xlsx");
        echo 'Data exported';
    }

    public function get_patient_regimen($filters) {
//print_r($filters);
        $this->db->select("regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
//$this->db->where('regimen_service','HepB');
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('dsh_patient');
        return $this->get_patient_regimen_drilldown_level1(array('main' => $query->result_array()), $filters);
    }

    public function get_patient_regimen_hb($filters) {
//print_r($filters);
        $this->db->select("regimen_service name, SUM(total) y, LOWER(REPLACE(regimen_service, ' ', '_')) drilldown", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->where('regimen_service', 'HepB');
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('dsh_patient');
        return $this->get_patient_regimen_drilldown_level1(array('main' => $query->result_array()), $filters);
    }

    public function get_patient_regimen_drilldown_level1($main_data, $filters) {
        $drilldown_data = array();
        $this->db->select("LOWER(REPLACE(regimen_service, ' ', '_')) category, regimen_category name, SUM(total) y, LOWER(CONCAT_WS('_', REPLACE(regimen_service, ' ', '_'), REPLACE(regimen_category, ' ', '_'))) drilldown", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drilldown');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('dsh_patient');
        $sub_data = $query->result_array();

        if ($main_data) {
            foreach ($main_data['main'] as $counter => $main) {
                $category = $main['drilldown'];

                $drilldown_data['drilldown'][$counter]['id'] = $category;
                $drilldown_data['drilldown'][$counter]['name'] = ucwords($category);
                $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;

                foreach ($sub_data as $sub) {
                    if ($category == $sub['category']) {
                        unset($sub['category']);
                        $drilldown_data['drilldown'][$counter]['data'][] = $sub;
                    }
                }
            }
        }
        $drilldown_data = $this->get_patient_regimen_drilldown_level2($drilldown_data, $filters);
        return array_merge($main_data, $drilldown_data);
    }

    public function get_patient_regimen_drilldown_level2($drilldown_data, $filters) {
        $this->db->select("LOWER(CONCAT_WS('_', REPLACE(regimen_service, ' ', '_'), REPLACE(regimen_category, ' ', '_'))) line, regimen name, SUM(total) y", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('dsh_patient');
        $regimen_data = $query->result_array();

        if ($drilldown_data) {
            $counter = sizeof($drilldown_data['drilldown']);
            foreach ($drilldown_data['drilldown'] as $main_data) {
                foreach ($main_data['data'] as $item) {
                    $filter_value = $item['name'];
                    $filter_name = $item['drilldown'];

                    $drilldown_data['drilldown'][$counter]['id'] = $filter_name;
                    $drilldown_data['drilldown'][$counter]['name'] = ucwords($filter_name);
                    $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;

                    foreach ($regimen_data as $regimen) {
                        if ($filter_name == $regimen['line']) {
                            unset($regimen['line']);
                            $drilldown_data['drilldown'][$counter]['data'][] = $regimen;
                        }
                    }
                    $counter += 1;
                }
            }
        }
        return $drilldown_data;
    }

    public function get_drug_consumption_allocation_trend_hb($filters) {
        unset($filters['drug']);
// print_r($filters);
//echo date('M', strtotime($filters['data_date']));
// die;
//die;
        $columns = array();
        $scaleup_data = array(
            array('type' => 'line', 'name' => 'HPB1A | TDF + 3TC (HIV-ve HepB patients)', 'data' => array()),
            array('type' => 'line', 'name' => 'HPB1B | TDF + FTC (HIV-ve HepB patients)', 'data' => array())
        );
        $this->db->select("CONCAT_WS('/', data_month, data_year) period, SUM(total) hpb1a, SUM(total) hpb1b", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'data_date') {
                    $this->db->where("data_month", date('M', strtotime($filters['data_date'])));
                    $this->db->where("data_year", date('Y', strtotime($filters['data_date'])));
                } else {
                    $this->db->where_in($category, $filter);
                }
            }
        }
        $this->db->where('regimen_service', 'HepB');
        $this->db->group_by('county');
        $this->db->order_by("total ASC");
        $query = $this->db->get('dsh_patien');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['county'];
                foreach ($scaleup_data as $index => $scaleup) {
                    if ($scaleup['name'] == 'HPB1A | TDF + 3TC (HIV-ve HepB patients)') {
                        array_push($scaleup_data[$index]['data'], $result['hpb1a']);
                    } else if ($scaleup['name'] == 'HPB1B | TDF + FTC (HIV-ve HepB patients)') {
                        array_push($scaleup_data[$index]['data'], $result['hpb1b']);
                    }
                }
            }
        }
        return array('main' => $scaleup_data, 'columns' => $columns);
    }

    public function get_drug_consumption_allocation_trend($filters) {

        $columns = array();
        $scaleup_data = array(
            array('type' => 'areaspline', 'name' => 'Allocated', 'data' => array()),
            array('type' => 'areaspline', 'name' => 'Consumed', 'data' => array())
        );
        $this->db->select("CONCAT_WS('/', data_month, data_year) period, SUM(total) consumed_total, SUM(allocated) allocated_total", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'data_date') {
                    $this->db->where("data_date >= ", date('Y-01-01', strtotime($filter . "- 1 year")));
                    $this->db->where("data_date <=", $filter);
                } else {
                    $this->db->where_in($category, $filter);
                }
            }
        }
        $this->db->group_by('period');
        $this->db->order_by("data_year ASC, FIELD( data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )");
        $query = $this->db->get('dsh_consumption');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['period'];
                foreach ($scaleup_data as $index => $scaleup) {
                    if ($scaleup['name'] == 'Allocated') {
                        array_push($scaleup_data[$index]['data'], $result['allocated_total']);
                    } else if ($scaleup['name'] == 'Consumed') {
                        array_push($scaleup_data[$index]['data'], $result['consumed_total']);
                    }
                }
            }
        }
        return array('main' => $scaleup_data, 'columns' => $columns);
    }

    public function get_drug_soh_trend($filters) {
        $columns = array();
        $soh_data = array(
            array(
                'type' => 'column',
                'name' => 'Stock on Hand',
                'data' => array()),
            array(
                'type' => 'spline',
                'color' => '#9400D3',
                'positiveColor' => '#9400D3',
                'name' => 'Average Monthly Consumption (AMC)',
                'data' => array()),
            array(
                'type' => 'line',
                'color' => '#FF0000',
                'negativeColor' => '#0088FF',
                'name' => 'Months of Stock (MOS)',
                'data' => array())
        );

        $this->db->select("drug, CONCAT_WS('/', data_month, data_year) period, SUM(total) soh_total, SUM(amc_total) amc_total, (SUM(total)/SUM(amc_total)) mos_total", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                if ($category == 'data_date') {
                    $this->db->where("data_date >= ", date('Y-01-01', strtotime($filter . "- 1 year")));
                    $this->db->where("data_date <=", $filter);
                    continue;
                }
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by("drug, period");
        $this->db->order_by("data_year ASC, FIELD( data_month, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' )");
        $query = $this->db->get('dsh_stock');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['period'];
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Stock on Hand') {
                        array_push($soh_data[$index]['data'], $result['soh_total']);
                    } else if ($soh['name'] == 'Average Monthly Consumption (AMC)') {
                        array_push($soh_data[$index]['data'], $result['amc_total']);
                    } else if ($soh['name'] == 'Months of Stock (MOS)') {
                        array_push($soh_data[$index]['data'], empty($result['mos_total']) ? 0 : $result['mos_total']);
                    }
                }
            }
        }
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_drug_soh_trend_hb($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        $columns = array();
        $soh_data = array(
// array(
// 'type' => 'column',
// 'name' => 'Stock on Hand',
// 'data' => array()),
            array(
                'type' => 'column',
                'color' => '#9400D3',
                'positiveColor' => '#9400D3',
                'name' => 'HPB1A | TDF + 3TC (HIV-ve HepB patients)',
                'data' => array()),
            array(
                'type' => 'column',
                'color' => '#FF0000',
                'negativeColor' => '#0088FF',
                'name' => 'HPB1B | TDF + FTC (HIV-ve HepB patients)',
                'data' => array())
        );


        $results = $this->db->query("SELECT name county,IF(a IS NULL,0,a) a,IF(b IS NULL,0,b)b ,SUM(IF(a IS NULL,0,a)+IF(b IS NULL,0,b)) hb
                            FROM tbl_county c
                            LEFT JOIN
                              (
                               SELECT county, SUM(total) a 
                               FROM dsh_patient 
                              WHERE `data_month` = '$datamonth' 
                              AND `data_year` = '$datayear'
                            AND regimen='HPB1A | TDF + 3TC (HIV-ve HepB patients)'
                            GROUP BY `county`
                            ORDER BY `total` ASC 
                            ) t2 ON c.name = t2.county


                            LEFT JOIN (
                            SELECT county,SUM(total) b
                            FROM `dsh_patient`
                            WHERE `data_month` = '$datamonth' 
                            AND `data_year` = '$datayear'
                            AND regimen='HPB1B | TDF + FTC (HIV-ve HepB patients)'
                            GROUP BY county
                            ) t1 ON c.name = t1.county
                            GROUP BY name ORDER BY hb DESC")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['county']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Patient Numbers Trends') {
                        array_push($soh_data[$index]['data'], $result['hb']);
                    } else if ($soh['name'] == 'HPB1A | TDF + 3TC (HIV-ve HepB patients)') {
                        array_push($soh_data[$index]['data'], $result['a']);
                    } else if ($soh['name'] == 'HPB1B | TDF + FTC (HIV-ve HepB patients)') {
                        array_push($soh_data[$index]['data'], empty($result['b']) ? 0 : $result['b']);
                    }
                }
            }
        }
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_adult_mos_2pager($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));

        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }

        $columns = array();
        $soh_data = array(
            /* array(
              'type' => 'column',
              'color' => '#9aa4ec',
              'negativeColor' => '#9aa4ec',
              'name' => 'Ordered',
              'data' => array()),
              array(
              'type' => 'column',
              'color' => '#58e3bd',
              'positiveColor' => '#58e3bd',
              'name' => 'Stock On Hand',
              'yAxis' => 0,
              'data' => array()), */
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending Months of Stock (MOS)',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Actual Months of Stock (MOS)',
                'yAxis' => 0,
                'data' => array()),
        );
        if (empty($this->Cache->get('2pager'))) {

            /* $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,pi.pending_qty ordered,
              ROUND((k.close_kemsa)/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1) mos,
              IF(pi.pending_qty <=0,0,ROUND((pi.pending_qty )/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1)) pmos ,k.data_month,k.data_year,k.data_date
              FROM vw_drug_list d
              INNER JOIN vw_procurement_list k ON k.drug_id = d.id
              LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
              WHERE d.drug_category='ARV'
              AND k.data_month = pi.data_month
              AND k.data_year = pi.data_year
              AND k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
              AND d.name IN($this->adult) ORDER BY mos ASC")->result_array();

              //AND d.name IN(SELECT name FROM `vw_drug_list` where regimen_category like '%paed%' and kemsa_code is not null and kemsa_code !='0' and drug_category='ARV' and id not in('71');)")->result_array();
             */ $results = $this->db->query("Select * FROM adult_national_mos WHERE type='adult'  AND data_year='$datayear' AND data_month='$datamonth' AND id NOT IN('5','7') GROUP BY drug ORDER BY kemsa DESC")->result_array();

            if ($results) {
                foreach ($results as $result) {
                    $columns[] = strtoupper($result['drug']);
                    foreach ($soh_data as $index => $soh) {
//if ($soh['name'] == 'Ordered') {
//array_push($soh_data[$index]['data'], $result['ordered']);
// if ($soh['name'] == 'Stock On Hand') {
// array_push($soh_data[$index]['data'], $result['soh']);
                        if ($soh['name'] == 'Actual Months of Stock (MOS)') {
                            array_push($soh_data[$index]['data'], $result['kemsa']);
                        } else if ($soh['name'] == 'Pending Months of Stock (MOS)') {
//array_push($soh_data[$index]['data'], $result['pmos']);
                            array_push($soh_data[$index]['data'], empty($result['pmos']) ? 0 : $result['pmos']);
                        }
                    }
                }
            }
            $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
            $this->Cache->add('2pager'.$datamonth.$datayear, $data);
            return array('main' => $soh_data, 'columns' => $columns);
        } else {
            return json_decode($this->Cache->get('2pager'.$datamonth.$datayear), true);
        }
    }

    public function get_paeds_mos_2pager($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }


        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $columns = array();
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending Months of Stock (MOS)',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Actual Months of Stock (MOS)',
                'yAxis' => 0,
                'data' => array()),
        );

        $day = date('Y-m-d', strtotime('first day of previous month'));
        if (empty($this->Cache->get('kemsapaed' . $datamonth . $datayear))) {
            /* $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,pi.pending_qty ordered,
              ROUND((k.close_kemsa)/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1) mos,
              IF(pi.pending_qty <=0,0,ROUND((pi.pending_qty )/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1)) pmos ,k.data_month,k.data_year,k.data_date
              FROM vw_drug_list d
              INNER JOIN vw_procurement_list k ON k.drug_id = d.id
              LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
              WHERE d.drug_category='ARV'
              AND k.data_month = pi.data_month
              AND k.data_year = pi.data_year
              AND k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
              AND d.name IN($this->paed) ORDER BY mos ASC;")->result_array(); */
            $results = $this->db->query("Select * FROM adult_national_mos WHERE type='paed'  AND data_year='$datayear' AND data_month='$datamonth' GROUP BY drug ORDER BY kemsa DESC")->result_array();

            if ($results) {
                foreach ($results as $result) {
                    $columns[] = strtoupper($result['drug']);
                    foreach ($soh_data as $index => $soh) {
                        if ($soh['name'] == 'Actual Months of Stock (MOS)') {
                            array_push($soh_data[$index]['data'], $result['kemsa']);
                        } else if ($soh['name'] == 'Pending Months of Stock (MOS)') {
// array_push($soh_data[$index]['data'], $result['pmos']);
                            array_push($soh_data[$index]['data'], empty($result['pmos']) ? 0 : $result['pmos']);
                        }
                    }
                }
            }
            $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
            $this->Cache->add('kemsapaed' . $datamonth . $datayear, $data);
            return array('main' => $soh_data, 'columns' => $columns);
        } else {
            return json_decode($this->Cache->get('kemsapaed' . $datamonth . $datayear), true);
        }
    }

    public function get_facility_paeds_mos_2pager($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        $columns = array();
        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }

        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Facilities',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Central Store',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending',
                'yAxis' => 0,
                'data' => array()),
        );

//$day = date('Y-m-d', strtotime('first day of previous month'));


        if (empty($this->Cache->get('facpad' . $datayear . $datamonth))) {
            /* $results = $this->db->query("SELECT d.id, CONCAT(d.name,' - ', d.pack_size,'s') drug,k.close_kemsa soh, pi.pending_qty pending,
              ROUND((k.close_kemsa)/fn_national_amc(d.name, DATE_FORMAT('2019-12-01' ,'%Y-%m-01')),1) kemsa,
              ROUND((fn_get_stocks(d.name,DATE_FORMAT('2019-12-01' - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1) facilities,
              IF(pi.pending_qty <=0,0,ROUND((pi.pending_qty )/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1)) pmos ,k.data_month,k.data_year,k.data_date
              FROM vw_drug_list d
              LEFT JOIN vw_procurement_list k ON k.drug_id = d.id
              LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
              WHERE  k.data_month = pi.data_month
              AND k.data_year = pi.data_year
              AND k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
              AND d.name IN($this->paed)
              AND k.drug_id NOT IN('85','45')
              ORDER BY facilities ASC;")->result_array(); */

            $results = $this->db->query("Select * FROM adult_national_mos WHERE type='paed' AND data_year='$datayear' AND data_month='$datamonth' GROUP BY drug ORDER BY SUM(facilities + kemsa + pmos) DESC")->result_array();
            if ($results) {
                foreach ($results as $result) {
                    $columns[] = strtoupper($result['drug']);
                    foreach ($soh_data as $index => $soh) {
                        if ($soh['name'] == 'Central Store') {
                            array_push($soh_data[$index]['data'], empty($result['kemsa']) ? 0 : $result['kemsa']);
                        } else if ($soh['name'] == 'Pending') {
                            array_push($soh_data[$index]['data'], empty($result['pmos']) ? 0 : $result['pmos']);
                        } else if ($soh['name'] == 'Facilities') {
                            array_push($soh_data[$index]['data'], empty($result['facilities']) ? 0 : $result['facilities']);
                        }
                    }
                }
            }
//echo '<pre>';
//print_r($soh_data);
// die;
            $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
            $this->Cache->add('facpad' . $datayear . $datamonth, $data);
            return array('main' => $results, 'columns' => $columns);
        } else {
            return json_decode($this->Cache->get('facpad' . $datayear . $datamonth), true);
        }
    }

    public function get_facility_adult_mos_2pager($filters) {


        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }


        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));

        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }


        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $columns = array();
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => ' #4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Facilities',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#c0504d',
                'name' => 'Central Store',
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#9bbb59',
                'name' => 'Pending',
                'yAxis' => 0,
                'data' => array()),
        );

        $day = date('Y-m-d', strtotime('first day of previous month'));
        //if (empty($this->Cache->get('adult_mos' . $datayear . $datamonth))) {
        /*            $results = $this->db->query("SELECT d.id, CONCAT(d.name,' - ', d.pack_size,'s') drug,k.close_kemsa soh, pi.pending_qty pending,
          ROUND((k.close_kemsa)/fn_national_amc(d.name, DATE_FORMAT('2019-12-01' ,'%Y-%m-01')),1) kemsa,
          ROUND((fn_get_stocks(d.name,DATE_FORMAT('2019-12-01' - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1) facilities,
          IF(pi.pending_qty <=0,0,ROUND((pi.pending_qty )/fn_national_amc(d.name, DATE_FORMAT('2019-12-01','%Y-%m-01')),1)) pmos ,k.data_month,k.data_year,k.data_date
          FROM vw_drug_list d
          LEFT JOIN vw_procurement_list k ON k.drug_id = d.id
          LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
          WHERE  k.data_month = pi.data_month
          AND k.data_year = pi.data_year
          AND k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
          AND d.name IN($this->adult)
          ORDER BY facilities ASC;")->result_array(); */
        $results = $this->db->query("Select * FROM adult_national_mos WHERE type='adult' AND data_year='$datayear' AND data_month='$datamonth' GROUP BY drug ORDER BY SUM(facilities + kemsa + pmos) DESC")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Central Store') {
                        array_push($soh_data[$index]['data'], empty($result['kemsa']) ? 0 : $result['kemsa']);
                    } else if ($soh['name'] == 'Pending') {
                        array_push($soh_data[$index]['data'], empty($result['pmos']) ? 0 : $result['pmos']);
                    } else if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], empty($result['facilities']) ? 0 : $result['facilities']);
                    }
                }
            }
        }
//echo '<pre>';
//print_r($soh_data);
// die;
        $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
        $this->Cache->add('adult_mos' . $datayear . $datamonth, $data);
        return array('main' => $soh_data, 'columns' => $columns);
        //} else {
        //return array('main' => $soh_data, 'columns' => $columns);
        // return json_decode($this->Cache->get('adult_mos' . $datayear . $datamonth), true);
        //}
    }

    public function get_facility_main_paeds_mos_2pager($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        $columns = array();
        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }

        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Actual Months of Stock (MOS)',
                'yAxis' => 0,
                'data' => array()),
        );

//$day = date('Y-m-d', strtotime('first day of previous month'));

        /* $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,k.close_kemsa soh, pi.pending_qty ordered,
          ROUND((fn_get_stocks(d.name,DATE_FORMAT('2019-12-01' - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name,  DATE_FORMAT('2019-12-01','%Y-%m-01')),1)  mos
          FROM vw_drug_list d INNER JOIN vw_procurement_list k ON k.drug_id = d.id
          LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
          WHERE k.data_month = pi.data_month
          AND k.data_year = pi.data_year
          AND  k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
          AND d.name IN($this->paed) ORDER BY mos ASC;")->result_array(); */
        $results = $this->db->query("Select * FROM adult_national_mos WHERE type='paed'  AND data_year='$datayear' AND data_month='$datamonth'  ORDER BY facilities DESC")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Actual Months of Stock (MOS)') {
                        array_push($soh_data[$index]['data'], $result['facilities']);
                    }
                }
            }
        }

        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_facility_main_adult_mos_2pager($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $columns = array();

        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#4fb1bd',
                'name' => 'Actual Months of Stock (MOS)',
                'yAxis' => 0,
                'data' => array()),
        );

        $day = date('Y-m-d', strtotime('first day of previous month'));
        if (empty($this->Cache->get('facmain' . $datamonth . $datayear))) {
            /* $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,k.close_kemsa soh, pi.pending_qty ordered,
              ROUND((fn_get_stocks(d.name,DATE_FORMAT('2019-12-01' - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name,  DATE_FORMAT('2019-12-01','%Y-%m-01')),1)  mos
              FROM vw_drug_list d INNER JOIN vw_procurement_list k ON k.drug_id = d.id
              LEFT JOIN dsh_pending_procurements pi ON pi.drug_id = d.id
              WHERE k.data_month = pi.data_month
              AND k.data_year = pi.data_year
              AND  k.data_date= DATE_FORMAT('2019-12-01','%Y-%m-01')
              AND d.name IN($this->adult) ORDER BY mos ASC;")->result_array(); */
            $results = $this->db->query("Select * FROM adult_national_mos WHERE type='adult'  AND data_year='$datayear' AND data_month='$datamonth'  ORDER BY facilities DESC")->result_array();

            if ($results) {
                foreach ($results as $result) {
                    $columns[] = strtoupper($result['drug']);
                    foreach ($soh_data as $index => $soh) {
                        if ($soh['name'] == 'Actual Months of Stock (MOS)') {
                            array_push($soh_data[$index]['data'], $result['facilities']);
                        }
                    }
                }
            }

            $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
            $this->Cache->add('facmain' . $datamonth . $datayear, $data);
            return array('main' => $results, 'columns' => $columns);
        } else {
            return json_decode($this->Cache->get('facmain' . $datamonth . $datayear), true);
        }
    }

    public function get_two_pager_adult($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }
        $columns = array();
        $date_now = date('Y-m-d');
        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#99ff99',
                'name' => 'Facilities',
                'yAxis' => 0,
                'index' => 0,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#00d2ff',
                'name' => 'CMS',
                'index' => 1,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#00d2ff',
                'name' => 'Pending Orders',
                'index' => 2,
                'data' => array()),
        );

//$day = date('Y-m-d', strtotime('first day of previous month'));

        $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,
                        ROUND((fn_get_stocks(d.name,DATE_FORMAT($date - INTERVAL 1 MONTH,'%Y-%m-01')))/get_mos_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01')),1)  mos,
                        IF(pi.contracted <=0,0,ROUND((pi.contracted/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  contracted,
                        IF(pi.pending <=0,0,ROUND((pi.pending/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  pending
                        FROM vw_drug_list d 
                        INNER JOIN vw_procurement_list k ON k.drug_id = d.id 
                        INNER JOIN tbl_procurement_item pi ON pi.drug_id = d.id 
                        WHERE k.data_month = pi.month 
                        AND k.data_year = pi.year 
                        AND  k.data_date= DATE_FORMAT('$date_now','%Y-%m-01') 
                        AND d.name IN($this->adult) ORDER BY mos ASC;")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], $result['mos']);
                    } else if ($soh['name'] == 'CMS') {
                        array_push($soh_data[$index]['data'], empty($result['contracted']) ? 0 : $result['contracted']);
                    } else if ($soh['name'] == 'Pending Orders') {
                        array_push($soh_data[$index]['data'], empty($result['pending']) ? 0 : $result['pending']);
                    }
                }
            }
        }
// echo '<pre>';
// print_r($soh_data);
// die;
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_two_pager_paeds($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }
        $date_now = date('Y-m-d');
        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));

        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $columns = array();
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#99ff99',
                'name' => 'Facilities',
                'index' => 0,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#00d2ff',
                'name' => 'CMS',
                'index' => 1,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#00d2ff',
                'name' => 'Pending Orders',
                'index' => 2,
                'data' => array()),
        );

        $day = date('Y-m-d', strtotime('first day of previous month'));

        $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,
                        ROUND((fn_get_stocks(d.name,DATE_FORMAT($date - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01')),1)  mos,
                        IF(pi.contracted <=0,0,ROUND((pi.contracted/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  contracted,
                        IF(pi.pending <=0,0,ROUND((pi.pending/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  pending
                        FROM vw_drug_list d 
                        INNER JOIN vw_procurement_list k ON k.drug_id = d.id 
                        INNER JOIN tbl_procurement_item pi ON pi.drug_id = d.id 
                        WHERE k.data_month = pi.month 
                        AND k.data_year = pi.year 
                        AND  k.data_date= DATE_FORMAT('$date_now','%Y-%m-01') 
                        AND d.name IN($this->paed) ORDER BY mos ASC")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], $result['mos']);
                    } else if ($soh['name'] == 'CMS') {
                        array_push($soh_data[$index]['data'], empty($result['contracted']) ? 0 : $result['contracted']);
                    } else if ($soh['name'] == 'Pending Orders') {
                        array_push($soh_data[$index]['data'], empty($result['pending']) ? 0 : $result['pending']);
                    }
                }
            }
        }
//echo '<pre>';
// print_r($soh_data);
// die;
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_two_pager_oi($filters) {
        unset($filters['drug']);
        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }
        $date_now = date('Y-m-d');
        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        if (empty($filters['data_date'])) {
            $date = 'NOW()';
        } else {
            $date = "'" . $filters['data_date'] . "'";
        }
        $columns = array();
        $soh_data = array(
            array(
                'type' => 'bar',
                'color' => '#4fb1bd',
                'negativeColor' => '#99ff99',
                'name' => 'Facilities',
                'yAxis' => 0,
                'index' => 0,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#c0504d',
                'negativeColor' => '#00d2ff',
                'name' => 'CMS',
                'index' => 1,
                'data' => array()),
            array(
                'type' => 'bar',
                'color' => '#9bbb59',
                'negativeColor' => '#00d2ff',
                'name' => 'Pending Orders',
                'index' => 2,
                'data' => array()),
        );

        $day = date('Y-m-d', strtotime('first day of previous month'));

        $results = $this->db->query("SELECT d.id,CONCAT(d.name,' - ', d.pack_size,'s') drug,
                        ROUND((fn_get_stocks(d.name,DATE_FORMAT($date - INTERVAL 1 MONTH,'%Y-%m-01')))/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01')),1)  mos,
                        IF(pi.contracted <=0,0,ROUND((pi.contracted/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  contracted,
                        IF(pi.pending <=0,0,ROUND((pi.pending/fn_national_amc(d.name,  DATE_FORMAT($date,'%Y-%m-01'))),1))  pending
                        FROM vw_drug_list d 
                        INNER JOIN vw_procurement_list k ON k.drug_id = d.id 
                        INNER JOIN tbl_procurement_item pi ON pi.drug_id = d.id 
                        WHERE k.data_month = pi.month 
                        AND k.data_year = pi.year 
                        AND  k.data_date= DATE_FORMAT('$date_now','%Y-%m-01') 
                        AND d.name IN($this->oi) ORDER BY mos ASC;")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['drug']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Facilities') {
                        array_push($soh_data[$index]['data'], $result['mos']);
                    } else if ($soh['name'] == 'CMS') {
                        array_push($soh_data[$index]['data'], empty($result['contracted']) ? 0 : $result['contracted']);
                    } else if ($soh['name'] == 'Pending Orders') {
                        array_push($soh_data[$index]['data'], empty($result['pending']) ? 0 : $result['pending']);
                    }
                }
            }
        }
//echo '<pre>';
//print_r($soh_data);
// die;
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_2pager_forecast($filters = '') {
        $columns = array();
        $scaleup_data = array(
            array('type' => 'column', 'name' => 'Forecusted to buy', 'data' => array()),
            array('type' => 'column', 'name' => 'Bought', 'data' => array()),
            array('type' => 'column', 'name' => 'Forecusted to Consume', 'data' => array()),
            array('type' => 'column', 'name' => 'Consumed', 'data' => array())
        );

//$day = date('Y-m-d', strtotime('first day of previous month'));
// $last_month = "fn_get_issues_average`('d.id', '$day')";
        if (empty($this->Cache->get('forecast'))) {
            $results = $this->db->query("SELECT 
                            d.name drug,
                            f.available_qty stocks_available_in_july_2019,
                            f.pending_contracted_qtys pending_qtys_july,
                            f.qty_to_procure qty_to_procure_2019_2020,
                            (f.pending_contracted_qtys + f.qty_to_procure) total_qty_to_be_delivered,
                            qp.procured qty_delivered_since_july_2019,
                            CONCAT(ROUND(qp.procured /(f.pending_contracted_qtys + f.qty_to_procure)*100,2), '%') percentage_delivered,
                            f.total_forecast qty_forcasted_to_consume,
                            qc.consumed actual_qty_consumed_from_july_todate,
                            CONCAT(ROUND(qc.consumed /f.total_forecast*100,2), '%') percentage_consumed 
                            FROM vw_drug_list d 
                            INNER JOIN vw_forecast_procured qp ON qp.drug_id = d.id  
                            INNER JOIN vw_forecast_consumed qc ON qc.drug_id = d.id  
                            INNER JOIN tbl_commodity_forecast f ON f.drug_id = d.id  
                            ORDER BY f.forecast DESC;")->result_array();


            $data = json_encode(array('main' => $results, 'columns' => $columns));
            $this->Cache->add('forecast', $data);
            return array('main' => $results, 'columns' => $columns);
        } else {
            return json_decode($this->Cache->get('forecast'), true);
        }
    }

    public function get_2pager_supplier($filters) {

        $columns = array();
        $soh_data = array(
            array(
                'type' => 'line',
                'color' => '#ff00f8',
                'negativeColor' => '#ff00f8',
                'name' => 'Delay Time (Days)',
                'data' => array())
        );


        $results = $this->db->query("SELECT * FROM tbl_supplier_index")->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result['supplier']);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Delay Time (Days)') {
                        array_push($soh_data[$index]['data'], $result['time']);
                    }
                }
            }
        }
//echo '<pre>';
//print_r($soh_data);
// die;
        return array('main' => $soh_data, 'columns' => $columns);
    }

    public function get_county_mos_2pager($filters) {

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        $default = $filters['drug'][0];

        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }

        if ($default == 'D') {
            $drug = 'Dolutegravir (DTG) 50mg Tabs';
        } else {
            $drug = $filters['drug'][0];
        }
        $table = 'dsh_stock';
        $column = 'county';
        $alias = 'county';


        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }
        //if (empty($this->Cache->get('mospager'))) {
        if (!empty($filters['county'])) {
            $county = $this->extract_elements($filters['county']);
            $query = " AND dc.county IN $county";
            $table = 'dsh_stock';
            $column = 'county';
        }
        if (!empty($filters['sub_county'])) {
            $subcounty = $this->extract_elements($filters['sub_county']);
            $query = " AND dc.sub_county IN $subcounty";
            $table = 'dsh_stock';
            $column = 'sub_county';
        }
        if (!empty($filters['facility'])) {
            $facilities = $this->extract_elements($filters['facility']);
            $query = " AND dc.facility IN $facilities";
            $table = 'dsh_stock';
            $column = 'facility';
        }

        if (empty($query)) {
            $results = $this->query_builder($datayear, $datamonth, $drug, '', $column);
        } else {
            $results = $this->query_builder($datayear, $datamonth, $drug, $query, $column);
        }



        $columns = array();
        $soh_data = array(
            array(
                'type' => 'column',
                'color' => '#00d2ff ',
                'negativeColor' => '#00d2ff',
                'name' => 'Months of Stock (MOS)',
                'yAxis' => 0,
                'data' => array()),
        );


        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result[$column]);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Months of Stock (MOS)') {
                        array_push($soh_data[$index]['data'], $result['mos']);
                    }
                }
            }
        }

        //   $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
        // $this->Cache->add('mospager', $data);
        return array('main' => $soh_data, 'columns' => $columns);
        //} else {
        // return json_decode($this->Cache->get('mospager'), true);
        //}
    }

    public function get_county_mos_consumption_2pager($filters) {

        $datamonth = date('M', strtotime($filters['data_date']));
        $datayear = date('Y', strtotime($filters['data_date']));
        $default = $filters['drug'][0];

        if ($datayear == '1970') {
            $datayear = $this->year;
            $datamonth = $this->month;
        }
        if ($default == 'D') {
            $drug = 'Dolutegravir (DTG) 50mg Tabs';
        } else {
            $drug = $filters['drug'][0];
        }
        $table = 'dsh_consumption';
        $column = 'county';
        $alias = 'county';


        if ($this->session->userdata('role') == 'subcounty') {
            
        } else if ($this->session->userdata('role') == 'county') {
            
        }
        //if (empty($this->Cache->get('mospager'))) {
        if (!empty($filters['county'])) {
            $county = $this->extract_elements($filters['county']);
            $query = " AND dc.county IN $county";
            $table = 'dsh_consumption';
            $column = 'county';
        }
        if (!empty($filters['sub_county'])) {
            $subcounty = $this->extract_elements($filters['sub_county']);
            $query = " AND dc.sub_county IN $subcounty";
            $table = 'dsh_consumption';
            $column = 'sub_county';
        }
        if (!empty($filters['facility'])) {
            $facilities = $this->extract_elements($filters['facility']);
            $query = " AND dc.facility IN $facilities";
            $table = 'dsh_consumption';
            $column = 'facility';
        }

        if (empty($query)) {
            $results = $this->query_builder2($datayear, $datamonth, $drug, '', $column);
        } else {
            $results = $this->query_builder2($datayear, $datamonth, $drug, $query, $column);
        }



        $columns = array();
        $soh_data = array(
            array(
                'type' => 'column',
                'color' => '#00d2ff ',
                'negativeColor' => '#00d2ff',
                'name' => 'Packs Consumed',
                'yAxis' => 0,
                'data' => array()),
        );


        if ($results) {
            foreach ($results as $result) {
                $columns[] = strtoupper($result[$column]);
                foreach ($soh_data as $index => $soh) {
                    if ($soh['name'] == 'Packs Consumed') {
                        array_push($soh_data[$index]['data'], $result['mos']);
                    }
                }
            }
        }

        //   $data = json_encode(array('main' => $soh_data, 'columns' => $columns));
        // $this->Cache->add('mospager', $data);
        return array('main' => $soh_data, 'columns' => $columns);
        //} else {
        //return json_decode($this->Cache->get('mospager'), true);
        //}
    }

    function query_builder($datayear, $datamonth, $drug, $param, $column) {
        return $this->db->query("SELECT (dc.$column) $column,SUM(dc.total) total,SUM(dc.amc_total) amc,IFNULL(ROUND(SUM(dc.total)/SUM(dc.amc_total),2),'0.00') mos,UPPER(dc.drug) drug
                                FROM dsh_stock dc
                                WHERE dc.data_year='2019'
                                AND dc.data_month='Dec'
                                AND dc.drug IN ('$drug')
                                $param
                                GROUP BY dc.$column
                                ORDER BY SUM(dc.total)/SUM(dc.amc_total) DESC;")->result_array();
    }

    function query_builder2($datayear, $datamonth, $drug, $param, $column) {
        return $this->db->query("SELECT (dc.$column) $column,SUM(dc.total)  mos,UPPER(dc.drug) drug
                                FROM dsh_consumption dc
                                WHERE dc.data_year='2019'
                                AND dc.data_month='Dec'
                                AND dc.drug IN ('$drug')
                                $param
                                GROUP BY dc.$column
                                ORDER BY SUM(dc.total) DESC;")->result_array();
    }

    function extract_elements($array) {
        $newid = '';
        foreach ($array as $i):
            $newid .= "'" . $i . "'" . ',';
        endforeach;
        return '(' . rtrim($newid, ',') . ')';
    }

    public function get_fforecast($filters) {
        $columns = array();

        $this->db->select("drug, facility, county, sub_county Subcounty, SUM(total) balance, SUM(amc_total) amc, IFNULL((SUM(total)/SUM(amc_total)), 0) mos", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drug, facility, county, sub_county');
        $this->db->having('mos < 3');
        $this->db->order_by('mos', 'ASC');
        $query = $this->db->get('dsh_stock');
        return array('main' => $query->result_array(), 'columns' => $columns);
    }

    public function get_low_mos_commodities($filters) {
        $columns = array();

        $this->db->select("drug, facility, county, sub_county Subcounty, SUM(total) balance, SUM(amc_total) amc, IFNULL((SUM(total)/SUM(amc_total)), 0) mos", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drug, facility, county, sub_county');
        $this->db->having('mos < 3');
        $this->db->order_by('mos', 'ASC');
        $query = $this->db->get('dsh_stock');
        return array('main' => $query->result_array(), 'columns' => $columns);
    }

    public function get_low_mos_commodities_hb($filters) {
        $columns = array();
        $datayear = $filters['data_year'];
        $datamonth = $filters['data_month'];

        $query = $this->db->query("SELECT d.regimen, f.name facility, d.county, d.sub_county subcounty, IFNULL(SUM(total), 0) total  
                        FROM dsh_patient d
                        INNER JOIN tbl_facility f ON f.name = d.facility
                        WHERE d.data_year IN('$datayear') 
                        AND d.data_month IN('$datamonth') 
                        AND d.regimen='HPB1A | TDF + 3TC (HIV-ve HepB patients)'
                        GROUP BY `regimen`, `facility`, `county`, `sub_county`
                        ORDER BY `total` DESC")->result_array();

        return array('main' => $query, 'columns' => $columns);
    }

    public function get_high_mos_commodities($filters) {
        $columns = array();

        $this->db->select("drug, facility, county, sub_county Subcounty, SUM(total) balance, SUM(amc_total) amc, IFNULL((SUM(total)/SUM(amc_total)), 0) mos", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drug, facility, county, sub_county');
        $this->db->having('mos > 6');
        $this->db->order_by('mos', 'ASC');
        $query = $this->db->get('dsh_stock');
        return array('main' => $query->result_array(), 'columns' => $columns);
    }

    public function get_high_mos_commodities_hb($filters) {
        $datayear = $filters['data_year'];
        $datamonth = $filters['data_month'];

        $query = $this->db->query("SELECT d.regimen, f.name facility, d.county, d.sub_county subcounty, IFNULL(SUM(total), 0) total  
                        FROM dsh_patient d
                        INNER JOIN tbl_facility f ON f.name = d.facility
                        WHERE d.data_year IN('$datayear') 
                        AND d.data_month IN('$datamonth') 
                        AND d.regimen='HPB1B | TDF + FTC (HIV-ve HepB patients)'
                        GROUP BY `regimen`, d.facility, `county`, `sub_county`
                        ORDER BY `total` DESC")->result_array();

        return array('main' => $query, 'columns' => $columns);
    }

}
