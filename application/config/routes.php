<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
*/
/*lowercase for linux*/
$route['default_controller']                                = 'Dashboard';
$route['api']                                               = 'API';
$route['main']                                              = 'Dashboard/main';
$route['ftp']                                               = 'Ftp';
$route['manager']                                           = 'Manager';
/*general_user*/
$route['manager/login']                                     = 'Manager/load_page/user/login/Login';
$route['manager/forgot_pass']                               = 'Manager/load_page/user/forgot/Forgot Password';
$route['manager/register_account']                          = 'Manager/load_page/user/register/New Account';
$route['user/create_account']                               = 'Manager/user/create_account';
$route['user/authenticate']                                 = 'Manager/user/authenticate';
$route['user/reset_account']                                = 'Manager/user/reset_account';
$route['manager/update_profile']                            = 'Manager/user/update_profile';
$route['manager/update_password']                           = 'Manager/user/update_password';
$route['manager/logout']                                    = 'Manager/user/logout';
/*template*/
$route['manager/dashboard']                                 = 'Manager/load_template/dashboard/dashboard/Dashboard/0';
$route['manager/hepb']                                      = 'Manager/load_template/dashboard/hepb/Dashboard/0';
$route['manager/2_pager']                                   = 'Manager/load_template_dashboard/dashboard/2pager/Two Pa/0';
$route['manager/2pager']                                    = 'Manager/load_template/dashboard/2pager/Dashboard/0';
$route['manager/two_pager']                                 = 'Manager/load_template/dashboard/two_pager/Dashboard/0';
$route['manager/profile']                                   = 'Manager/load_template/user/profile/Profile/0';
$route['manager/(:any)/(:any)']                             = 'Manager/load_template/$1/$2/$2';
/*order*/
$route['manager/orders/reports']                            = 'Manager/load_template/orders/reports/Reports/0';
$route['manager/orders/view/(:any)/(:any)']                 = 'Manager/load_template/orders/cdrr_maps/View Order/0/$1/$2';
$route['manager/orders/allocation']                         = 'Manager/load_template/orders/allocation/Allocation/0';
$route['manager/orders/allocate/(:any)/(:any)']             = 'Manager/load_template/orders/allocate/Allocation/0';
$route['manager/orders/view_allocation/(:any)/(:any)']      = 'Manager/load_template/orders/allocate/Allocation/0';
$route['manager/orders/reporting_rates']                    = 'Manager/load_template/orders/reporting_rates/Reporting Rates/0';
$route['manager/orders/allocation/subcounty/(:any)/(:any)'] = 'Manager/load_template/orders/subcounty_reports/Reporting Rates/0';
$route['manager/orders/allocation/county/(:any)/(:any)']    = 'Manager/load_template/orders/county_reports/Reporting Rates/0';
$route['manager/orders/edit_allocation/(:any)']             = 'Manager/load_template/orders/edit_allocation/Edit Allocation/0/$1';
$route['manager/orders/view_satellites/(:any)/(:any)']      = 'Manager/load_template/orders/satellites/Satellites/0';
/*procurement*/
$route['manager/procurement/minute/(:any)/(:any)']          = 'Manager/load_template/minute/minute/Minute/0/$1/$2';
$route['manager/procurement/commodity']                     = 'Manager/load_template/procurement/commodity/Procurement Commodities/0';
$route['manager/procurement/meeting']                       = 'Manager/load_template/procurement/meeting/Procurement Meeting/0';
$route['manager/procurement/meeting/(:any)/(:any)']         = 'Manager/load_template/procurement/minute/Procurement Meeting Minute/0';
$route['manager/procurement/meeting/(:any)/(:any)/(:any)']  = 'Manager/load_template/procurement/minute/Procurement Meeting Minute Edit/0';
$route['manager/procurement/tracker']                       = 'Manager/load_template/procurement/tracker/Procurement Tracker/0';
$route['manager/save_procurement']                          = 'Manager/procurement/save_tracker';
$route['manager/public/minute/(:any)']                      = 'Manager/load_page/public/minute/Minute';
$route['manager/public/procurement_template/(:any)']        = 'Manager/load_page/procurement/procurement_template/Template';
$route['manager/create/pdf/(:any)']                         = 'Manager/generateMinute';
$route['manager/section/faq/download']                      = 'Manager/getFaq';


$route['ADT/facility/(:num)'] = "ADT/Dashboard/facility/$1";
$route['ADT/backups']         = "ADT/Dashboard/backups";
$route['ADT/partner']         = "ADT/Dashboard/partner";
$route['ADT/county']          = "ADT/Dashboard/county";
$route['ADT/subcounty']       = "ADT/Dashboard/subcounty";
$route['ADT/facilities']      = "ADT/Dashboard/facilities";


$route['ADT/dashboard/adt_art_numbers_by_status'] = "ADT/Dashboard/adt_art_numbers_by_status";
$route['ADT/dashboard/art_numbers_by_service']    = "ADT/Dashboard/art_numbers_by_service";
$route['ADT/dashboard/get_patients_by_partner']   = "ADT/Dashboard/get_patients_by_partner";
$route['ADT/dashboard/get_regimen_per_patient']   = "ADT/Dashboard/get_regimen_per_patient";
$route['ADT/dashboard/adt_art_ageing_table']      = "ADT/Dashboard/adt_art_ageing_table";

$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;